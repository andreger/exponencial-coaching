<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inscricao extends CI_Controller {

	var $aluno = null;
	
	public function __construct()
	{
		parent::__construct();

		if(is_area_desativada(array(PAINEL_INSCRICAO_COACHING))){
			redirecionar_erro_500();
		}

		date_default_timezone_set('America/Sao_Paulo');
		
		redirecionar_se_nao_estiver_logado();

		session_start();

		$this->load->model('Coaching_model');

		$usuario = get_usuario_array(get_current_user_id());

		$this->aluno = $this->Coaching_model->get_aluno($usuario['email']);
	}
	
	public function index()
	{
		if(!$this->aluno) {
			redirect(get_novo_aluno_url());
		}

		$this->load->helper('status');

		$data['fase_aluno'] = get_etapa_id($this->aluno);

		// Planilhas detalhadas tb exibem informações de aluno ativo
		if($data['fase_aluno'] == 4) {
			$data['fase_aluno'] = 6;
		}

		switch($data['fase_aluno']) {
			case 6: self::ativo(); break;
			case 7: self::ex_aluno(); break;
			case 8: self::nao_convertido(); break;
			default: $this->load->view('inscricao/index', $data);
		}
	}
	
	public function iniciar()
	{
		$usuario = get_usuario_array(get_current_user_id());
		$this->Coaching_model->novo_aluno($usuario['email'], $usuario['nome_completo']);
		
		$titulo = $usuario['nome_completo'] . ' iniciou o processo de coaching';
		enviar_email('coaching@exponencialconcursos.com.br', $titulo, $titulo);
		
		redirect(get_inscricao_home_url());
	}
	
	public function novo_aluno()
	{
		$this->load->view('inscricao/novo_aluno');
	}
	
	public function ex_aluno()
	{
		$this->load->view('inscricao/ex_aluno');
	}
	
	public function nao_convertido()
	{
		$this->load->view('inscricao/nao_convertido');
	}
	
	public function ativo()
	{
// 		$this->output->cache(60);
		$this->load->view('inscricao/ativo');
	}
	
	public function formulario_basico()
	{
		if($this->input->post()) {

			$data_podio = array();
			
			//if($item = $this->input->post('nome')) $data_podio['nome'] = $item;
			if($item = $this->input->post('concurso')) $data_podio['escolha-sua-turma-de-coaching'] = (int)$item;
			if($item = $this->input->post('skype_id')) $data_podio['skype-id'] = $item;
			if($item = $this->input->post('cpf')) $data_podio['cpf'] = (float)(desformatar_cpf($item));
			if($item = $this->input->post('sexo')) $data_podio['sexo'] = (int)$item;
			if($item = $this->input->post('estado')) $data_podio['onde-voce-mora'] = (int)$item;
			if($item = $this->input->post('nascimento')) $data_podio['nascimento'] = converter_para_yyyymmdd($item) . ' 00:00:00';
			if($item = $this->input->post('mora_com')) $data_podio['mora-com'] = array_de_inteiros($item);
			if($item = $this->input->post('filhos')) $data_podio['possui-filhos'] = (int)$item;
			if($item = $this->input->post('irmaos')) $data_podio['possui-irmaos'] = (int)$item;
			if($item = $this->input->post('atividade')) $data_podio['pratica-alguma-atividade-fisica'] = (int)$item;
			if($item = $this->input->post('qualidade_sono')) $data_podio['qual-a-qualidade-do-seu-sono'] = (int)$item;
			if($item = $this->input->post('capacidade_atencao')) $data_podio['qual-sua-capacidade-de-atencao-quando-estudando'] = (int)$item;
			if($item = $this->input->post('horas_estudo')) $data_podio['horas-de-estudo-disponivel-por-dia-em-media'] = (int)$item;
			if($item = $this->input->post('horarios')) $data_podio['qual-horario-estuda-melhor'] = (int)$item;
			if($item = $this->input->post('cansaco')) $data_podio['sente-cansaco-fisico-eou-mental-ao-estudar'] = (int)$item;
			if(($horas = $this->input->post('horas_sono')) && ($minutos = $this->input->post('minutos_sono'))) $data_podio['tempo-medio-de-sono'] = ((int)$horas * 3600) + (int)$minutos * 60;
			if($item = $this->input->post('media_colegio')) $data_podio['qual-era-sua-media-no-colegio'] = (int)$item;
			if($item = $this->input->post('tempo_estudo')) $data_podio['ha-quanto-tempo-estuda-para-concursos'] = (int)$item;
			if($item = $this->input->post('consultorias')) $data_podio['ja-fez-alguma-consultoriacoaching-antes-qualis'] = array_de_inteiros($item);
			if($item = $this->input->post('motivo_concurso')) $data_podio['por-que-decidiu-fazer-este-concurso'] = $item;
			if($item = $this->input->post('motivo_coaching')) $data_podio['por-que-esta-buscando-coaching'] = $item;
			if($item = $this->input->post('como_conheceu')) $data_podio['como-conheceu-o-exponencial-concursos'] = (int)$item;
			if($item = $this->input->post('indicacao')) $data_podio['caso-tenha-conhecido-o-exponencial-por-indicacao-de-um-'] = $item;
			
			$data_podio['status'] = [PODIO_COACHEE_STATUS_FORMULARIO_DETALHADO, PODIO_COACHEE_STATUS_ATIVO];
			$data_podio['data-formulario-detalhado'] = get_data_hora_agora();
			
			$this->Coaching_model->atualizar_coachee($this->aluno, $data_podio);
			
			// $titulo = $this->aluno['nome'] . ' passou para etapa Inscrição Pendente';
			// enviar_email(EMAIL_COACHING, $titulo, $titulo);
			
			redirect(get_inscricao_home_url());
		}
		
		$data['concurso_selecionado'] = $this->input->post('concurso') ?: null;
		$data['concursos_combo'] = get_combo_options($this->Coaching_model->listar_opcoes_combo('escolha-sua-turma-de-coaching'));
		
		$data['sexo_selecionado'] = $this->input->post('sexo') ?: null;
		$data['sexos_combo'] = array('' => '',
				'1' => 'Feminino',
				'2' => 'Masculino'
		);
		
		$data['estado_selecionado'] = $this->input->post('estado') ?: null;
		$data['estados_combo'] = array('' => '',
				"1" => "Acre",
				"2" => "Alagoas",
				"3" => "Amapá",
				"4" => "Amazonas",
				"5" => "Bahia",
				"6" => "Ceará",
				"7" => "Distrito Federal",
				"27"=> "Espírito Santo",
				"8" => "Goiás",
				"9" => "Maranhão",
				"10" => "Mato Grosso",
				"11" => "Mato Grosso do Sul",
				"12" => "Minas Gerais",
				"13" => "Pará",
				"14" => "Paraíba",
				"15" => "Paraná",
				"16" => "Pernambuco",
				"17" => "Piauí",
				"18" => "Rio de Janeiro",
				"19" => "Rio Grande do Norte",
				"20" => "Rio Grande do Sul",
				"21" => "Rondônia",
				"22" => "Roraima",
				"23" => "Santa Catarina",
				"24" => "São Paulo",
				"25" => "Sergipe",
				"26" => "Tocantins"
		);
		
		$data['filho_selecionado'] = $this->input->post('filhos') ?: null;
		$data['filhos_combo'] = array('' => '',
				"1" => "0",
				"2" => "1",
				"3" => "2",
				"4" => "Mais de 2",
		);
		
		$data['irmao_selecionado'] = $this->input->post('irmaos') ?: null;
		$data['irmaos_combo'] = array('' => '',
				"1" => "0",
				"2" => "1",
				"3" => "2",
				"4" => "Mais de 2",
		);
		
		$data['atividade_selecionada'] = $this->input->post('atividade') ?: null;
		$data['atividades_combo'] = array('' => '',
				"1" => "Não",
				"2" => "Sim"
		);
		
		$data['qualidade_selecionada'] = $this->input->post('qualidade_sono') ?: null;
		$data['qualidades_combo'] = array('' => '',
				"1" => "Durmo muito bem, sinto-me descansado na maioria das vezes",
				"2" => "Durmo bem alguns dias, mas na grande maioria das vezes sinto que dormi mal",
				"3" => "Durmo mal praticamente todos os dias",
				"4" => "Sofro de insônia e/ou outros distúrbios do sono"
		);
		
		$data['capacidade_selecionada'] = $this->input->post('capacidade_atencao') ?: null;
		$data['capacidades_combo'] = array('' => '',
				"1" => "Consigo prestar bastante atenção normalmente",
				"2" => "Presto atenção, mas distraio-me com pessoas, ruídos ou similares",
				"3" => "Qualquer coisa me distrai, inclusive meus pensamentos!"
		);
		
		$data['hora_selecionada'] = $this->input->post('horas_estudo') ?: null;
		$data['horas_combo'] = array('' => '',
				"1" => "até 14 horas",
				"2" => "de 14 a 28 horas",
				"3" => "de 28 a 42 horas",
				"4" => "de 42 a 56 horas",
				"5" => "mais de 56 horas"
		);
		
		$data['horario_selecionado'] = $this->input->post('horarios') ?: null;
		$data['horarios_combo'] = array('' => '',
				"1" => "Manhã",
				"2" => "Tarde",
				"3" => "Noite",
				"5" => "Madrugada",
				"4" => "Indiferente"
		);
		
		$data['cansaco_selecionado'] = $this->input->post('cansaco') ?: null;
		$data['cansacos_combo'] = array('' => '',
				"1" => "Sinto cansaço físico",
				"2" => "Sinto cansaço mental",
				"3" => "Sinto cansaço físico e mental",
				"4" => "Nenhum - canso-me somente depois de muitas horas de estudo, o que julgo natural"
		);
		
		$data['media_selecionada'] = $this->input->post('media_colegio') ?: null;
		$data['medias_combo'] = array(
				"1" => "5",
				"2" => "6",
				"3" => "7",
				"4" => "8",
				"5" => "9",
				"6" => "10"
		);
		
		$data['tempo_selecionado'] = $this->input->post('tempo_estudo') ?: null;
		$data['tempo_estudo_combo'] = array('' => '',
				"1" => "Estou começando agora",
				"2" => "Menos de 6 meses",
				"3" => "Entre 6 meses e 1 ano",
				"4" => "Entre 1 e 2 anos",
				"5" => "Mais de 2 anos"
		);
		
		$data['como_conheceu_selecionado'] = $this->input->post('como_conheceu') ?: null;
		$data['como_conheceu_combo'] = get_combo_options($this->Coaching_model->listar_opcoes_combo('como-conheceu-o-exponencial-concursos'));
		$data['como_conheceu_combo'] = array('' => '',
				"1" => "Facebook",
				"2" => "Forum Concurseiros",
				"3" => "Google",
				"4" => "Indicação de amigos",
				"7" => "Indicação de aluno do Exponencial",
				"5" => "Pelos professores do Exponencial",
				"6" => "Outro"
		);
		
		$data['mora_com_combo'] = array(
				"1" => "Sozinho(a)",
				"2" => "Pais",
				"3" => "Cônjuge/ Companheiro(a)",
				"4" => "Amigo(s)",
		);
		
		/***********************************************************************
		 * https://podio.com/profinfnetedubr/lslc/apps/bug-tracker/items/802
		 * Acertar ordem de opções no primeiro formulário do coaching
		 ***********************************************************************/
	 	$consultorias = get_combo_options($this->Coaching_model->listar_opcoes_combo('ja-fez-alguma-consultoriacoaching-antes-qualis'));
	 	asort($consultorias);
	 
	 	$outros = $consultorias[5];
	 	unset($consultorias[5]);
	 
		$nunca = $consultorias[6];
	 	unset($consultorias[6]);
	 
	 	$consultorias[5] = $outros;
	 	$consultorias[6] = $nunca;
	 
	 	$data['consultorias_combo'] = $consultorias;
	
		$this->load->view('inscricao/formulario_basico', $data);
	}
	
	public function formulario_detalhado()
	{
		if($this->input->post()) {

			$data_podio = array();
			
			if($item = $this->input->post('materias')) $data_podio['selecione-as-materias-em-que-tem-dificuldade'] = array_de_inteiros($item);
			if($item = $this->input->post('especificidades')) $data_podio['titulo'] = $item;
			if($item = $this->input->post('faculdade')) $data_podio['se-graduado-informe-sua-faculdade-do-contrario-deixe-em'] = $item;
			if($item = $this->input->post('graduacao')) $data_podio['se-graduado-informe-o-curso-em-que-se-graduou-do-contra'] = $item;
			if($item = $this->input->post('data_graduacao')) $data_podio['se-graduado-informe-a-data-de-sua-formacao-do-contrario'] = converter_para_yyyymmdd_HHiiss($item);
			if($item = $this->input->post('experiencia')) $data_podio['informe-resumidamente-sua-experiencia-profissional'] = $item;
			if($item = $this->input->post('trabalhando')) $data_podio['esta-trabalhando-atualmente'] = (int)$item;
			if($item = $this->input->post('deslocamento')) $data_podio['quando-voce-se-desloca-do-trabalho-para-casa-e-vice-ver'] = (int)$item;
			if($item = $this->input->post('desgaste')) $data_podio['quando-voce-sai-do-trabalho-sente-se-desgastado-fisica-'] = (int)$item;
			if(($horas = $this->input->post('horas_deslocamento')) && ($minutos = $this->input->post('minutos_deslocamento'))) $data_podio['quanto-tempo-diariamente-voce-gasta-no-deslocamento-cas'] = ((int)$horas * 3600) + (int)$minutos * 60;
			if($item = $this->input->post('lugar')) $data_podio['onde-voce-costuma-estudar'] = array_de_inteiros($item);
			if($item = $this->input->post('tempo')) $data_podio['quanto-tempo-voce-gasta-para-deslocar-se-ate-a-bibliote'] = (int)$item;
			if($item = $this->input->post('ambiente')) $data_podio['o-ambiente-barulhento-oa-distrai'] = (int)$item;
			if($item = $this->input->post('dificuldade_tempo')) $data_podio['ja-teve-dificuldades-em-fazer-uma-prova-por-conta-do-te'] = (int)$item;
			if($item = $this->input->post('dificuldade_materia')) $data_podio['se-respondeu-sim-acima-indique-as-materias-que-mais-tem'] = array_de_inteiros($item);
			if($item = $this->input->post('classificacao')) $data_podio['informe-outras-provas-que-tenha-realizado-e-sua-classif'] = $item;
			if($item = $this->input->post('tipos_materiais')) $data_podio['selecione-os-tipos-de-materiais-com-que-prefere-estudar'] = array_de_inteiros($item);
			if($item = $this->input->post('tecnicas')) $data_podio['indique-as-tecnicas-de-estudos-que-ja-aplica-ou-aplicou'] = array_de_inteiros($item);
			if($item = $this->input->post('resumos')) $data_podio['voce-faz-resumos-das-materias-que-estuda'] = (int)$item;
			if($item = $this->input->post('dificuldade_lei')) $data_podio['tem-dificuldades-em-ler-leis-diretamente'] = (int)$item;
			if($item = $this->input->post('lei_acao')) $data_podio['quando-voce-le-leis-diretamente-o-que-voce-faz'] = (int)$item;
			if($item = $this->input->post('ultima_prova')) $data_podio['voce-fez-a-ultima-prova-do-icms-rj-2013'] = (int)$item;
			if($item = $this->input->post('dificuldade_prova')) $data_podio['se-fez-a-ultima-prova-em-quais-materias-teve-maior-difi'] = array_de_inteiros($item);
			if($item = $this->input->post('motivos')) $data_podio['explique-os-motivos-por-que-teve-dificuldades-nas-mater'] = $item;
			
			$data_podio['nome'] = $this->aluno['nome'];
			
			$this->Coaching_model->atualizar_coachee_estudo($this->aluno, $data_podio);
			
			// $titulo = $this->aluno['nome'] . ' passou para etapa Perfil Psicopedagógico';
			// enviar_email(EMAIL_COACHING, $titulo, $titulo);
				
			redirect(get_inscricao_home_url());
		}
		
		$data['materias_combo'] = get_combo_options($this->Coaching_model->listar_opcoes_combo('selecione-as-materias-em-que-tem-dificuldade'));
		
		$data['trabalhando_combo'] = array('' => '',
				"1" => "Não",
				"2" => "Sim"
		);
		$data['trabalhando_selecionado'] = $this->input->post('trabalhando') ?: null;
		
		$data['deslocamento_combo'] = array('' => '',
				"1" => "Assistir a vídeos",
				"2" => "Escrever",
				"3" => "Escutar áudio",
				"4" => "Fazer anotações ou questões",
				"5" => "Ler"
		);
		$data['deslocamento_selecionado'] = $this->input->post('deslocamento') ?: null;
		
		$data['desgaste_combo'] = array('' => '',
				"1" => "Nunca",
				"2" => "Raramente",
				"3" => "Frequentemente",
				"4" => "Sempre"
		);
		$data['desgaste_selecionado'] = $this->input->post('desgaste') ?: null;
		
		$data['lugar_combo'] = get_combo_options($this->Coaching_model->listar_opcoes_combo('onde-voce-costuma-estudar'));
		
		$data['tempo_combo'] = array('' => '',
				"1" => "Menos de 30 minutos",
				"2" => "Entre 30 minutos e 1 hora",
				"3" => "Mais de 1 hora",
				"4" => "Não há biblioteca ou local similar onde eu possa estudar",
				"5" => "Não gosto de estudar em bibliotecas ou locais similares"
		);
		$data['tempo_selecionado'] = $this->input->post('tempo') ?: null;
		
		$data['ambiente_combo'] = array('' => '',
				"1" => "Não",
				"2" => "Às vezes",
				"3" => "Sempre"
		);
		$data['ambiente_selecionado'] = $this->input->post('ambiente') ?: null;
		
		$data['dificuldade_tempo_combo'] = array('' => '',
				"1" => "Não",
				"2" => "Sim"
		);
		$data['dificuldade_tempo_selecionado'] = $this->input->post('dificuldade_tempo') ?: null;
		
		$data['dificuldade_materia_combo'] = get_combo_options($this->Coaching_model->listar_opcoes_combo('se-respondeu-sim-acima-indique-as-materias-que-mais-tem'));
		
		$data['tipos_materiais_combo'] = get_combo_options($this->Coaching_model->listar_opcoes_combo('selecione-os-tipos-de-materiais-com-que-prefere-estudar'));
		
		$data['tecnicas_combo'] = get_combo_options($this->Coaching_model->listar_opcoes_combo('indique-as-tecnicas-de-estudos-que-ja-aplica-ou-aplicou'));
		
		$data['resumos_combo'] = array('' => '',
				"1" => "Não, nunca",
				"2" => "Não, mas estudo por resumos dos outros",
				"3" => "De vez em quando",
				"4" => "Sempre ou quase sempre"
		);
		$data['resumos_selecionado'] = $this->input->post('resumos') ?: null;
		
		$data['dificuldade_lei_combo'] = array('' => '',
				"1" => "Não",
				"2" => "Sim, mas encaro",
				"3" => "Sim, daí prefiro assistir a uma aula ou ler uma apostila a ler a lei diretamente"
		);
		$data['dificuldade_lei_selecionado'] = $this->input->post('dificuldade_lei') ?: null;
		
		$data['lei_acao_combo'] = array('' => '',
				"1" => "Sublinho as partes principais",
				"2" => "Faço um resumo em papel",
				"3" => "Monto mapas mentais ou esquemas",
				"4" => "Gravo partes em áudio para ouvir novamente. ",
				"5" => "Não faço nada, apenas as leio e faço exercícios depois",
				"6" => "Nunca leio leis diretamente - ou raramente o faço. Prefiro buscar materiais ou aulas que as expliquem para mim"
		);
		$data['lei_acao_selecionado'] = $this->input->post('lei_acao') ?: null;
		
		$data['ultima_prova_combo'] = array('' => '',
				"1" => "Não",
				"2" => "Sim"
		);
		$data['ultima_prova_selecionado'] = $this->input->post('ultima_prova') ?: null;
		
		$data['dificuldade_prova_combo'] = get_combo_options($this->Coaching_model->listar_opcoes_combo('se-fez-a-ultima-prova-em-quais-materias-teve-maior-difi'));
		
		$this->load->view('inscricao/formulario_detalhado', $data);
	}
	
	public function perfil_psicopedagogico()
	{
		$data = array();
		
		if($this->input->post()) {
			$data_podio = array(
				'mbti' => (int)$this->input->post('resultado')
			);
			
			$this->Coaching_model->atualizar($this->aluno['item_id'], $data_podio);

			$this->Coaching_model->atualizar_status($this->aluno['item_id'], [PODIO_COACHEE_STATUS_PLANILHAS_DETALHADAS, PODIO_COACHEE_STATUS_ATIVO]);
			
			$titulo = $this->aluno['nome'] . ' passou para etapa Inscrição Pendente';
			enviar_email(EMAIL_COACHING, $titulo, $titulo);

			$mensagem = get_template_email('solicitar-planilhas.php', array(
				'nome' => $this->aluno['nome']
			));

			enviar_email($this->aluno['email'], 'Exponencial Concursos - Coaching', $mensagem);

			redirect(get_inscricao_home_url());
		}
		
		$this->load->view('inscricao/perfil_psicopedagogico', $data);
	}

	public function solicitar_planilhas_detalhadas($item_id)
	{
		$aluno = $this->Coaching_model->get_aluno_by_item_id($item_id);
		$link = $this->input->get('link');
		
		$mensagem = get_template_email('solicitar-planilhas.php', array(
			'nome' => $aluno['nome']
		));

		enviar_email($aluno['email'], 'Exponencial Concursos - Coaching', $mensagem);
		$this->Coaching_model->atualizar_status($item_id, PODIO_COACHEE_STATUS_ATIVO);
		
		$titulo = 'Foram solicitadas planilhas detalhadas de ' . $aluno['nome'];
		enviar_email('coaching@exponencialconcursos.com.br', $titulo, $titulo);
		
		set_mensagem_flash("SUCESSO", "Planilhas detalhadas solicitadas com sucesso!");

		redirect(get_relatorio_inscricoes());
	}
	
	public function planilhas_detalhadas()
	{
		$data = array();
		
		if($this->input->post() ) {

			criar_pasta_temporaria ();
			$target_dir = $_SERVER ['DOCUMENT_ROOT'] . "/wp-content/temp/";
			$avaliacao_file = $target_dir . basename ( $_FILES ["avaliacao"] ["tmp_name"] );
			$disponibilidade_file = $target_dir . basename ( $_FILES ["disponibilidade"] ["tmp_name"] );
			$avaliacao_podio_file = 'avaliacao-' . time () . '.xlsx';
			$disponibilidade_podio_file = 'disponibilidade-' . time () . '.xlsx';

			move_uploaded_file ( $_FILES ['avaliacao'] ['tmp_name'], $avaliacao_file );
			move_uploaded_file ( $_FILES ['disponibilidade'] ['tmp_name'], $disponibilidade_file );

			$this->Coaching_model->anexar($this->aluno['item_id'], $avaliacao_file, $avaliacao_podio_file);
			$this->Coaching_model->anexar($this->aluno['item_id'], $disponibilidade_file, $disponibilidade_podio_file);

			$this->Coaching_model->atualizar_status($this->aluno['item_id'], PODIO_COACHEE_STATUS_ATIVO);
			
			// $titulo = $this->aluno['nome'] . ' enviou planilhas_detalhadas';
			// enviar_email('coaching@exponencialconcursos.com.br', $titulo, $titulo);

			$data['sucesso'] = true;

			redirect(get_inscricao_home_url());
		}
		
		$this->load->view('inscricao/planilhas_detalhadas', $data);
	}
	
	public function liberar_pagamento($item_id)
	{
		$dia_de_hoje = date('d');
		$dia_inicio = '';
		$data_inicio = '';
		if($dia_de_hoje <= 3) {
			$dia_inicio = 10;
			$data_inicio = date('10/m/Y');
		}
		elseif($dia_de_hoje > 3 && $dia_de_hoje <=14) {
			$dia_inicio = 20;
			$data_inicio = date('20/m/Y');
		}
		else {
			$dia_inicio = 10;
			$data_inicio = date('10/m/Y', strtotime('+20 days')); // antigamente era +1 month, mas se o e-mail fosse disparado num mês com 31 dias o sistema pulava 2 meses, ao invés de 1
		}		
		
		$aluno = $this->Coaching_model->get_aluno_by_item_id($item_id);

		$mensagem = get_template_email('coaching-pagamento.php', array(
				'dia_de_hoje' => $dia_de_hoje,
				'dia_inicio' => $dia_inicio,
				'data_inicio' => $data_inicio,
				'link_mensal' => get_pagamento_coaching_link_mensal($aluno['area_id']),
				'link_trimestral' => get_pagamento_coaching_link_trimestral($aluno['area_id']),
				'link_semestral' => get_pagamento_coaching_link_semestral($aluno['area_id']),
				'nome' => $aluno['nome']
		));

		enviar_email($aluno['email'], 'Exponencial Concursos - Coaching', $mensagem);
		
		$this->Coaching_model->atualizar_status($item_id, PODIO_COACHEE_STATUS_PAGAMENTO_LIBERADO);
		
		$titulo = $aluno['nome'] . ' passou para etapa Pagamento Liberado';
		
		enviar_email('coaching@exponencialconcursos.com.br', $titulo, $titulo);

		set_mensagem_flash("SUCESSO", "Pagamento liberado com sucesso!");

		redirect(get_relatorio_inscricoes());
	}
	
	public function ativar($item_id)
	{
		$aluno = $this->Coaching_model->get_aluno_by_item_id($item_id);
		// $link = $this->input->get('link');
		
		$mensagem = get_template_email('coaching-confirmacao.php', array(
			'nome' => $aluno['nome']
		));
		
		enviar_email($aluno['email'], 'Exponencial Concursos - Coaching', $mensagem);

		$data_podio = [
			'data-formulario-detalhado' => get_data_hora_agora(),
		];

		$this->Coaching_model->atualizar($item_id, $data_podio);
		
		$this->Coaching_model->atualizar_status($item_id, array(PODIO_COACHEE_STATUS_ATIVO, PODIO_COACHEE_STATUS_FORMULARIO_DETALHADO));

		$titulo = $aluno['nome'] . ' está Ativo no coaching';
		enviar_email(EMAIL_COACHING, $titulo, $titulo);

		set_mensagem_flash("SUCESSO", "Aluno ativado com sucesso!");
		
		redirect(get_relatorio_inscricoes());
	}	
	
}

