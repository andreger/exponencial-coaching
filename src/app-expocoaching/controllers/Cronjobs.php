<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjobs extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Sao_Paulo');
	}
	
	/**
	 * Min Hour	Day	Mon	Wday 
	 *  0	11	 *	 *	 *	 
	 */

	public function enviar_email_cobranca()
	{
	    /**
	     * O envio do e-mail foi suspenso por Lucas Salvetti em 09/07/19
	     * 
	     * Cronjob tb foi desativado em WP-Admin > Exponencial > Painel de Controle
	     * 
	     * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2244
	     */
	    return;
	    
		$this->load->model('Coaching_model');
		$this->load->model('Pagamento_model');
		
		// lista todos os alunos ativos
		$alunos_ativos = $this->Coaching_model->listar_alunos_inscricao_ativa();
		
		$data_inicio = date('Y-m-01', time());
		$data_fim = date('Y-m-t', time());
		$dia_de_hoje = date('d', time());
		
		// lista os pagamentos do mês
		$pagamentos = $this->Pagamento_model->listar_pagamentos($data_inicio, $data_fim);

		// mantém apenas os pagamentos com alunos associados
		$pagamentos_alunos = array();
		foreach ($pagamentos as $pagamento) {
			if(isset($pagamento['aluno'])) {
				array_push($pagamentos_alunos, $pagamento['aluno']);
			}
		}
		
		$num_dias_depois = date('d', strtotime("+" . EMAIL_COBRANCA_COACHING_DIAS_ANTES . " days"));
		$data_dias_depois = date('Y-m-d', strtotime("+" . EMAIL_COBRANCA_COACHING_DIAS_ANTES . " days"));
		$data_pgto = converter_para_ddmmyyyy($data_dias_depois);

		echo "Dias depois: {$num_dias_depois} <br>";
		echo "Data dias depois: {$data_dias_depois} <br>";
		echo "Data pagamento: {$data_pgto} <br><br>";

		foreach ($alunos_ativos as $aluno) {
			echo "Processando aluno {$aluno['nome']} ({$aluno['item_id']}) <br>";

			// considera apenas alunos que tenha referencia_pgto
			if(isset($aluno['referencia_pgto']) && is_a($aluno['referencia_pgto'], 'DateTime')) {

				echo "Referencia pgto: {$aluno['referencia_pgto']->format('d')} <br>";
				if($aluno['referencia_pgto']->format('d') == $num_dias_depois) {

					// verifica se aluno não está no array dos pagamentos do mês
					if(!in_array($aluno['item_id'], $pagamentos_alunos)) {

						$link_mensal = get_pagamento_coaching_link_mensal($aluno['area_id']);
						$link_trimestral = get_pagamento_coaching_link_trimestral($aluno['area_id']);
						$link_semestral = get_pagamento_coaching_link_semestral($aluno['area_id']);
						$data = array('nome' => $aluno['nome'], 'link_mensal' => $link_mensal, 'link_trimestral' => $link_trimestral, 'link_semestral' => $link_semestral);

						echo "<b>Enviando e-mail para {$aluno['item_id']} - {$aluno['nome']}</b><br>";
						
						$mensagem = get_template_email('coaching-cobranca.php', $data);
						
						if(isset($aluno['email'])) {
							enviar_email($aluno['email'], 'Exponencial Concursos - Renovação do Serviço de Coaching', $mensagem, 'exponencial@exponencialconcursos.com.br', 'coaching@exponencialconcursos.com.br');
						}
					}
					else {
						echo "Este aluno possui pagamento cadastrado nesse mês<br>";
					}
				}
			}
			else {
				echo "Aluno não possui referencia pgto <br>";
			}

			echo "<br>";
		}
		
		// self::debug_imprimir_alunos($alunos_ativos);
		// self::debug_imprimir_pagamentos($pagamentos);
		// self::debug_imprimir_pagamentos_pendentes($result);
	}
	
	public function gerar_relatorio_pagamentos_pendentes()
	{
		$this->load->model('Coaching_model');
		$this->load->model('Pagamento_model');
		
		$alunos_ativos = $this->Coaching_model->listar_alunos_inscricao_ativa();
		$alunos_chave = $this->Coaching_model->listar_alunos_chave();
		
		$data_inicio = date('Y-m-01', time());
		$data_fim = date('Y-m-t', time());
		$dia_de_hoje = date('d', time());
		
		$pagamentos = $this->Pagamento_model->listar_pagamentos($data_inicio, $data_fim);
		
		$pagamentos_alunos = array();
		foreach ($pagamentos as $pagamento) {
			if(isset($pagamento['aluno'])) {
				array_push($pagamentos_alunos, $pagamento['aluno']);
			}
		}
		
		$result = array();
		foreach ($alunos_ativos as $aluno) {
			foreach ($alunos_chave as $aluno_chave) {
				if($aluno['item_id'] == $aluno_chave['item_id']) {
					continue 2;
				}
			}
			
			if(isset($aluno['referencia_pgto'])) {
				if($aluno['referencia_pgto']->format('Y-m-d') >= date('Y-m-d')) continue; 
				
				$dia_pgto = $aluno['referencia_pgto']->format('d');

				if(!in_array($aluno['item_id'], $pagamentos_alunos)) {
					
					$ultimo_pagamento = $this->Pagamento_model->listar_ultimo_pagamento_de_aluno($aluno['item_id']);
					// compara último pagamento com pagamento do mês passado
					if($ultimo_pagamento < date("Y-m-d", strtotime('-1 month'))) {
					
						$data = array(
							'ppe_id' => $aluno['item_id'],
							'ppe_nome' => $aluno['nome'],
							'ppe_coach' => $aluno['coach'],
							'ppe_turma' => $aluno['area'],
							'ppe_data_pgto' => $dia_pgto,
							'ppe_email' => $aluno['email'],
							'ppe_data_criacao' => get_data_hora_agora(),
							'ppe_ultimo_pagamento' => $ultimo_pagamento
						);
						array_push($result, $data);
					}
				}

			}
		}
		
		$this->Pagamento_model->salvar_pagamentos_pendentes($result);
		
		self::debug_imprimir_alunos($alunos_ativos);
		self::debug_imprimir_pagamentos($pagamentos);
		self::debug_imprimir_pagamentos_pendentes($result);
	}
	
	public function atualizar_combos()
	{
		try {
			$this->load->model('Coaching_model');
			
			$opcoes = array();
			
			/*********************************
			 * Combos App Coachee
			 *********************************/
			$combos_app_coachee = array(
					'escolha-sua-turma-de-coaching',
					'ja-fez-alguma-consultoriacoaching-antes-qualis',
					'como-conheceu-o-exponencial-concursos',
			);

			foreach ($combos_app_coachee as $combo) {
				$campo = PodioAppField::get(PODIO_APP_COACHEE, $combo);

				foreach ($campo->config['settings']['options'] as $option) {
					// Ignora opcoes que foram deletadas. Por algum motivo o Podio as mantém.
					if($option['status'] == 'deleted') {
						continue;
					}

					$dados = array(
							'cco_key' => $combo,
							'cco_valor' => $option['id'],
							'cco_texto' => $option['text']
					);
	
					array_push($opcoes, $dados);
				}
			}
			
			/*********************************
			 * Combos App Coachee Estudo
			 *********************************/
			$combos_app_coachee_estudo = array(
					'selecione-as-materias-em-que-tem-dificuldade',
					'onde-voce-costuma-estudar',
					'selecione-os-tipos-de-materiais-com-que-prefere-estudar',
					'indique-as-tecnicas-de-estudos-que-ja-aplica-ou-aplicou',
					'se-respondeu-sim-acima-indique-as-materias-que-mais-tem',
					'se-fez-a-ultima-prova-em-quais-materias-teve-maior-difi'
			);
				
				
			foreach ($combos_app_coachee_estudo as $combo) {
				$campo = PodioAppField::get(PODIO_APP_COACHEE_ESTUDO, $combo);

				foreach ($campo->config['settings']['options'] as $option) {

					// Ignora opcoes que foram deletadas. Por algum motivo o Podio as mantém.
					if($option['status'] == 'deleted') {
						continue;
					}

					$dados = array(
							'cco_key' => $combo,
							'cco_valor' => $option['id'],
							'cco_texto' => $option['text']
					);
			
					array_push($opcoes, $dados);
				}
			}

			$this->Coaching_model->salvar_opcoes_combo($opcoes);				
				
		} catch(PodioError $pe) {
			error_log($pe->__toString());
		}
	}
	
	/*********************************************************************
	 * Rotinas para atualização dos alunos ativos com cupons e produtos
	 *********************************************************************/
	public function atualizar_alunos_ativos_coaching_cupons_produtos()
	{
		$this->load->model('Coaching_model');
		
		$alunos = $this->Coaching_model->listar_alunos_inscricao_ativa();
		
		$emails = array();
		foreach($alunos as $aluno) {
			array_push($emails, $aluno['email']);
		}
		
		$cupons = listar_cupons_coaching();
		foreach ($cupons as $cupom) {
			$post = get_cupom_by_post_name($cupom['ite_slug']);
			update_post_meta($post->ID, 'customer_email', $emails);
		}
	}
	
	/*********************************************************************
	 * Rotinas para atualização dos grupos de e-mail
	 *********************************************************************/
	public function atualizar_membros_google_groups($grupo = 1)
	{
		$this->load->model('Coaching_model');
		
		switch ($grupo) {
			
			case 1: {
// 				self::deletar_membros_google_group('coaching-ativos@exponencialconcursos.com.br');
				self::atualizar_membros_google_group('coaching-ativos@exponencialconcursos.com.br', $this->Coaching_model->listar_alunos_inscricao_ativa());
				break;
			}
			
			case 2: {
// 				self::deletar_membros_google_group('coaching-exalunos@exponencialconcursos.com.br');
				self::atualizar_membros_google_group('coaching-exalunos@exponencialconcursos.com.br', $this->Coaching_model->listar_alunos_suspensos_ou_ex());
				break;
			}
			
			case 3: {
// 				self::deletar_membros_google_group('coaching-todos@exponencialconcursos.com.br');
				self::atualizar_membros_google_group('coaching-todos@exponencialconcursos.com.br', $this->Coaching_model->listar_todos_alunos());
				break;
			}
			
			case 4: {
// 				self::deletar_membros_google_group('coaching-alunos@exponencialconcursos.com.br');
				self::atualizar_membros_google_group('coaching-alunos@exponencialconcursos.com.br', $this->Coaching_model->listar_alunos_ativos_ou_ex());
				break;
			}
			
			case 5: {
// 				self::deletar_membros_google_group('coaching-inscricao@exponencialconcursos.com.br');
				self::atualizar_membros_google_group('coaching-inscricao@exponencialconcursos.com.br', $this->Coaching_model->listar_alunos_nao_convertidos());
				break;
			}
		}
	}
	
	public function deletar_todos_membros_google_group($grupo)
	{
		$client = self::get_google_client();
		$batch = new Google_Http_Batch($client);
		$dir = new Google_Service_Directory($client);
		
		try	{
			log_google('debug', "******************************************");
			log_google('debug', "Listando grupo: " . $grupo);
			log_google('debug', "******************************************");
			
			$client->setUseBatch(true);
			
			$members = $dir->members->listMembers($grupo, array('maxResults' => 10000));
			$batch->add($members, 1);

			// execute our batch query
			$results = $batch->execute();
			
			// turn off batch queries now so we can query for more members in the case of paginated result sets
			//$client->setUseBatch(False);
			
			
			$batch = new Google_Http_Batch($client);
			
			foreach ($results as $v) {
				$all = $v->getMembers();
				
				foreach ($all as $m) {
					set_time_limit(30);
					$batch->add($dir->members->delete($grupo, $m->email));
				}
				
			}
			
			$batch->execute();
		}
		catch (Exception $gse) {
			print_r($gse);
		}
	
	}
	
	public function deletar_membros_google_group($grupo, $emails)
	{
		$client = self::get_google_client();
		$batch = new Google_Http_Batch($client);
		$dir = new Google_Service_Directory($client);
	
		try	{
			log_google('debug', "******************************************");
			log_google('debug', "Excluindo membros do grupo: " . $grupo);
			log_google('debug', "******************************************");
				
			$client->setUseBatch(true);
				
			$i = 0;
			foreach ($emails as $email) {
				if(($email == 'contato@exponencialconcursos.com.br')
					|| ($email == 'coaching@exponencialconcursos.com.br')) continue;
				
				$i++;
				log_google('debug', "Excluindo [$i] : " . $email);
				$batch->add($dir->members->delete($grupo, $email));
			}
			$batch->execute();
			log_google('debug', "$i e-mails excluídos");
		}
		catch (Exception $gse) {
			print_r($gse);
		}
	
	}
	
	public function listar_membros_google_group($grupo)
	{
		$client = self::get_google_client();
		$batch = new Google_Http_Batch($client);
		$dir = new Google_Service_Directory($client);
	
		try	{
			log_google('debug', "******************************************");
			log_google('debug', "Listando grupo: " . $grupo);
			log_google('debug', "******************************************");
				
			$client->setUseBatch(true);
				
			$members = $dir->members->listMembers($grupo, array('maxResults' => 10000));
			$batch->add($members, 1);
	
			// execute our batch query
			$results = $batch->execute();
				
			$membros = array();
			
			$i = 0;
			foreach ($results as $v) {
				$all = $v->getMembers();
	
				foreach ($all as $m) {
					$i++;
					log_google('debug', "Adicionando ao Array Google [$i] : " . $m->email);
					array_push($membros, $m->email);
				}
	
			}
			log_google('debug', "$i e-mail(s) adicionados ao Array Google");
			return $membros;
		}
		catch (Exception $gse) {
			print_r($gse);
			return null;
		}
	
	}
	
	private function atualizar_membros_google_group($grupo, $usuarios)
	{
		set_time_limit(600);
		
		log_google('debug', "******************************************");
		log_google('debug', "Atualizando grupo: {$grupo}: " . count($usuarios) . " usuários.");
		log_google('debug', "******************************************");
		
		$podio_array = array();
		foreach ($usuarios as $usuario) {
			$email = explode(",", $usuario['email']);
			$email = explode(";", $email[0]);
			$email = $email[0];
			
			array_push($podio_array, $email);
		}
		
		$google_array = self::listar_membros_google_group($grupo);
		
		$remover_array = array_diff($google_array, $podio_array);
		$adicionar_array = array_diff($podio_array, $google_array);
		
		if($remover_array) {
			self::deletar_membros_google_group($grupo, $remover_array);
		}
		
		if($adicionar_array) {
			self::adicionar_membros_google_group($grupo, $adicionar_array);
		}
		
	}
	
	private function adicionar_membros_google_group($grupo, $emails)
	{
		log_google('debug', "******************************************");
		log_google('debug', "Adicionar membros ao grupo: {$grupo}: +" . count($usuarios) . " usuários.");
		log_google('debug', "******************************************");
	
		$client = self::get_google_client();
		$client->setUseBatch(true);
	
		$batch = new Google_Http_Batch($client);
		$directoryApi = new Google_Service_Directory($client);
	
		$i = 0;
		foreach($emails as $email) {				
			$member = new Google_Service_Directory_Member();
			$member->setEmail($email);
			$member->setRole('MEMBER');
			$member->setType('USER');
				
			try	{
				$i++;
				log_google('debug', "Inserindo e-mail [$i]: " . $email);
				$batch->add($directoryApi->members->insert($grupo, $member));
			}
			catch (Google_Service_Exception $gse) {
				log_google('debug', "Oops... Já existe esse e-mail: " . $email);
				continue; // Se membro já existir captura exceção e continua looping
			}
		}
		
		$client->execute($batch);
		log_google('debug', "Adicionados $i novos e-mails");
	}
	
	private function get_google_client()
	{
		log_google('debug', "Recuperando google client");
		set_include_path(APPPATH . 'libraries/google-api-php-client' . PATH_SEPARATOR . get_include_path());
		require $_SERVER['DOCUMENT_ROOT'] . '/painel-coaching/app-expocoaching/libraries/google-api-php-client/autoload.php';
		
		$clientId = '101565395308386882909';
		$serviceAccountName = 'main-111@exponencial-1250.iam.gserviceaccount.com';
		$delegatedAdmin = 'leonardo.coelho@exponencialconcursos.com.br';
		$keyFile = $_SERVER['DOCUMENT_ROOT'] . '/painel-coaching/app-expocoaching/libraries/google-api-php-client/Exponencial-99ae5030606b.p12';
		$appName = '101565395308386882909';
		
		$scopes = array(
				'https://www.googleapis.com/auth/admin.directory.user',
				'https://www.googleapis.com/auth/admin.directory.group',
				'https://www.googleapis.com/auth/admin.directory.group.readonly',
				'https://www.googleapis.com/auth/apps.groups.settings',
				'https://www.googleapis.com/auth/admin.directory.group.member',
				'https://www.googleapis.com/auth/admin.directory.group.member.readonly'
		);
		
		$creds = new Google_Auth_AssertionCredentials(
				$serviceAccountName,
				$scopes,
				file_get_contents($keyFile)
		);
		
		$creds->sub = $delegatedAdmin;
		
		$client = new Google_Client();
		$client->setUseBatch(true);
		$client->setApplicationName($appName);
		$client->setClientId($clientId);
		
		if ($client->getAuth()->isAccessTokenExpired()) {
			$client->getAuth()->refreshTokenWithAssertion($creds);
		}
		
		return $client;
	}
	
	private function debug_imprimir_alunos($items)
	{
		echo "****************************<br>";
		echo "* Alunos (" . count($items) .")<br>";
		echo "****************************<br>";
		
		foreach ($items as $item) {
			echo $item['item_id'] . ' : ' . $item['nome'] . '<br>';
		}
	}
	
	private function debug_imprimir_pagamentos($items)
	{
		echo "****************************<br>";
		echo "* Pagamentos do período (" . count($items) .")<br>";
		echo "****************************<br>";
		
		foreach ($items as $item) {
			echo $item['aluno'] . ' : ' . $item['mes_de_referencia']->format('Y-m-d') . '<br>';
		}
	}
	
	private function debug_imprimir_pagamentos_pendentes($items)
	{
		echo "****************************<br>";
		echo "* Pagamentos pendentes (" . count($items) .")<br>";
		echo "****************************<br>";
		
		foreach ($items as $item) {
			echo $item['ppe_id'] . ' : ' . $item['ppe_nome'] . ' : ' . $item['ppe_data_pgto'] . '<br>';
		}
	}
	
	private function debug_imprimir_emails($items)
	{
		echo "****************************<br>";
		echo "* E-mails enviados (" . count($items) .")<br>";
		echo "****************************<br>";
	
		foreach ($items as $item) {
			echo $item['item_id'] . ' : ' . $item['nome'] . '<br>';
		}
	}
}
