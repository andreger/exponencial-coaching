<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorios extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Sao_Paulo');
		
		set_time_limit(120);
	}
	
	public function inscricoes()
	{
		session_start();
		
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_COACHING
		), ACESSO_NEGADO);

		$this->load->model('Coaching_model');
		
		$data['alunos'] = $this->Coaching_model->listar_alunos_inscricao_em_andamento();
		
		$this->load->view('relatorios/inscricoes', $data);
	}
	
	public function pagamentos_pendentes()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_COACHING
		), ACESSO_NEGADO);

// 		$this->output->cache(60);
		
		$this->load->model('Pagamento_model');
		
		$pagamentos_pendentes = $this->Pagamento_model->listar_pagamentos_pendentes();
		
		$data['pagamentos_pendentes'] = $pagamentos_pendentes;
		$data['data_relatorio'] = converter_para_ddmmyyyy_HHii($pagamentos_pendentes[0]['ppe_data_criacao']);
		$this->load->view('relatorios/pagamentos_pendentes', $data);	
	}
	
	public function pagamentos_do_mes()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_COACHING
		), ACESSO_NEGADO);

// 		$this->output->cache(60);
		
		$this->load->model('Pagamento_model');
		
		$data['pagamentos'] = $this->Pagamento_model->listar_pagamentos_do_mes();
		
		foreach ($data['pagamentos'] as $key => $row) {
			$area[$key]  = slugify($row['area']);
			$nome[$key] = slugify($row['nome']);
		}
			
		array_multisort($area, SORT_ASC, $nome, SORT_ASC, $data['pagamentos']);
		
		$this->load->view('relatorios/pagamentos_do_mes', $data);
	}
	
	public function exportar_pagamentos_pendentes()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_COACHING
		), ACESSO_NEGADO);

		$this->output->cache(60);
		
		$this->load->model('Pagamento_model');
		
		$pagamentos_pendentes = $this->Pagamento_model->listar_pagamentos_pendentes();
		
		$table_result = get_pagamentos_pendentes_table($pagamentos_pendentes);
		$table_result = str_replace("<table", "<table style='border-collapse:collapse; border: 1px solid black;'", $table_result);
		$table_result = str_replace("<tr", "<tr style='border: 1px solid black;'", $table_result);
		$table_result = str_replace("<td", "<td style='border: 1px solid black;'", $table_result);
		
		$list = get_html_translation_table(HTML_ENTITIES);
		unset($list['"']);
		unset($list['<']);
		unset($list['>']);
		unset($list['&']);
		
		$search = array_keys($list);
		$values = array_values($list);
		$search = array_map('utf8_encode', $search);
		
		$data['out'] =  str_replace($search, $values, $table_result);
		
		$this->load->view('relatorios/exportar_pagamentos_pendentes', $data);
	}
	
}
