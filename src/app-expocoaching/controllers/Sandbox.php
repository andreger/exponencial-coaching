<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sandbox extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Sao_Paulo');
		
		$this->load->library('podio-php-4.0.1/Podio');
		try {
			Podio::setup(PODIO_CLIENT_COACHEE, PODIO_CLIENT_COACHEE_SECRET);
			Podio::authenticate_with_app(PODIO_APP_COACHEE, PODIO_APP_COACHEE_TOKEN);
		}
		catch(PodioError $pe) {
			error_log($pe->__toString());
		}
	}
	
	public function atualizar_combos()
	{
		try {
			$combos = array(
				'escolha-sua-turma-de-coaching',
			);
			
			$opcoes = array();
			foreach ($combos as $combo) {
				$campo = PodioAppField::get(PODIO_APP_COACHEE, $combo);
				
				foreach ($campo->config['settings']['options'] as $option) {
					$dados = array(
						'cco_key' => $combo,
						'cco_valor' => $option['id'],
						'cco_texto' => $option['text']
					);
										
					array_push($opcoes, $dados);
				} 
			}
			
			
			
		} catch(PodioError $pe) {
			error_log($pe->__toString());
		} 
		echo "";
	}
	
	public function google_groups()
	{		
		set_include_path(APPPATH . 'libraries/google-api-php-client' . PATH_SEPARATOR . get_include_path());
		require $_SERVER['DOCUMENT_ROOT'] . '/painel-coaching/app-expocoaching/libraries/google-api-php-client/autoload.php';
		
		$clientId = '101565395308386882909';
		
		$serviceAccountName = 'main-111@exponencial-1250.iam.gserviceaccount.com';
		
		$delegatedAdmin = 'leonardo.coelho@exponencialconcursos.com.br';
		
		$keyFile = $_SERVER['DOCUMENT_ROOT'] . '/painel-coaching/app-expocoaching/libraries/google-api-php-client/Exponencial-99ae5030606b.p12';
		
		$appName = '101565395308386882909';
		
		$scopes = array(
				'https://www.googleapis.com/auth/admin.directory.user',
				'https://www.googleapis.com/auth/admin.directory.group',
				'https://www.googleapis.com/auth/apps.groups.settings',
				'https://www.googleapis.com/auth/admin.directory.group.member'
		);
		
		$creds = new Google_Auth_AssertionCredentials(
				$serviceAccountName,
				$scopes,
				file_get_contents($keyFile)
		);
		
		$creds->sub = $delegatedAdmin;
		
		$client = new Google_Client();
		$client->setApplicationName($appName);
		$client->setClientId($clientId);
		if ($client->getAuth()->isAccessTokenExpired()) {
		    $client->getAuth()->refreshTokenWithAssertion($creds);
		}
		
		$dir = new Google_Service_Directory($client);
		
		$member = new Google_Service_Directory_Member(array(
				'email' => 'andreger@gmail.com',
				'kind' => 'admin#directory#member',
				'role' => 'MEMBER',
				'type' => 'USER'));
			
		try
		{
			$list = $dir->members->insert('coaching-ativos@exponencialconcursos.com.br', $member);
			var_dump($list);
		}
		catch (Google_Service_Exception $gse)
		{
			echo "<pre>";
			print_r($gse->getMessage());
		}
	}
	
}
