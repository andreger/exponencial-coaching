<?php
defined('BASEPATH') OR exit('No direct script access allowed');

KLoader::helper("AcessoGrupoHelper");

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		date_default_timezone_set('America/Sao_Paulo');

		$this->load->helper("combo");
		$this->load->helper("meta");
		$this->load->helper("admin");
		$this->load->helper("server");
		
		$this->load->model("Assunto_model");
		$this->load->model("Caderno_model");
		$this->load->model("Disciplina_model");
		$this->load->model("Meta_model");
		$this->load->model("Meta_agregada_model");
		
		init_profiler();	
	}

	public function index() 
	{
		self::listar_metas_atomicas();
	}
	
	/**
	 * Centraliza a busca de metas atômicas e agregadas
	 * 
	 * @since L1
	 * 
	 * @param string $busca_sessao_str
	 * @param int $offset
	 * @param Object $model
	 * @return array contendo os dados da busca
	 */
	private function listar_metas($busca_sessao_str, &$model, $offset, $limit = LISTAR_METAS_MAX)
	{
	    
	    // Inicialização da sessão
	    if(!$_SESSION[$busca_sessao_str]) {
	        $_SESSION[$busca_sessao_str] = [
	            'nome_selecionado' => null
	        ];
	    }
	    // Ação pós clique no botão Filtrar
	    if($this->input->post("buscar")){
	        
	        $_SESSION[$busca_sessao_str]['nome_selecionado'] = $this->input->post("nome");
	        $_SESSION[$busca_sessao_str]['assuntos_selecionados'] = $this->input->post("assuntos_ids");
	        $_SESSION[$busca_sessao_str]['consultores_selecionados'] = $this->input->post("consultores_ids");
	        $_SESSION[$busca_sessao_str]['disciplinas_selecionadas'] = $this->input->post("disciplinas_ids");
	        $_SESSION[$busca_sessao_str]['hbc_de'] = $this->input->post("hbc_de");
	        $_SESSION[$busca_sessao_str]['hbc_ate'] = $this->input->post("hbc_ate");
	        $_SESSION[$busca_sessao_str]['data_criacao_de'] = $this->input->post("data_criacao_de");
	        $_SESSION[$busca_sessao_str]['data_criacao_ate'] = $this->input->post("data_criacao_ate");
	        $_SESSION[$busca_sessao_str]['data_edicao_de'] = $this->input->post("data_edicao_de");
	        $_SESSION[$busca_sessao_str]['data_edicao_ate'] = $this->input->post("data_edicao_ate");
	        
	        if($met_ids = $this->input->post('met_ids')) {
	            $met_ids = json_decode(stripslashes($met_ids), true);
	            $_SESSION[$busca_sessao_str]['ignorar_metas_atomicas'] = array();
	            foreach ($met_ids as $met_id)
	            {
	                $_SESSION[$busca_sessao_str]['ignorar_metas_atomicas'][] = $met_id['met_id'];
	            }
	        }
	        
	    }
	    
	    $filtro['nome_selecionado'] = $_SESSION[$busca_sessao_str]['nome_selecionado'];
	    $filtro['assuntos_selecionados'] = $_SESSION[$busca_sessao_str]['assuntos_selecionados'];
	    $filtro['consultores_selecionados'] = $_SESSION[$busca_sessao_str]['consultores_selecionados'];
	    $filtro['disciplinas_selecionadas'] = $_SESSION[$busca_sessao_str]['disciplinas_selecionadas'];
	    $filtro['hbc_de'] = $_SESSION[$busca_sessao_str]['hbc_de'];
	    $filtro['hbc_ate'] = $_SESSION[$busca_sessao_str]['hbc_ate'];
	    $filtro['data_criacao_de'] = $_SESSION[$busca_sessao_str]['data_criacao_de'];
	    $filtro['data_criacao_ate'] = $_SESSION[$busca_sessao_str]['data_criacao_ate'];
	    $filtro['data_edicao_de'] = $_SESSION[$busca_sessao_str]['data_edicao_de'];
	    $filtro['data_edicao_ate'] = $_SESSION[$busca_sessao_str]['data_edicao_ate'];
	    $filtro['ignorar_metas_atomicas'] = $_SESSION[$busca_sessao_str]['ignorar_metas_atomicas'];
	    $filtro["limit"] = $limit;
	    $filtro["offset"] = $offset;
	    
	    $result['metas'] = $model->filtrar($filtro);
	    $result['total'] = $model->contar($filtro);
	    $result['filtro'] = $filtro;
	    
	    return $result;
	}
	
	public function listar_metas_agregadas($offset = 0)
	{
	    AcessoGrupoHelper::coaching_metas(ACESSO_NEGADO);
	    
	    // inclusão de bibliotecas
	    $data['include_chosen'] = true;
	    $data["include_mask"] = true;
	    $data["include_datepicker"] = true;
	    
	    $data['js'] = [
	        '/painel-coaching/assets-admin/js/paginas/listar_metas.js'
	    ];
	    
	    $resultado_busca = self::listar_metas('frm_listar_metas_agregadas', $this->Meta_agregada_model, $offset);
	    
	    $filtro = $resultado_busca['filtro'];
	    $metas = $resultado_busca['metas'];
	    $total = $resultado_busca['total'];
	    
	    foreach ($metas as &$item) {
	        $item = $this->Meta_agregada_model->get_by_id($item['mea_id']);
	        
	        $criador = get_usuario_array($item['mea_criador_id']);
	        $item['criador_nome'] = $criador['nome_completo'];
	        
	        $editor = get_usuario_array($item['mea_editor_id']);
	        $item['editor_nome'] = $editor['nome_completo'];
	        
	        $areas = $this->Meta_agregada_model->listar_areas($item['mea_id']);
	        $item['areas'] = get_array_str($areas, "name");
	        
	        $item['editar_url'] = editar_meta_agregada_url($item['mea_id']);
	    }
	    
	    $data['usuario_logado_id'] = get_current_user_id();
	    $data['metas'] = $metas;
	    $data['titulo'] = "Metas";
	    $data['assuntos'] = [];
	    $data['filtro'] = $filtro;
	    
	    if($filtro['disciplinas_selecionadas']){
	        $data['assuntos'] = get_combo($this->Assunto_model->listar_por_disciplinas($filtro['disciplinas_selecionadas']), "ass_id", "ass_nome");
	    }
	    $data['consultores'] = get_combo(get_consultores_array(), "id", "nome_completo");
	    $data['disciplinas'] = get_combo($this->Disciplina_model->listar(), "dis_id", "dis_nome");
	    
	    $segmento = 3;
	    $config = pagination_config("/painel-coaching/admin/listar_metas_agregadas", $total, $filtro['limit'], $segmento);
	    $this->pagination->initialize($config);
	    
	    
	    $data["links"] = $this->pagination->create_links();
	    
	    $this->load->view('common/header', $data);
	    $this->load->view('admin/listar_metas_agregadas', $data);
	    $this->load->view('common/footer', $data);
	}

	public function listar_metas_atomicas($offset = 0)
	{
	    AcessoGrupoHelper::coaching_metas(ACESSO_NEGADO);

		// inclusão de bibliotecas
		$data['include_chosen'] = true;
		$data["include_mask"] = true;
		$data["include_datepicker"] = true;
		
		$data['js'] = [
			'/painel-coaching/assets-admin/js/paginas/listar_metas.js'
		];
		
		$resultado_busca = self::listar_metas('frm_listar_metas', $this->Meta_model, $offset);
		
		$filtro = $resultado_busca['filtro'];
		$metas = $resultado_busca['metas'];
		$total = $resultado_busca['total'];
		
		foreach ($metas as &$item) {
		    $item = $this->Meta_model->get_by_id($item['met_id']);
		    
		    $criador = get_usuario_array($item['usu_id']);
		    $item['criador_nome'] = $criador['nome_completo'];
		    
		    $editor = get_usuario_array($item['met_editor_id']);
		    $item['editor_nome'] = $editor['nome_completo'];
		    
		    $disciplinas = $this->Meta_model->listar_disciplinas($item['met_id']);
		    $item['disciplinas'] = get_array_str($disciplinas, "dis_nome");
		    
		    $areas = $this->Meta_model->listar_areas($item['met_id']);
		    $item['areas'] = get_array_str($areas, "name");
		    
		    $item['editar_url'] = editar_meta_atomica_url($item['met_id']);
		}

		$data['usuario_logado_id'] = get_current_user_id();
		$data['metas'] = $metas;
		$data['titulo'] = "Metas";
		$data['assuntos'] = [];
		$data['filtro'] = $filtro;
		
		if($filtro['disciplinas_selecionadas']){
			$data['assuntos'] = get_combo($this->Assunto_model->listar_por_disciplinas($filtro['disciplinas_selecionadas']), "ass_id", "ass_nome");
		}
		$data['consultores'] = get_combo(get_consultores_array(), "id", "nome_completo");
		$data['disciplinas'] = get_combo($this->Disciplina_model->listar(), "dis_id", "dis_nome");

		$segmento = 3;
		$config = pagination_config("/painel-coaching/admin/listar_metas_atomicas", $total, $filtro['limit'], $segmento);
		$this->pagination->initialize($config);
		
		
		$data["links"] = $this->pagination->create_links();

		$this->load->view('common/header', $data);
		$this->load->view('admin/listar_metas_atomicas', $data);
		$this->load->view('common/footer', $data);
	}

	public function editar_meta_atomica($id = NULL)
	{
	    AcessoGrupoHelper::coaching_metas(ACESSO_NEGADO);

		$data["include_summernote"] = true;
		$data["include_chosen"] = true;
		$data["include_mask"] = true;
		$data['include_jstree'] = true;
		
		$data['js'] = ['/painel-coaching/assets-admin/js/paginas/editar_meta.js'];

		KLoader::helper('ConverterHelper');
		KLoader::model('AreaModel');

		$usuario_logado_id = get_current_user_id();

		if ($this->input->post('salvar')) {
		
			// define as regras de validação do formulário
			$this->form_validation->set_rules('met_nome', 'Nome', "required|max_length[200]");
			$this->form_validation->set_rules('met_hbc', 'HBC', "callback_hbc_check");

			$this->form_validation->set_message('required', '%s é obrigatório.');
			$this->form_validation->set_message('max_length', '%s não pode passar de %d caracteres.');

			if ($this->form_validation->run() === true)
			{				
				// salva dados básicos
				$meta_coach = [
					'met_id' => $id ?: NULL,
					'met_nome' => $this->input->post('met_nome'),
					'met_orientacoes' => $this->input->post('met_orientacao'),
				    'met_hbc' => $this->input->post('met_hbc'),
					'met_anotacoes' => $this->input->post('met_anotacao'),
				];

				$meta_id = $this->Meta_model->salvar($meta_coach);

				// salva disciplinas
				if($dis_ids = $this->input->post('dis_ids')) {
					$this->Meta_model->excluir_disciplinas($meta_id);
					$this->Meta_model->adicionar_disciplinas($meta_id, $dis_ids);
				}

				// salva assuntos
				if($ass_ids = $this->input->post('ass_ids')) {
					$this->Meta_model->excluir_assuntos($meta_id);
					$this->Meta_model->adicionar_assuntos($meta_id, $ass_ids);
				}

				// salva cadernos
				if($cad_ids = $this->input->post('cad_ids')) {
					$this->Meta_model->excluir_cadernos($meta_id);
					$this->Meta_model->adicionar_cadernos($meta_id, $cad_ids);
				}

				// salva áreas
				if($are_ids = $this->input->post('are_ids')) {
					$this->Meta_model->excluir_areas($meta_id);
					$this->Meta_model->adicionar_areas($meta_id, $are_ids);
				}
				
				//Atualiza o HBC agregado das metas agregadas associadas a essa meta atômica
				$metas_agregadas = $this->Meta_agregada_model->listar_por_meta_atomica($meta_id);
				foreach($metas_agregadas as $meta_agregada)
				{
				    $this->Meta_agregada_model->atualizar_hbc_agregado($meta_agregada['mea_id']);
				}

				set_mensagem_flash( 'sucesso', 'Os dados da meta atômica foram gravados com sucesso!' );
				redirect( get_listar_metas_atomicas_url() );
			}

		}

		$data['usuario_logado_id'] = $usuario_logado_id;
		$data['titulo'] = $id ? "Editar Meta" : "Nova Meta";		

		$meta = $id ? $this->Meta_model->get_by_id($id) : null;

		if($id && !is_administrador() && !is_coordenador_coaching() && $meta['usu_id'] != $usuario_logado_id){
			redirecionar_para_acesso_negado();
			exit;
		}
		
		$data['met_id'] = $meta ? $meta['met_id'] : "";;
		$data['met_nome'] = $meta ? $meta['met_nome'] : "";
		$data['met_disciplinas'] = $meta ? array_column($this->Meta_model->listar_disciplinas($id), 'dis_id') : "";
		$data['met_assuntos'] = $meta ? array_column($this->Meta_model->listar_assuntos($id), 'ass_id') : "";
		$data['met_areas'] = $meta ? array_column($this->Meta_model->listar_areas($id), 'are_id') : "";
		$data['met_cadernos'] = $meta ? array_column($this->Meta_model->listar_cadernos($id), 'cad_id') : "";
		$data['met_orientacao'] = $meta ? stripslashes($meta['met_orientacoes']) : "";
		$data['met_anotacao'] = $meta ? $meta['met_anotacoes'] : "";
		$data['met_hbc'] = $meta ? $meta['met_hbc'] : "";

		$data['disciplinas'] = get_combo($this->Disciplina_model->listar(), "dis_id", "dis_nome");
		$data['cadernos'] = get_combo($this->Caderno_model->listar($usuario_logado_id), "cad_id", "cad_nome");

		$data['areas'] = get_combo(ConverterHelper::object_to_array(AreaModel::listar()), "term_id", "name");

		$dis_ids = NULL;
		if($this->input->post('met_disciplinas')) {
			$dis_ids = $this->input->post('met_disciplinas');
		}
		elseif($data['met_disciplinas']) {
			$dis_ids = $data['met_disciplinas'];
		}

		$data['assuntos'] = $dis_ids ? get_combo($this->Assunto_model->listar_por_disciplinas($dis_ids), "ass_id", "ass_nome") : [];

		$this->load->view('common/header', $data);
		$this->load->view('admin/editar_meta_atomica', $data);
		$this->load->view('common/footer',$data);
	}
	
	public function excluir_meta_atomica($meta_id)
	{
	    AcessoGrupoHelper::coaching_metas(ACESSO_NEGADO);

		$meta = $this->Meta_model->get_by_id($meta_id);
		
		$usuario_logado_id = get_current_user_id();
		
		if($meta && !is_administrador() && !is_coordenador_coaching() && $meta['usu_id'] != $usuario_logado_id){
			redirecionar_para_acesso_negado();
			exit;
		}

		$this->Meta_model->excluir_meta($meta_id);

		set_mensagem_flash( 'sucesso', 'A meta foi excluída com sucesso!' );
		redirect ( get_listar_metas_atomicas_url ());

	}

	public function imprimir_meta_atomica($id)
	{
	    AcessoGrupoHelper::coaching_metas(ACESSO_NEGADO);
	    
		KLoader::helper('ConverterHelper');
		KLoader::model('AreaModel');

		$usuario_logado_id = get_current_user_id();

		$data['usuario_logado_id'] = $usuario_logado_id;
		$data['titulo'] = "Meta";		

		$meta = $id ? $this->Meta_model->get_by_id($id) : null;

		$data['met_nome'] = $meta ? $meta['met_nome'] : "";
		$data['met_disciplinas'] = $meta ? implode(", ", array_column($this->Meta_model->listar_disciplinas($id), 'dis_nome')) : "";
		$data['met_assuntos'] = $meta ? implode(", ", array_column($this->Meta_model->listar_assuntos($id), 'ass_nome')) : "";
		$data['met_areas'] = $meta ? implode(", ", array_column($this->Meta_model->listar_areas($id), 'are_nome')) : "";
		$data['met_cadernos'] = $meta ? implode(", ", array_column($this->Meta_model->listar_cadernos($id), 'cad_nome')) : "";
		$data['met_orientacao'] = $meta ? stripslashes($meta['met_orientacoes']) : "";
		$data['met_anotacao'] = $meta ? $meta['met_anotacoes'] : "";
		$data['met_hbc'] = $meta ? $meta['met_hbc'] : "";

		$this->load->view('admin/imprimir_meta_atomica', $data);
	}
	
	public function duplicar_meta_atomica($meta_id)
	{
	    AcessoGrupoHelper::coaching_metas(ACESSO_NEGADO);
	    
	    $meta = $this->Meta_model->get_by_id($meta_id);
	    
	    $meta["met_id"] = null;
	    $meta["met_nome"] .= " (Cópia)";
	    
	    $nova_meta_id = $this->Meta_model->salvar($meta);
	    
	    // salva disciplinas
	    if($nova_meta_dis_ids = array_column($this->Meta_model->listar_disciplinas($meta_id), 'dis_id')) {
	        $this->Meta_model->adicionar_disciplinas($nova_meta_id, $nova_meta_dis_ids);
	    }
	    
	    // salva assuntos
	    if($nova_meta_ass_ids = array_column($this->Meta_model->listar_assuntos($meta_id), 'ass_id')) {
	        $this->Meta_model->adicionar_assuntos($nova_meta_id, $nova_meta_ass_ids);
	    }
	    
	    // salva cadernos
	    if($nova_meta_cad_ids = array_column($this->Meta_model->listar_cadernos($meta_id), 'cad_id')) {
	        $this->Meta_model->adicionar_cadernos($nova_meta_id, $nova_meta_cad_ids);
	    }
	    
	    // salva áreas
	    if($nova_meta_are_ids = array_column($this->Meta_model->listar_areas($meta_id), 'are_id')) {
	        $this->Meta_model->adicionar_areas($nova_meta_id, $nova_meta_are_ids);
	    }
	    
	    set_mensagem_flash( 'sucesso', 'A meta atômica foi duplicada com sucesso!' );
	    redirect( get_listar_metas_atomicas_url() );
	}
	
	public function editar_meta_agregada($id = NULL)
	{
	    AcessoGrupoHelper::coaching_metas(ACESSO_NEGADO);
	    
	    $data["include_summernote"] = true;
		$data["include_chosen"] = true;
		$data["include_datepicker"] = true;
	    $data["include_mask"] = true;
	    $data['include_jstree'] = true;
	    $data['include_nestable'] = true;
	    
	    $data['js'] = [
	                    '/painel-coaching/assets-admin/js/paginas/editar_meta.js',
            	        '/painel-coaching/assets-admin/js/paginas/listar_metas.js'
            	    ];
	    
	    KLoader::helper('ConverterHelper');
	    KLoader::model('AreaModel');
	    
	    $usuario_logado_id = get_current_user_id();
	    
	    $data['usuario_logado_id'] = $usuario_logado_id;
	    
	    $meta = $id ? $this->Meta_agregada_model->get_by_id($id) : null;
	    
	    if($id && !is_administrador() && !is_coordenador_coaching() && $meta['usu_id'] != $usuario_logado_id){
	        redirecionar_para_acesso_negado();
	        exit;
	    }
	    
	    if ($this->input->post('salvar')) {
	        
	        // define as regras de validação do formulário
	        $this->form_validation->set_rules('mea_nome', 'Nome', "required|max_length[200]");
	        
	        $this->form_validation->set_message('required', '%s é obrigatório.');
	        $this->form_validation->set_message('max_length', '%s não pode passar de %d caracteres.');
	        
	        if ($this->form_validation->run() === true)
	        {
	            // salva dados básicos
	            $meta_coach = [
	                'mea_id' => $id ?: NULL,
	                'mea_nome' => $this->input->post('mea_nome'),
	                'mea_anotacoes' => $this->input->post('mea_anotacoes'),
	            ];
	            
	            $meta_id = $this->Meta_agregada_model->salvar($meta_coach);
	            
	            // salva metas atômicas
	            if($met_ids = $this->input->post('met_ids')) {
	                $met_ids = json_decode(stripslashes($met_ids), true);
	                $this->Meta_agregada_model->excluir_metas_atomicas($meta_id);
	                $this->Meta_agregada_model->adicionar_metas_atomicas($meta_id, $met_ids);
	            }
	            
	            // salva áreas
	            if($are_ids = $this->input->post('are_ids')) {
	                $this->Meta_agregada_model->excluir_areas($meta_id);
	                $this->Meta_agregada_model->adicionar_areas($meta_id, $are_ids);
	            }
	            
	            $this->Meta_agregada_model->atualizar_hbc_agregado($meta_id);
	            
	            set_mensagem_flash( 'sucesso', 'Os dados da meta agregada foram gravados com sucesso!' );
	            redirect( get_listar_metas_agregadas_url() );
	        }
	        
	    }
	    elseif($this->input->post('buscar'))
	    {
	        $resultado_busca = self::listar_metas('frm_editar_meta_agregada', $this->Meta_model, 0, null);
	        
	        $filtro = $resultado_busca['filtro'];
	        $metas_busca = $resultado_busca['metas'];
	        $total = $resultado_busca['total'];
	        
	        foreach ($metas_busca as &$item) {
	            $item = $this->Meta_model->get_by_id($item['met_id']);
	            
	            $criador = get_usuario_array($item['usu_id']);
	            $item['criador_nome'] = $criador['nome_completo'];
	            
	            $disciplinas = $this->Meta_model->listar_disciplinas($item['met_id']);
	            $item['disciplinas'] = get_array_str($disciplinas, "dis_nome");
	            
	            $areas = $this->Meta_model->listar_areas($item['met_id']);
	            $item['areas'] = get_array_str($areas, "name");
	        }
	        
	        $data['filtro'] = $filtro;
	        $data['metas_busca'] = $metas_busca;
	        $data['total'] = $total;
	        
	        if($met_ids = $this->input->post('met_ids')) {
	            $met_ids = json_decode(stripslashes($met_ids), true);
	            $meta['mea_metas_atomicas'] = array();
	            $meta['mea_met_ids'] = array();
	            foreach ($met_ids as $met_id)
	            {
	                $meta['mea_metas_atomicas'][] = $this->Meta_model->get_by_id($met_id['met_id']);
	                $meta['mea_met_ids'][] = ['met_id' => $met_id['met_id']];
	            }
	        }
	        
	        $data['is_exibir_busca'] = true;
	    }
	    else {
	        $meta['mea_metas_atomicas'] = $meta ? $this->Meta_model->listar_por_meta_agregada($id) : null;
	        foreach($meta['mea_metas_atomicas'] as $meta_atomica)
	        {
	            $meta['mea_met_ids'][] = ['met_id' => $meta_atomica['met_id']];
	        }
	    }
	    
	    $meta['mea_areas'] = $meta ? array_column($this->Meta_agregada_model->listar_areas($id), 'are_id') : "";
	    
	    $data['meta'] = $meta;
	    
		$data['areas'] = get_combo(ConverterHelper::object_to_array(AreaModel::listar()), "term_id", "name");
		
		if($data['filtro']['disciplinas_selecionadas']){
			$data['assuntos'] = get_combo($this->Assunto_model->listar_por_disciplinas($filtro['disciplinas_selecionadas']), "ass_id", "ass_nome");
		}

		$data['consultores'] = get_combo(get_consultores_array(), "id", "nome_completo");
		$data['disciplinas'] = get_combo($this->Disciplina_model->listar(), "dis_id", "dis_nome");
	    
	    $this->load->view('common/header', $data);
	    $this->load->view('admin/editar_meta_agregada', $data);
	    $this->load->view('common/footer',$data);
	}
	
	public function excluir_meta_agregada($meta_id)
	{
	    AcessoGrupoHelper::coaching_metas(ACESSO_NEGADO);
	    
	    $meta = $this->Meta_agregada_model->get_by_id($meta_id);
	    
	    $usuario_logado_id = get_current_user_id();
	    
	    if($meta && !is_administrador() && !is_coordenador_coaching() && $meta['mea_criador_id'] != $usuario_logado_id){
	        redirecionar_para_acesso_negado();
	        exit;
	    }
	    
	    $this->Meta_agregada_model->excluir_meta_agregada($meta_id);
	    
	    set_mensagem_flash( 'sucesso', 'A meta agregada foi excluída com sucesso!' );
	    redirect ( get_listar_metas_agregadas_url ());
	    
	}
	
	public function imprimir_meta_agregada($id)
	{
	    AcessoGrupoHelper::coaching_metas(ACESSO_NEGADO);
	    
	    KLoader::helper('ConverterHelper');
	    KLoader::model('AreaModel');
	    
	    $usuario_logado_id = get_current_user_id();
	    
	    $data['usuario_logado_id'] = $usuario_logado_id;
	    $data['titulo'] = "Meta";
	    
	    $meta = $id ? $this->Meta_agregada_model->get_by_id($id) : null;
	    
	    $metas_atomicas = $this->Meta_model->listar_por_meta_agregada($meta['mea_id']);

	    $data['metas_atomicas'] = array();
	    
	    foreach ($metas_atomicas as $meta)
	    {
	        $meta_atomica = array();	        
	        $meta_atomica['met_nome'] = $meta['met_nome'];
	        $meta_atomica['met_disciplinas'] = implode(", ", array_column($this->Meta_model->listar_disciplinas($meta['met_id']), 'dis_nome'));
	        $meta_atomica['met_assuntos'] = implode(", ", array_column($this->Meta_model->listar_assuntos($meta['met_id']), 'ass_nome'));
	        $meta_atomica['met_areas'] = implode(", ", array_column($this->Meta_model->listar_areas($meta['met_id']), 'are_nome'));
	        $meta_atomica['met_cadernos'] = implode(", ", array_column($this->Meta_model->listar_cadernos($meta['met_id']), 'cad_nome'));
	        $meta_atomica['met_orientacao'] = stripslashes($meta['met_orientacoes']);
	        $meta_atomica['met_anotacao'] = $meta['met_anotacoes'];
	        $meta_atomica['met_hbc'] = $meta['met_hbc'];
    	    
    	    array_push($data['metas_atomicas'], $meta_atomica);
	    }
	    
	    $this->load->view('admin/imprimir_meta_agregada', $data);
	}
	
	public function duplicar_meta_agregada($meta_id)
	{
	    AcessoGrupoHelper::coaching_metas(ACESSO_NEGADO);
	    
	    $meta = $this->Meta_agregada_model->get_by_id($meta_id);
	    
	    $meta["mea_id"] = null;
	    $meta["mea_nome"] .= " (Cópia)";
	    
	    $nova_meta_id = $this->Meta_agregada_model->salvar($meta);
	    
	    // salva metas atômicas
	    if($nova_met_ids = $this->Meta_model->listar_por_meta_agregada($meta_id)) {
	        $this->Meta_agregada_model->adicionar_metas_atomicas($nova_meta_id, $nova_met_ids);
	    }
	    
	    // salva áreas
	    if($nova_are_ids = array_column($this->Meta_agregada_model->listar_areas($meta_id), 'are_id')) {
	        $this->Meta_agregada_model->adicionar_areas($nova_meta_id, $nova_are_ids);
	    }
	    
	    set_mensagem_flash( 'sucesso', 'A meta agregada foi duplicada com sucesso!' );
	    redirect( get_listar_metas_agregadas_url() );
	}
	
	/**
	 * Valida o campo hbc
	 * 
	 * @param string $str Input do campo
	 * 
	 * @return boolean
	 */
	
	public function hbc_check($str)
	{
	    $hbc_a = explode(":", $str);
	    
	    if($hbc_a[1] > 59) {
	        $this->form_validation->set_message('hbc_check', 'Minutos não podem exceder o valor 59.');
	        
	        return false;
	    }
	    
	    return true;
	}
	
	public function xhr_upload_imagem() {
	    if ($_FILES['file']['name']) {
	        if (!$_FILES['file']['error']) {
	            $name = time();
	            $ext = explode('.', $_FILES['file']['name']);
	            $filename = $name . '.' . $ext[count($ext)-1];
	            $destination = $_SERVER['DOCUMENT_ROOT'] .'/painel-coaching/uploads/imagens/' . $filename; //change this directory
	            $location = $_FILES["file"]["tmp_name"];
	            move_uploaded_file($location, $destination);
	            echo '/painel-coaching/uploads/imagens/' . $filename;//change this URL
	        }
	        else
	        {
	            echo  $message = 'Erro ao subir a imagem:  '.$_FILES['file']['error'];
	        }
	    }
	}
}
