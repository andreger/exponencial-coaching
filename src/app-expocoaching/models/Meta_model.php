<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Model para organizar rotinas relacionadas às metas do coaching
 * 
 * @package	Coaching/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

class Meta_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	/**
	 * Adiciona áreas a uma meta
	 * 
	 * @since K4
	 * 
	 * @param int $meta_id Id da meta
	 * @param int|array $areas_ids Id ou ids de áreas a serem adicionadas
	 * 
	 * @return int Id da meta de coaching
	 */ 
	
	public function adicionar_areas($meta_id, $areas_ids)
	{
		if(!is_array($areas_ids)) {
			$areas_ids = [$areas_ids];
		}

		$dados = [];
		foreach ($areas_ids as $id) {
			$dados[] = ['met_id' => $meta_id, 'are_id' => $id];
		}

		$this->db->insert_batch('coaching_metas_areas', $dados);
	}

	/**
	 * Adiciona assuntos a uma meta
	 * 
	 * @since K4
	 * 
	 * @param int $meta_id Id da meta
	 * @param int|array $assuntos_ids Id ou ids de assuntos a serem adicionadas
	 * 
	 * @return int Id da meta de coaching
	 */ 
	
	public function adicionar_assuntos($meta_id, $assuntos_ids)
	{
		if(!is_array($assuntos_ids)) {
			$assuntos_ids = [$assuntos_ids];
		}

		$dados = [];
		foreach ($assuntos_ids as $id) {
			$dados[] = ['met_id' => $meta_id, 'ass_id' => $id];
		}

		$this->db->insert_batch('coaching_metas_assuntos', $dados);
	}

	/**
	 * Adiciona cadernos a uma meta
	 * 
	 * @since K4
	 * 
	 * @param int $meta_id Id da meta
	 * @param int|array $cadernos_ids Id ou ids de cadernos a serem adicionadas
	 * 
	 * @return int Id da meta de coaching
	 */ 
	
	public function adicionar_cadernos($meta_id, $cadernos_ids)
	{
		if(!is_array($cadernos_ids)) {
			$cadernos_ids = [$cadernos_ids];
		}

		$dados = [];
		foreach ($cadernos_ids as $id) {
			$dados[] = ['met_id' => $meta_id, 'cad_id' => $id];
		}

		$this->db->insert_batch('coaching_metas_cadernos', $dados);
	}

	/**
	 * Adiciona disciplinas a uma meta
	 * 
	 * @since K4
	 * 
	 * @param int $meta_id Id da meta
	 * @param int|array $disciplinas_ids Id ou ids de disciplinas a serem adicionadas
	 * 
	 * @return int Id da meta de coaching
	 */ 
	
	public function adicionar_disciplinas($meta_id, $disciplinas_ids)
	{
		if(!is_array($disciplinas_ids)) {
			$disciplinas_ids = [$disciplinas_ids];
		}

		$dados = [];
		foreach ($disciplinas_ids as $id) {
			$dados[] = ['met_id' => $meta_id, 'dis_id' => $id];
		}

		$this->db->insert_batch('coaching_metas_disciplinas', $dados);
	}

	/**
	 * Exclui todas áreas de uma meta
	 * 
	 * @since K4
	 * 
	 * @param int $meta_id Id da meta
	 */ 

	public function excluir_areas($meta_id)
	{
		$this->db->where("met_id", $meta_id);
		$this->db->delete("coaching_metas_areas");
	}

	/**
	 * Exclui todas assuntos de uma meta
	 * 
	 * @since K4
	 * 
	 * @param int $meta_id Id da meta
	 */ 

	public function excluir_assuntos($meta_id)
	{
		$this->db->where("met_id", $meta_id);
		$this->db->delete("coaching_metas_assuntos");
	}

	/**
	 * Exclui todas cadernos de uma meta
	 * 
	 * @since K4
	 * 
	 * @param int $meta_id Id da meta
	 */ 

	public function excluir_cadernos($meta_id)
	{
		$this->db->where("met_id", $meta_id);
		$this->db->delete("coaching_metas_cadernos");
	}

	/**
	 * Exclui todas disciplinas de uma meta
	 * 
	 * @since K4
	 * 
	 * @param int $meta_id Id da meta
	 */ 

	public function excluir_disciplinas($meta_id)
	{
		$this->db->where("met_id", $meta_id);
		$this->db->delete("coaching_metas_disciplinas");
	}

	/**
	 * Exclui uma meta
	 * 
	 * @since K4
	 * 
	 * @param int $meta_id Id da meta
	 */ 

	public function excluir_meta($meta_id)
	{
		$this->db->trans_start();

		self::excluir_areas($meta_id);
		self::excluir_assuntos($meta_id);
		self::excluir_cadernos($meta_id);
		self::excluir_disciplinas($meta_id);

		$this->db->where("met_id", $meta_id);
		$this->db->delete("coaching_metas");

		$this->db->trans_complete();
	}
	
	/**
	 * Conta as metas de coaching
	 *
	 * @since K5
	 *
	 * @return int
	 */
	
	public function contar($filtro = null)
	{
	   return self::filtrar($filtro, true);
	}
	
	/**
	 * Filtra as metas de coaching
	 * 
	 * @since K4
	 * 
	 * @return array Lista de ids de metas de coaching
	 */ 

	public function filtrar($filtro = null, $contar = false)
	{

		if($filtro){

			if($filtro['nome_selecionado']){
				$this->db->like('upper(cm.met_nome)', strtoupper($filtro['nome_selecionado']));
			}

			if(!empty($filtro['consultores_selecionados'])){
				$this->db->where_in('usu_id', $filtro['consultores_selecionados']);
			}

			if($filtro['assuntos_selecionados']){
				$this->db->where_in('cms.ass_id', $filtro['assuntos_selecionados']);
			}

			if($filtro['disciplinas_selecionadas']){
				$this->db->where_in('cmd.dis_id', $filtro['disciplinas_selecionadas']);
			}

			if($filtro['hbc_de']) {
			    $this->db->where('met_hbc >=', $filtro['hbc_de']);
			}
			
			if($filtro['hbc_ate']) {
			    $this->db->where('met_hbc <=', $filtro['hbc_ate']);
			}
			
			if($filtro['data_criacao_de']){
			    $this->db->where('met_data_criacao >=', converter_para_yyyymmdd_HHiiss($filtro['data_criacao_de'] . " 00:00:00"));
			}
			
			if($filtro['data_criacao_ate']){
			    $this->db->where('met_data_criacao <=', converter_para_yyyymmdd_HHiiss($filtro['data_criacao_ate'] . " 23:59:59"));
			}
			
			if($filtro['data_edicao_de']){
			    $this->db->where('met_data_edicao >=', converter_para_yyyymmdd_HHiiss($filtro['data_edicao_de'] . " 00:00:00"));
			}
			
			if($filtro['data_edicao_ate']){
			    $this->db->where('met_data_edicao <=', converter_para_yyyymmdd_HHiiss($filtro['data_edicao_ate'] . " 23:59:59"));
			}
			
			if($filtro['ignorar_metas_atomicas']){
			    $this->db->where_not_in('cm.met_id', $filtro['ignorar_metas_atomicas']);
			}

		}

		$this->db->select("cm.met_id");
		$this->db->distinct();
		$this->db->join("coaching_metas_areas cma", "cm.met_id = cma.met_id", "left");
		$this->db->join("coaching_metas_assuntos cms", "cm.met_id = cms.met_id", "left");
		$this->db->join("coaching_metas_cadernos cmc", "cm.met_id = cmc.met_id", "left");
		$this->db->join("coaching_metas_disciplinas cmd", "cm.met_id = cmd.met_id", "left");
		
		if($contar) {
		    return $this->db->count_all_results("coaching_metas cm");
		}
		else {
		    $this->db->group_by("cm.met_id");
		    
		    if($filtro['limit']){
		        $this->db->limit($filtro['limit'], $filtro['offset']);
		    }
		    
     		$query = $this->db->get("coaching_metas cm");
    		return $query->result_array();
		}
	}

	/**
	 * Recupera uma meta de coaching atráves de um id fornecido
	 * 
	 * @since K4
	 * 
	 * @param int $meta_id Id da meta
	 * 
	 * @return array Array associativo da meta de coaching
	 */ 

	public function get_by_id($meta_id)
	{
		$this->db->where("met_id", $meta_id);
		$query = $this->db->get("coaching_metas");
		
		return $query->row_array();
	}

	/**
	 * Lista todas as áreas de meta de coaching
	 * 
	 * @since K4
	 * 
	 * @param int $meta_id Id da meta
	 * 
	 * @return array Lista de áreas
	 */ 

	public function listar_areas($meta_id)
	{
		$this->db->join(DB_NAME . ".wp_terms t", "t.term_id = cma.are_id");
		$this->db->where("cma.met_id", $meta_id);
		$query = $this->db->get("coaching_metas_areas cma");
		
		return $query->result_array();
	}		

	/**
	 * Lista todas as assuntos de meta de coaching
	 * 
	 * @since K4
	 * 
	 * @param int $meta_id Id da meta
	 * 
	 * @return array Lista de assuntos
	 */ 

	public function listar_assuntos($meta_id)
	{
		$this->db->join("assuntos a", "a.ass_id = cma.ass_id");
		$this->db->where("cma.met_id", $meta_id);
		$query = $this->db->get("coaching_metas_assuntos cma");
		
		return $query->result_array();
	}

	/**
	 * Lista todas as cadernos de meta de coaching
	 * 
	 * @since K4
	 * 
	 * @param int $meta_id Id da meta
	 * 
	 * @return array Lista de cadernos
	 */ 

	public function listar_cadernos($meta_id)
	{
		$this->db->join("cadernos c", "c.cad_id = cmc.cad_id");
		$this->db->where("cmc.met_id", $meta_id);
		$query = $this->db->get("coaching_metas_cadernos cmc");
		
		return $query->result_array();
	}		


	/**
	 * Lista todas as disciplinas de meta de coaching
	 * 
	 * @since K4
	 * 
	 * @param int $meta_id Id da meta
	 * 
	 * @return array Lista de disciplinas
	 */ 

	public function listar_disciplinas($meta_id)
	{
		$this->db->join("disciplinas d", "d.dis_id = cmd.dis_id");
		$this->db->where("cmd.met_id", $meta_id);
		$query = $this->db->get("coaching_metas_disciplinas cmd");
		
		return $query->result_array();
	}		

	/**
	 * Salva uma meta de coaching
	 * 
	 * @since K4
	 * 
	 * @param array $dados a serem salvos
	 * 
	 * @return int Id da meta de coaching
	 */ 

	public function salvar($dados)
	{
		$dados['met_data_edicao'] = date("Y-m-d H:i:s");
		$dados['met_editor_id'] = get_current_user_id();

		if(isset($dados['met_id'])) {
			$this->db->where("met_id", $dados['met_id']);
			$this->db->update("coaching_metas", $dados);

			return $dados['met_id'];
		}
		else {
			$dados['met_data_criacao'] = $dados['met_data_edicao'];
			$dados['usu_id'] = $dados['met_editor_id'];

			$this->db->insert("coaching_metas", $dados);

			return $this->db->insert_id();
		}
	}
	
	/**
	 * Lista todas as metas atômicas de uma meta agregada de coaching
	 *
	 * @since L1
	 *
	 * @param int $meta_agregada_id Id da meta agregada
	 *
	 * @return array Lista de metas atômicas
	 */
	
	public function listar_por_meta_agregada($meta_agregada_id)
	{
	    $this->db->join("coaching_metas_agregadas_metas mea", "mea.met_id = met.met_id");
	    $this->db->where("mea.mea_id", $meta_agregada_id);
	    $this->db->order_by("mea.mam_ordem", "ASC");
	    
	    //$this->db->where_in("met.met_id", [1, 3, 4]);
	    $query = $this->db->get("coaching_metas met");
	    return $query->result_array();
	}

}
