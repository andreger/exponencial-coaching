<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Model para organizar rotinas relacionadas aos cadernos
 * 
 * @package	Coaching/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

class Caderno_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	/**
	 * Lista todos os cadernos de um determinado usuário
	 * 
	 * @since K4
	 * 
	 * @param int $usuario_id Id do usuário
	 * 
	 * @return array Lista de cadernos (registros da tabela exponenc_corp.cadernos)
	 */ 

	public function listar($usuario_id)
	{
		$this->db->where('usu_id', $usuario_id);
		$this->db->order_by('cad_nome', 'asc');
		$query = $this->db->get('cadernos');
		
		return $query->result_array();
	}

}