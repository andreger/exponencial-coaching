<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pagamento_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function listar_pagamentos($data_inicio, $data_fim)
	{
		$this->load->library('podio-php-4.0.1/Podio');
		
		try {
			log_podio('debug', 'Podio: Listando pagamento');
			Podio::setup(PODIO_CLIENT_COACHEE, PODIO_CLIENT_COACHEE_SECRET);
			Podio::authenticate_with_app(PODIO_APP_COACHEE, PODIO_APP_COACHEE_TOKEN);
			
			$items = PodioItem::filter(PODIO_APP_PAGAMENTOS,
				array(
					'filters' => array(
						'mes-de-referencia' => array(
							'from' => $data_inicio,
							'to' => $data_fim
						)
					),
					'limit' => 500
				)
			);
			
			$pagamentos = array();
				
			foreach ($items as $item) {
				$pagamento_array = get_pagamento_array($item);
				$pagamentos[$pagamento_array['aluno']] = $pagamento_array;
			}

			return $pagamentos;
		}
		catch(PodioError $pe) {
			log_podio('error', 'Podio: Erro ao listar pagamentos. ' . $pe->__toString());
			redirecionar_erro_500();
		}
	}
	
	public function listar_pagamentos_do_mes()
	{
		$this->load->model('Coaching_model');
		$this->load->helper('pagamento');
		$this->load->library('podio-php-4.0.1/Podio');
	
		try {			
			$alunos_ativos = $this->Coaching_model->listar_alunos_inscricao_ativa();
			$alunos_chave = $this->Coaching_model->listar_alunos_chave();
			
			log_podio('debug', 'Podio: Listando pagamentos do mês');
			Podio::setup(PODIO_CLIENT_COACHEE, PODIO_CLIENT_COACHEE_SECRET);
			Podio::authenticate_with_app(PODIO_APP_COACHEE, PODIO_APP_COACHEE_TOKEN);
				
			$items = PodioItem::filter(PODIO_APP_PAGAMENTOS,
					array(
							'filters' => array(
									'mes-de-referencia' => array(
											'from' => date('Y-m-01'),
											'to' => date('Y-m-t')
									)
							),
							'sort_by' => 'aluno',
							'sort_desc' => false,
							'limit' => 500
					)
			);
			
			$alunos_ids = array();
			$alunos_pgto_pendente = array();
			
			foreach ($alunos_ativos as $aluno_ativo) { 
				foreach ($alunos_chave as $aluno_chave) {
					if($aluno_ativo['item_id'] == $aluno_chave['item_id']) {
						continue 2;
					}
				}				
				
				$adicionar = true;
				foreach ($items as $item) {
					$pagamento_array = get_pagamento_array($item);
					
					if($pagamento_array['aluno'] == $aluno_ativo['item_id']) {
						$adicionar = false;
						break;
					}
				}
				
				if($adicionar) {
					array_push($alunos_ids, $aluno_ativo['item_id']);
					$alunos_pgto_pendente[$aluno_ativo['item_id']] = $aluno_ativo;
				}
			}
				
			$ultimos_pagamentos = self::listar_ultimo_pagamento_de_alunos($alunos_ids);
	
			foreach ($alunos_pgto_pendente as $id => &$aluno) {
				$aluno['ultimo_pagamento'] = isset($ultimos_pagamentos[$id]) ? $ultimos_pagamentos[$id] : "";
			}
			
			return $alunos_pgto_pendente;
		}
		catch(PodioError $pe) {
			log_podio('error', 'Podio: Erro ao listar pagamentos do mês. ' . $pe->__toString());
			redirecionar_erro_500();
		}
	}
	
	public function listar_ultimo_pagamento_de_aluno($aluno_id)
	{
		$this->load->library('podio-php-4.0.1/Podio');
	
		try {
			log_podio('debug', 'Podio: Listando último pagamento de aluno');
			Podio::setup(PODIO_CLIENT_COACHEE, PODIO_CLIENT_COACHEE_SECRET);
			Podio::authenticate_with_app(PODIO_APP_COACHEE, PODIO_APP_COACHEE_TOKEN);
				
			$items = PodioItem::filter(PODIO_APP_PAGAMENTOS,
					array(
							'filters' => array(
									'aluno' => $aluno_id 
							),
							'sort_by' => 'mes-de-referencia',
							'sort_desc' => true,
							'limit' => 1
					)
			);
	
			if($items->filtered > 0) {
				$item = get_pagamento_array($items[0]);
				return $item['mes_de_referencia']->format('Y-m-d');
			}
			return null;
		}
		catch(PodioError $pe) {
			log_podio('error', 'Podio: Erro ao listar último pagamento de aluno. ' . $pe->__toString());
			redirecionar_erro_500();
		}
	}
	
	public function listar_ultimo_pagamento_de_alunos($alunos_ids)
	{
		$this->load->library('podio-php-4.0.1/Podio');
	
		try {
			log_podio('debug', 'Podio: Listando último pagamento de aluno');
			Podio::setup(PODIO_CLIENT_COACHEE, PODIO_CLIENT_COACHEE_SECRET);
			Podio::authenticate_with_app(PODIO_APP_COACHEE, PODIO_APP_COACHEE_TOKEN);
				
			$items = PodioItem::filter(PODIO_APP_PAGAMENTOS,
					array(
							'filters' => array(
									'aluno' => $alunos_ids 
							),
							'sort_by' => 'mes-de-referencia',
							'sort_desc' => true,
							'limit' => 500
					)
			);
	
			if($items->filtered > 0) {
				$pagamentos = array();
				
				foreach ($items as $item) {
					$item = get_pagamento_array($item);
					
					if(!array_key_exists($item['aluno'], $pagamentos)) {
						$pagamentos[$item['aluno']] = $item['mes_de_referencia']->format('Y-m-d');
					}
				}
				
				return $pagamentos;
			}
			return null;
		}
		catch(PodioError $pe) {
			log_podio('error', 'Podio: Erro ao listar último pagamento de aluno. ' . $pe->__toString());
			redirecionar_erro_500();
		}
	}
	
	public function excluir_todos_pagamentos_pendentes()
	{
		$this->db->empty_table('pagamentos_pendentes');
	}
	
	public function salvar_pagamentos_pendentes($pagamentos)
	{
		self::excluir_todos_pagamentos_pendentes();
		
		if(count($pagamentos) > 0) {
			$this->db->insert_batch('pagamentos_pendentes', $pagamentos);
		}
	}
	
	public function listar_pagamentos_pendentes()
	{
		$this->db->order_by('ppe_turma');
		$this->db->order_by('ppe_nome');
		$query = $this->db->get('pagamentos_pendentes');
		
		return $query->result_array();
	}
}
