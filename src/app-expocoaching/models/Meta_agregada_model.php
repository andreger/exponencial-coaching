<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Model para organizar rotinas relacionadas às metas agregadas do coaching
 * 
 * @package	Coaching/Models
 *
 * @author 	João Paulo Ferreira <jpaulofms@gmail.com>
 *
 * @since	L1
 */ 

class Meta_agregada_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	/**
	 * Adiciona áreas a uma meta agregada
	 * 
	 * @since L1
	 * 
	 * @param int $meta_id Id da meta
	 * @param int|array $areas_ids Id ou ids de áreas a serem adicionadas
	 * 
	 * @return int Id da meta agregada de coaching
	 */ 
	
	public function adicionar_areas($meta_agregada_id, $areas_ids)
	{
		if(!is_array($areas_ids)) {
			$areas_ids = [$areas_ids];
		}

		$dados = [];
		foreach ($areas_ids as $id) {
		    $dados[] = ['mea_id' => $meta_agregada_id, 'are_id' => $id];
		}

		$this->db->insert_batch('coaching_metas_agregadas_areas', $dados);
	}

	/**
	 * Adiciona metas atômicas a uma meta agregada na ordem apresentada
	 * 
	 * @since L1
	 * 
	 * @param int $meta_agregada_id Id da meta agregada
	 * @param array|array<Array> $metas_atomica array contendo as informações das metas e sua ordem
	 */ 
	
	public function adicionar_metas_atomicas($meta_agregada_id, $metas_atomicas)
	{
	    if(!is_array($metas_atomicas)) {
	        $metas_atomicas = [$metas_atomicas];
		}

		$count = 1;
		$dados = [];
		foreach ($metas_atomicas as $meta_atomica) {
			$dados[] = ['mea_id' => $meta_agregada_id, 'met_id' => $meta_atomica['met_id'], 'mam_ordem' => $count++];
		}

		$this->db->insert_batch('coaching_metas_agregadas_metas', $dados);
	}

	/**
	 * Exclui todas áreas de uma meta agregada
	 * 
	 * @since L1
	 * 
	 * @param int $meta_agregada_id Id da meta agregada
	 */ 

	public function excluir_areas($meta_agregada_id)
	{
		$this->db->where("mea_id", $meta_agregada_id);
		$this->db->delete("coaching_metas_agregadas_areas");
	}

	/**
	 * Exclui todas as metas atômicas de uma meta agregada
	 * 
	 * @since L1
	 * 
	 * @param int $meta_agregada_id ID da meta agregada
	 */

	 public function excluir_metas_atomicas($meta_agregada_id)
	 {
		$this->db->where("mea_id", $meta_agregada_id);
		$this->db->delete("coaching_metas_agregadas_metas");
	 }

	/**
	 * Exclui uma meta agregada
	 * 
	 * @since L1
	 * 
	 * @param int $meta_agregada_id Id da meta agregada
	 */ 

	public function excluir_meta_agregada($meta_agregada_id)
	{
		$this->db->trans_start();

		self::excluir_areas($meta_agregada_id);
		self::excluir_metas_atomicas($meta_agregada_id);

		$this->db->where("mea_id", $meta_agregada_id);
		$this->db->delete("coaching_metas_agregadas");

		$this->db->trans_complete();
	}
	
	/**
	 * Conta as metas agregadas de coaching
	 *
	 * @since L1
	 *
	 * @return int
	 */
	
	public function contar($filtro = null)
	{
	   return self::filtrar($filtro, true);
	}
	
	/**
	 * Filtra as metas agregadas de coaching
	 * //TODO:JPFMS
	 * @since L1
	 * 
	 * @return array Lista de ids de metas agregadas de coaching
	 */ 

	public function filtrar($filtro = null, $contar = false)
	{

		if($filtro){

			if($filtro['nome_selecionado']){
				$this->db->like('upper(cma.mea_nome)', strtoupper($filtro['nome_selecionado']));
			}

			if(!empty($filtro['consultores_selecionados'])){
				$this->db->where_in('cma.mea_criador_id', $filtro['consultores_selecionados']);
			}

			if($filtro['assuntos_selecionados']){
				$this->db->where_in('cms.ass_id', $filtro['assuntos_selecionados']);
			}

			if($filtro['disciplinas_selecionadas']){
				$this->db->where_in('cmd.dis_id', $filtro['disciplinas_selecionadas']);
			}

			if($filtro['hbc_de']) {
			    $this->db->where('mea_hbc >=', $filtro['hbc_de']);
			}
			
			if($filtro['hbc_ate']) {
			    $this->db->where('mea_hbc <=', $filtro['hbc_ate']);
			}
			
			if($filtro['data_criacao_de']){
			    $this->db->where('cma.mea_data_criacao >=', converter_para_yyyymmdd_HHiiss($filtro['data_criacao_de'] . " 00:00:00"));
			}
			
			if($filtro['data_criacao_ate']){
			    $this->db->where('cma.mea_data_criacao <=', converter_para_yyyymmdd_HHiiss($filtro['data_criacao_ate'] . " 23:59:59"));
			}
			
			if($filtro['data_edicao_de']){
			    $this->db->where('cma.mea_data_edicao >=', converter_para_yyyymmdd_HHiiss($filtro['data_edicao_de'] . " 00:00:00"));
			}
			
			if($filtro['data_edicao_ate']){
			    $this->db->where('cma.mea_data_edicao <=', converter_para_yyyymmdd_HHiiss($filtro['data_edicao_ate'] . " 23:59:59"));
			}

		}

		$this->db->select("cma.mea_id");
		$this->db->distinct();
		
		$this->db->join("coaching_metas_agregadas_metas cmam", "cmam.mea_id = cma.mea_id", "left");
		$this->db->join("coaching_metas cm", "cm.met_id = cmam.met_id", "left");
		
		$this->db->join("coaching_metas_agregadas_areas cmaa", "cmaa.mea_id = cma.mea_id", "left");
		
		$this->db->join("coaching_metas_assuntos cms", "cm.met_id = cms.met_id", "left");
		$this->db->join("coaching_metas_disciplinas cmd", "cm.met_id = cmd.met_id", "left");
		
		if($contar) {
		    return $this->db->count_all_results("coaching_metas_agregadas cma");
		}
		else {
		    $this->db->group_by("cma.mea_id");
		    
		    if($filtro['limit']){
		        $this->db->limit($filtro['limit'], $filtro['offset']);
		    }
		    
     		$query = $this->db->get("coaching_metas_agregadas cma");
    		return $query->result_array();
		}
	}

	/**
	 * Recupera uma meta de coaching atráves de um ID fornecido
	 * 
	 * @since L1
	 * 
	 * @param int $met_agregada_id Id da meta agregada
	 * 
	 * @return array Array associativo da meta agregada de coaching
	 */ 

	public function get_by_id($meta_agregada_id)
	{
		$this->db->where("mea_id", $meta_agregada_id);
		$query = $this->db->get("coaching_metas_agregadas");
		
		return $query->row_array();
	}

	/**
	 * Lista todas as áreas de meta de coaching
	 * 
	 * @since L1
	 * 
	 * @param int $meta_agregada_id Id da meta agregada
	 * 
	 * @return array Lista de áreas
	 */ 

	public function listar_areas($meta_agregada_id)
	{
		$this->db->join(DB_NAME . ".wp_terms t", "t.term_id = maa.are_id");
		$this->db->where("maa.mea_id", $meta_agregada_id);
		$query = $this->db->get("coaching_metas_agregadas_areas maa");
		return $query->result_array();
	}		

	/**
	 * Salva uma meta agregada de coaching
	 * 
	 * @since L1
	 * 
	 * @param array $dados a serem salvos
	 * 
	 * @return int Id da meta agregada de coaching
	 */ 

	public function salvar($dados)
	{
		$dados['mea_data_edicao'] = date("Y-m-d H:i:s");
		$dados['mea_editor_id'] = get_current_user_id();

		if(isset($dados['mea_id'])) {
			$this->db->where("mea_id", $dados['mea_id']);
			$this->db->update("coaching_metas_agregadas", $dados);

			return $dados['mea_id'];
		}
		else {
			$dados['mea_data_criacao'] = $dados['mea_data_edicao'];
			$dados['mea_criador_id'] = $dados['mea_editor_id'];

			$this->db->insert("coaching_metas_agregadas", $dados);

			return $this->db->insert_id();
		}
	}
	
	/**
	 * Lista as metas agregadas que contém a meta atômica informada
	 *
	 * @since L1
	 *
	 * @param $meta_atomica_id ID da meta atômica
	 */
	public function listar_por_meta_atomica($meta_atomica_id)
	{
	    $this->db->where('met.met_id', $meta_atomica_id);
	    $this->db->join('coaching_metas_agregadas_metas met', 'met.mea_id = mea.mea_id');
	    $query = $this->db->get("coaching_metas_agregadas mea");
	    return $query->result_array();
	}
	
	/**
	 * Atualiza o valor do HBC de uma meta agregada com base em suas metas atômicas associadas
	 * 
	 * @since L1
	 * 
	 * @param $meta_agregada_id ID da meta agregada
	 */
	public function atualizar_hbc_agregado($meta_agregada_id)
	{
	    //Soma os HBC
	    $this->db->select("SEC_TO_TIME(SUM(TIME_TO_SEC(CAST(met.met_hbc as TIME)))) AS total");
        $this->db->from("coaching_metas met");
        $this->db->join("coaching_metas_agregadas_metas mea", "mea.met_id = met.met_id");
        $this->db->where("mea.mea_id", $meta_agregada_id);
	    
        $query = $this->db->get();
        $result = $query->row_array();        
        $total = $result['total'];
        
        if(!$total)
        {
            $total = "00:00:00";
        }
        
        //Altera o total
        $this->db->where("mea_id", $meta_agregada_id);
        $this->db->set("mea_hbc", substr($total, 0, -3));//Remove os segundos
        $this->db->update("coaching_metas_agregadas");
        
	}

}
