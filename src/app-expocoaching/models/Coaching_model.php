<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Coaching_model extends CI_Model {

	public function __construct()
	{
		$this->load->library('podio-php-4.0.1/Podio');
		$this->load->database();
		
		try {
			log_podio('debug', "Podio: Autenticando usuário. [App: " . PODIO_CLIENT_COACHEE . "] [Secret: " . PODIO_CLIENT_COACHEE_SECRET . "]");
			Podio::setup(PODIO_CLIENT_COACHEE, PODIO_CLIENT_COACHEE_SECRET);
			Podio::authenticate_with_app(PODIO_APP_COACHEE, PODIO_APP_COACHEE_TOKEN);
		}
		catch(PodioError $pe) {
			log_podio('error', 'Podio: Erro ao autenticar usuário. ' . $pe->__toString());
			redirecionar_erro_500();
		}
	}
	
	public function listar_alunos_inscricao_ativa()
	{
		$status_array = array(
				PODIO_COACHEE_STATUS_ATIVO,
		);
	
		return self::listar_alunos_por_status($status_array);
	}
	
	public function listar_alunos_chave()
	{
		$status_array = array(
				PODIO_COACHEE_STATUS_ALUNO_CHAVE,
		);
	
		return self::listar_alunos_por_status($status_array);
	}
	
	public function listar_alunos_inscricao_em_andamento()
	{
		$status_array = array(
			PODIO_COACHEE_STATUS_FORMULARIO_BASICO,
			PODIO_COACHEE_STATUS_FORMULARIO_DETALHADO,
			PODIO_COACHEE_STATUS_PERFIL_PSICOPEDAGOGICO,
			PODIO_COACHEE_STATUS_PLANILHAS_DETALHADAS,
			PODIO_COACHEE_STATUS_INSCRICAO_PENDENTE,
			PODIO_COACHEE_STATUS_PAGAMENTO_LIBERADO
		);
	
		return self::listar_alunos_por_status($status_array);
	}
	
	public function listar_alunos_por_status($status_array)
	{
		try {
			log_podio('debug', 'Podio: Filtrando alunos por status. ' . implode($status_array));
			
			$alunos = array();
				
			// conta o número de items e calcula o número de páginas
			$items = PodioItem::filter(PODIO_APP_COACHEE,
					array(
							'filters' => array(
									'status' => $status_array
							),
							'limit' => 1
					)
			);

			$filtrados = $items->filtered;
				
			if($filtrados > 0) {
				$paginas = floor(($filtrados - 1) / PODIO_COACHE_ITENS_POR_PAGINA) + 1;
			
				for($i = 0; $i < $paginas; $i++) {
					set_time_limit(120);
					
					$items = PodioItem::filter(PODIO_APP_COACHEE,
							array(
									'filters' => array(
											'status' => $status_array
									),
									'limit' => PODIO_COACHE_ITENS_POR_PAGINA,
									'offset' => $i * PODIO_COACHE_ITENS_POR_PAGINA
							)
					);
		
		
					foreach ($items as $item) {
						array_push($alunos, self::get_aluno_array($item));
					}
				}
			}
			log_podio('debug', 'Podio: Rate Limit Remaining >> ' . Podio::rate_limit_remaining());
			
			return $alunos;
		}
		catch(PodioError $pe) {
			log_podio('error', 'Podio: Erro ao listar alunos por status. ' . $pe->__toString());
			redirecionar_erro_500();
		}
	}
	
	public function listar_alunos_suspensos_ou_ex()
	{
		$status_array = array(
			PODIO_COACHEE_STATUS_SUSPENSO,
			PODIO_COACHEE_STATUS_EX_ALUNO
		);
		
		return self::listar_alunos_por_status($status_array);
	}
	
	public function listar_alunos_nao_convertidos()
	{
		$status_array = array(
				PODIO_COACHEE_STATUS_NAO_CONVERTIDO
		);
	
		return self::listar_alunos_por_status($status_array);
	}
	
	public function listar_alunos_ativos_ou_ex()
	{
		$status_array = array(
				PODIO_COACHEE_STATUS_ATIVO,
				PODIO_COACHEE_STATUS_EX_ALUNO
		);
	
		return self::listar_alunos_por_status($status_array);
	}
	
	public function listar_alunos_ativos_ou_ex_ou_suspensos()
	{
		$status_array = array(
				PODIO_COACHEE_STATUS_ATIVO,
				PODIO_COACHEE_STATUS_EX_ALUNO,
				PODIO_COACHEE_STATUS_SUSPENSO
		);
	
		return self::listar_alunos_por_status($status_array);
	}
	
	public function listar_todos_alunos()
	{
		try {
			log_podio('debug', 'Podio: Listando todos os alunos');
			
			$alunos = array();
			
			// conta o número de items e calcula o número de páginas
			$items = PodioItem::filter(PODIO_APP_COACHEE,
					array(
							'limit' => 1
					)
			);

			$filtrados = $items->filtered;
			
			if($filtrados > 0) {
				$paginas = floor(($filtrados - 1) / PODIO_COACHE_ITENS_POR_PAGINA) + 1;
				
				for($i = 0; $i < $paginas; $i++) {
					set_time_limit(120);
					
					$items = PodioItem::filter(PODIO_APP_COACHEE,
							array(
									'limit' => PODIO_COACHE_ITENS_POR_PAGINA,
									'offset' => $i * PODIO_COACHE_ITENS_POR_PAGINA
							)
					);
					
					foreach ($items as $item) {
						array_push($alunos, self::get_aluno_array($item));
					}
					
				}
			}
			
			log_podio('debug', 'Podio: Rate Limit Remaining >> ' . Podio::rate_limit_remaining());
			
			return $alunos;
		}
		catch(PodioError $pe) {
			log_podio('error', 'Podio: Erro ao listar todos os alunos. ' . $pe->__toString());
			redirecionar_erro_500();
		}
	}
	
	public function get_aluno_by_item_id($item_id)
	{
		try {
			log_podio('debug', 'Podio: Recuperando aluno por id');
			$podio_item = PodioItem::get($item_id);
			return self::get_aluno_array($podio_item);
		} catch (PodioError $pe) {
			log_podio('error', 'Podio: Erro ao recuperar aluno por item_id. ' . $pe->__toString());
			redirecionar_erro_500();
		}
	}
	
	private function get_aluno_array($podio_item)
	{
		$aluno = array();
		$aluno['item_id'] = $podio_item->item_id;
		
		foreach ($podio_item->fields as $campo) {
			
			switch ($campo->external_id) {
				case 'referencia-pgto' : $aluno['referencia_pgto'] = $campo->start_date; break;

				case 'nome': 	$aluno['nome'] = $campo->values; break;

				case 'e-mail': 	$aluno['email'] = $campo->values; break;

				case 'cpf': 	$aluno['cpf'] = $campo->values; break;

				case 'status': 	{
					$etapas = [];
					foreach ($campo->values as $value) {
						array_push($etapas, $value['text']);
					}
					asort($etapas);
					$aluno['etapa'] = implode(' / ', $etapas); break;
				}

				case 'turma': 	$aluno['turma'] = $campo->values[0]['text']; 
								$aluno['turma_id'] = $campo->values[0]['id'];break;

				case 'escolha-sua-turma-de-coaching': 
								$aluno['area'] = $campo->values[0]['text']; 
								$aluno['area_id'] = $campo->values[0]['id'];break;

				case 'coach': 	$aluno['coach'] = $campo->values[0]['text']; break;

				case 'data-formulario-basico': 		$aluno['data_formulario_basico'] = $campo->start_date; break;

				case 'data-formulario-detalhado': 	$aluno['data_formulario_detalhado'] = $campo->start_date; break;

				case 'data-perfil-psicopedagogico': $aluno['data_perfil_psicopedagogico'] = $campo->start_date; break;

				case 'data-planilhas-detalhadas': $aluno['data_planilhas_detalhadas'] = $campo->start_date; break;

				case 'data-inscricao-pendente': $aluno['data_inscricao_pendente'] = $campo->start_date; break;
			}
		}
	
		return $aluno;		
	}
	
	public function atualizar_status($item_id, $status_id)
	{
		try {
			log_podio('debug', 'Podio: Atualizando status item id.');

			if(is_array($status_id)) {
				PodioItemField::update($item_id, 'status', $status_id);
			}
			else {
				PodioItemField::update($item_id, 'status', array($status_id));
			}
		} catch (PodioError $pe) {
			log_podio('error', 'Podio: Erro ao atualizar status. ' . $pe->__toString());
		}
	}
	
	public function novo_aluno($email, $nome)
	{
		try {
			// se houver o aluno no podio não executa o resto
			if(self::get_aluno($email)) return; 
			
			log_podio('debug', 'Podio: Crianto novo coachee.');
			PodioItem::create(PODIO_APP_COACHEE, array('fields' => 
				array(
					"nome" => $nome,
					"e-mail" => $email,
					"status" => PODIO_COACHEE_STATUS_FORMULARIO_BASICO,
					"data-formulario-basico" => get_data_hora_agora()
				)
			));
			
			log_podio('debug', 'Podio: Criando novo coache estudo.');
			PodioItem::create(PODIO_APP_COACHEE_ESTUDO, array('fields' =>
					array(
							"e-mail" => $email,
					)
			));

		} catch (PodioError $pe) {
			log_podio('error', 'Podio: Erro ao criar novo aluno. ' . $pe->__toString());
			redirecionar_erro_500();
		}
	}
	
	public function get_aluno($email)
	{
		try {
			log_podio('debug', 'Podio: Recuperando aluno por e-mail.');
			$items = PodioItem::filter ( PODIO_APP_COACHEE, array (
					'filters' => array (
							'e-mail' => $email
					)
			) );
			
			if(count($items) > 0) {
				return self::get_aluno_array($items[0]);	
			}
			
			return false;			
		} catch (PodioError $pe) {
			log_podio('error', 'Podio: Erro ao recuperar aluno. ' . $pe->__toString());
			redirecionar_erro_500();
		}
	}
	
	public function get_coachee_item_id($email)
	{
		try {
			log_podio('debug', 'Podio: Recuperando coaching item id.');
			$items = PodioItem::filter ( PODIO_APP_COACHEE, array (
					'filters' => array (
							'e-mail' => $email
					)
			) );
	
			if(count($items) > 0) {
				return $items[0]->id;
			}
	
			return false;
		} catch (PodioError $pe) {
			log_podio('error', 'Podio: Erro ao recuperar coachee item id. ' . $pe->__toString());
			redirecionar_erro_500();
		}
	}
	
	public function get_coachee_estudo_item_id($email)
	{
		try {
			log_podio('debug', 'Podio: Recuperando coachee estudo item id.');
			$items = PodioItem::filter ( PODIO_APP_COACHEE_ESTUDO, array (
					'filters' => array (
							'e-mail' => $email
					)
			) );
				
			if(count($items) > 0) {
				return $items[0]->id;
			}
				
			return false;
		} catch (PodioError $pe) {
			log_podio('error', 'Podio: Erro ao recuperar coachee estudo item id. ' . $pe->__toString());
			redirecionar_erro_500();
		}
	}
	
	public function atualizar_coachee($aluno, $data)
	{
		try {
			log_podio('debug', 'Podio: Atualizando coachee.');
			PodioItem::update($aluno['item_id'], array('fields' => $data));
			
			$coaching_estudo_item_id = self::get_coachee_estudo_item_id($aluno['email']);
			$coaching_estudo_data = array('relacionamento' => (int)$aluno['item_id'], 'nome' => $data['nome']);
			
			log_podio('debug', 'Podio: Atualizando coachee estudo.');
			PodioItem::update($coaching_estudo_item_id, array('fields' => $coaching_estudo_data));
		} catch (PodioError $pe) {
			log_podio('error', 'Podio: Erro ao atualizar coachee. ' . $pe->__toString());
			redirecionar_erro_500();
		}
		
	}
	
	
	
	public function atualizar_coachee_estudo($aluno, $data)
	{
		try {
			log_podio('debug', 'Podio: Recuperando coachee estudo item id.');
			$item_id = self::get_coachee_estudo_item_id($aluno['email']);

			log_podio('debug', 'Podio: Atualizando coachee estudo.');
			PodioItem::update($item_id, array('fields' => $data));
			
			$item_id = self::get_coachee_item_id($aluno['email']);
			
			log_podio('debug', 'Podio: Atualizando status item id.');
			self::atualizar_status($aluno['item_id'], [PODIO_COACHEE_STATUS_PERFIL_PSICOPEDAGOGICO, PODIO_COACHEE_STATUS_ATIVO]);
			
			log_podio('debug', 'Podio: Atualizando coache.');
			self::atualizar_coachee($aluno, array('data-perfil-psicopedagogico' => get_data_hora_agora()));
		} catch (PodioError $pe) {
			log_podio('error', 'Podio: Erro ao atualizar coachee estudo. ' . $pe->__toString());
			redirecionar_erro_500();
		}
	
	} 
	
	public function atualizar($item_id, $data)
	{
		try {
			log_podio('debug', 'Podio: Atualizando item id.');
			PodioItem::update($item_id, array('fields' => $data));
		} catch (PodioError $pe) {
			log_podio('error', 'Podio: Erro ao atualizar item id. ' . $pe->__toString());
			redirecionar_erro_500();
		}
	
	}
	
	public function anexar($item_id, $origem, $destino)
	{		
		try {
			log_podio('debug', 'Podio: Autenticando por senha.');
			Podio::setup (PODIO_CLIENT_COACHEE, PODIO_CLIENT_COACHEE_SECRET);
			Podio::authenticate_with_password ( 'andreger@gmail.com', 'K3yd3@2015' );

			log_podio('debug', 'Podio: Upload de arquivo.');

			$uploaded_item = PodioFile::upload ( $origem, $destino );

			log_podio('debug', 'Podio: Anexando arquivo.');
			PodioFile::attach ( $uploaded_item->id, array (
					'ref_type' => 'item',
					'ref_id' => $item_id
			) );

		} catch (PodioError $pe) {
			log_podio('error', 'Podio: Erro ao anexar arquivo. ' . $pe->__toString());
			redirecionar_erro_500();
		}
	}
	
	public function salvar_opcoes_combo($dados)
	{
		self::remover_todas_opcoes_combo();
		$this->db->insert_batch('coaching_combos', $dados);
	}
	
	public function remover_todas_opcoes_combo()
	{
		$this->db->empty_table('coaching_combos');
	}
	
	public function listar_opcoes_combo($combo_key)
	{
		$this->db->where('cco_key', $combo_key);
		$query = $this->db->get('coaching_combos');
		
		return $query->result_array();
	}
}
