<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Model para organizar rotinas relacionadas às disciplinas
 * 
 * @package	Coaching/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

class Disciplina_model extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
	}

	/**
	 * Lista todos as disciplinas
	 * 
	 * @since K4
	 * 
	 * @param bool $somente_ativas Flag para considerar todas as disciplinas ou somente as ativas
	 * 
	 * @return array Lista de disciplinas (registros da tabela exponenc_corp.disciplinas)
	 */ 
	
	public function listar($somente_ativas = TRUE)
	{
		if($somente_ativas) {
			$this->db->where('dis_ativo', SIM);
		}

		$this->db->order_by('dis_nome');

		$query = $this->db->get('disciplinas');

		return $query->result_array();
	}

}