<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Model para organizar rotinas relacionadas aos assuntos
 * 
 * @package	Coaching/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

class Assunto_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	/**
	 * Retorna assuntos através de ids de disciplinas
	 * 
	 * @since K4
	 * 
	 * @param int|array $ids Id ou lista de ids de disciplinas
	 * 
	 * @return array Lista de assuntos
	 */

	public function listar_por_disciplinas($ids, $somente_ativos = TRUE, $order_by = 'ass_prioridade asc, ass_nome asc') 
	{
		if(!is_array($ids)) {
			$ids = [$ids];
		}

		if($somente_ativos) {
			$this->db->where('ass_ativo', SIM);
		}

		foreach($ids as $id) {
			$this->db->where('dis_id', $id);
		}

		$this->db->order_by($order_by);

		$query = $this->db->get('assuntos');

		return $query->result_array();
	}

}