<?php get_header(); ?>

<div class="ex-aluno-fundo pt-5 pb-5">
	<div class="container pt-5 pb-5" id="coaching">
		<div class="pt-4 pb-4">
		<div class="pt-5 pb-5 ml-auto mr-auto col-12 col-lg-10">
			<h1 class="text-white">
			Olá, identificamos que você já foi aluno do Coaching Exponencial. Caso queira retomar os estudos conosco, <a class="t-d-none text-blue-2" href="/fale-conosco">clique aqui</a> e nos envie uma mensagem.</h1>
		</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>			