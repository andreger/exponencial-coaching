<?php redirecionar_se_nao_estiver_logado(); ?>
<?php get_header(); ?>
<div class="">	
	<?php echo get_cabecalho_secao('bandeira-coaching.png', 'Painel do Coaching')?>
	
	<div id="coaching-etapa-4" class="container pt-1 pt-md-5 pb-5">
	
	<?php if(isset($sucesso) && $sucesso) : ?>

		<div>
			<div class="alert alert-success col-12 text-center">Planilhas enviadas com sucesso!</div>

			<div class="text-center pt-3"><a href="<?= get_inscricao_home_url() ?>">Clique aqui</a> para continuar.</div>
		</div>
		
	<?php else : ?>

		<div class="mt-5 mt-md-1 row titulo-coaching">
			<div class="ml-auto mr-auto p-2 col-9 col-md-4 col-lg-3 col-xl-2 bg-darkblue text-white font-12 font-arial font-weight-bold" id="titulo-etapa">Coaching - Planilhas Detalhadas</div>
			<div class="col-12 col-md-8  pt-1 mt-4 text-dark" id="informacao">Para completar esta etapa siga os seguintes passos:</div>
		</div>
		
		<div id="passos" class="row mt-5">
			<div id="etapa-1" class="ml-auto mr-auto col-9 col-md-6 col-lg-3 col-xl-4">				
				<div class="mt-1">
					<img class="img-fluid" src="/wp-content/themes/academy/images/coaching/2497148212-download.png">
					Faça download das seguintes planilhas:
				</div>
				<div class="mt-2">					
					<a href="<?= get_avaliacao_estudos_url() ?>"><img class="img-fluid" src="/wp-content/themes/academy/images/coaching/3692160744-download-avaliacao.png"></a>
				</div>
				<div class="mt-2">					
					<a href="<?= get_disponibilidade_aluno_url() ?>"><img class="img-fluid" src="/wp-content/themes/academy/images/coaching/1337939442-download-disponibilidade.png"></a>
				</div>			
			</div>

		<div id="etapa-2" class="mt-3 mt-lg-0 ml-auto mr-auto col-12 col-md-9 col-lg-7 col-xl-6">
				<form action="<?= get_planilhas_detalhadas_url() ?>" method="post" enctype="multipart/form-data">					
				<div class="text-right d-none d-lg-block">
					<input class="position-absolute btn u-btn-blue" id="enviar" type="submit" name="submit" value="Enviar">
				</div>	
					<ul>
						<li>
							<div class="mt-1">
								<img class="img-fluid" src="/wp-content/themes/academy/images/coaching/1319570270-upload.png">
								 Preencha as planilhas e envie através do formulário abaixo:
							</div>
						</li>

						<li class="li-upload upload-avaliacao" id="upload-avaliacao">
							<div><input id="input-avaliacao" class="col-8 upload" type="file" name="avaliacao"></div>
							<input class="upload-text" id="upload-avaliacao" placeholder="Escolher Arquivo" disabled="disabled">
						</li>
						<li class="li-upload upload-disponibilidade" id="upload-disponibilidade">
							<div><input id="input-disponibilidade" class="col-8 upload" type="file" name="disponibilidade"></div>
							<input class="upload-text" id="upload-disponibilidade" placeholder="Escolher Arquivo" disabled="disabled">
						</li>
					</ul>
					<div class="mt-4 text-center d-block d-lg-none col-12">
					<input class="btn u-btn-blue" id="enviar" type="submit" name="submit" value="Enviar">
				</form>

			</div>
			
		</div>

	<?php endif ?>
	</div>
</div>
<?php include_js_libs() ?>
<script type="text/javascript">
jQuery(".upload").change(function() {
	var filename = jQuery(this).val();
	jQuery(this).parent().parent().find("input.upload-text").val(filename);
});
</script>
<?php get_footer(); ?>  
