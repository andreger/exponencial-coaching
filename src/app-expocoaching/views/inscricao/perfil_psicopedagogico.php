<?php redirecionar_se_nao_estiver_logado() ?>
<?php get_header(); ?>
<div class="">

	<form action="<?php echo get_perfil_psicopedagogico_url() ?>" method="post">
	<?php echo get_cabecalho_secao('bandeira-coaching.png', 'Painel do Coaching')?>

	<div id="coaching-etapa-4" class="container pt-1 pt-md-5 pb-5">		
			<div class="bg-darkblue text-white font-12 ml-auto mr-auto ml-md-0 mr-md-0 p-2 col-7 col-md-3 col-lg-2 text-center">Coaching - Etapa 3</div>
	
	


	<div class="row mt-5">
		<div class="col-12 col-md-8 font-10">
			<div>Por favor, preencha o seguinte teste: <a class="t-d-none text-blue" href="http://www.16personalities.com/br/teste-de-personalidade" target="_blank">http://www.16personalities.com/br/teste-de-personalidade.</a></div>
			<div>São 60 perguntas para identificar o seu tipo psicológico. </div>
			<div>Isto será usado para avaliarmos as técnicas mais prováveis de se adequarem a seu estudo, além da forma mais apropriada de conduzir a relação com você.</div>
			<div>Ao final, virá um resultado com uma combinação de 4 letras (ENTJ, ISFP, coisas do tipo). Pedimos a gentileza de que faça a avaliação e, na sequência, informe-nos no campo abaixo:</div>

			<div class="coaching-submit mt-5">
				<div class="col-12 text-right d-none d-md-block">
					<input class="btn u-btn-primary" type="submit" name="submit" value="Enviar" >
				</div>
			</div>
		</div>
		
		<div class="col-12 col-md-4">
		
			
		   	
		    <div class="col-12 col-md-10 text-center">
				<div class="ml-auto mr-auto coaching-resultado">
			
					<div class="text-center pt-5">
						Resultado:
						<select class="ml-auto mr-auto form-control col-6" name="resultado">
							<option value="3">ENFJ</option>
							<option value="10">ENFP</option>
							<option value="11">ENTJ</option>
							<option value="12">ENTP</option>
							<option value="6">ESFJ</option>
							<option value="13">ESFP</option>
							<option value="9">ESTJ</option>
							<option value="1">ESTP</option>
							<option value="4">INFJ</option>
							<option value="14">INFP</option>
							<option value="5">INTJ</option>
							<option value="7">INTP</option>
							<option value="8">ISFJ</option>
							<option value="16">ISFP</option>
							<option value="2">ISTJ</option>
							<option value="15">ISTP</option>
						</select>
					</div>
				</div>

				<div class="mt-3 d-block d-md-none">
					<input class="btn u-btn-primary" type="submit" name="submit" value="Enviar">
				</div>
			</div>
		</div>
	</div>
	</form>
</div>
</div>
<?php get_footer(); ?>  