<?php get_header(); ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
	$(function() {
		$( document ).tooltip({
			position: {
				my: "center bottom-5",
				at: "center top",
				using: function( position, feedback ) {
					$( this ).css( position );
					$( "<div>" )
						.addClass( "arrow" )
						.addClass( feedback.vertical )
						.addClass( feedback.horizontal )
						.appendTo( this );
				}
			}
		});
	});
</script>
<style>
  .ui-tooltip {
	background: rgb(52,132,197);
    padding: 10px;
    color: white;
    border: none;
	border-radius: 2;
    font: 400 14px 'Droid Sans', Sans-Serif;
    text-align: justify;
    box-shadow: -3px -3px rgb(10,35,100);
  }
  .arrow {
    width: 90px;
    height: 25px;
    overflow: hidden;
    position: absolute;
    left: 50%;
    margin-left: -35px;
    bottom: -23px;
  }
  .arrow.top {
    top: -16px;
    bottom: auto;
  }
  .arrow.left {
    left: 20%;
  }
  .arrow:after {
	border: 5px solid rgb(52,132,197);
  	background: rgb(10,103,162);
    content: "";
    position: absolute;
    left: 20px;
    top: -30px;
    width: 25px;
    height: 25px;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
  }
  .arrow.top:after {
    bottom: -20px;
    top: auto;
  }
</style>

<div class="container-fluid pt-1 pt-md-4">
<div class="pt-3 mb-3 col-12 text-center text-md-left text-blue">
		<h1>Inscrições - Coaching</h1>
	</div>
</div>
<div class="container pb-5 <?= isset($novo_aluno) && $novo_aluno ? "coaching-fundo" : "";?>">
	<div id="">		
			<div class="col-12 mt-4">
				<?= get_mensagem_flash_wp() ?>
			</div>

			<div class="col-12 mt-4 font-10">
				<img src="<?php echo get_tema_image_url('alerta.png')?>"> Clique no box em azul para prosseguir na sua inscrição. Caso o botão não esteja disponível, entre em contato conosco pelo e-mail <a href="mailto:contato@exponencialconcursos.com.br?subject=Assunto da Mensagem">contato@exponencialconcursos.com.br</a> e WhatsApp 21 9.9202.7050
			</div>

			<div class="col-12">
				
				<div class="mt-4">
				<?php if(!is_etapa_status_ativo($fase_aluno)) : ?>
					<div class="coaching-posicao-1">
					<!--	<img width="250" class="coaching-inscricao-img" src="<?= get_tema_image_url('/coaching/nao-inscrito.png') ?>"> -->
					</div>
				<?php else : ?>
					<div class="coaching-posicao-1">
						<div>
							<img class="coaching-inscricao-img" src="<?= get_tema_image_url('/coaching/alunoativo.png') ?>">
						</div>
					</div>
				<?php endif ?>
				</div>
				
				
				<div class="row ml-auto mr-auto mt-5" id="lista-fases">
					
						<div class="col-12 col-md-6 col-lg-4 p-0">
							<div class="ml-auto mr-auto row p-0" title="Fase de pré-inscrição - preenchimento de informações básicas de estudo.">
							<?php if($fase_aluno == 1) : ?>
								<div class="col-10 p-0">
								<a href="<?= get_formulario_basico_url() ?>">
									<img class="img-fluid" src="<?= get_tema_image_url('/coaching/formulario-basico.png') ?>">
								</a>
								</div>
							<?php else : ?>
							<div class="col-10 p-0">	
								<img class="img-fluid" src="<?= get_tema_image_url('/coaching/formulario-basico-inativo.png') ?>">
							</div>
							<?php endif ?>
							<div class="col-2 p-0 mt-1">
								<img class="img-fluid" src="<?= get_tema_image_url('/coaching/seta-coaching.png') ?>">
							</div>
							</div>
						</div>
						<div class="col-12 col-md-6 col-lg-4 p-0">
							<div class="ml-auto mr-auto row p-0" title="Fase de pré-inscrição - preenchimento de informações detalhadas de estudo.">
							<?php if($fase_aluno == 2) : ?>
								<div class="col-10 p-0">
								<a href="<?= get_formulario_detalhado_url() ?>">
									<img class="img-fluid" src="<?= get_tema_image_url('/coaching/formulario-detalhado.png') ?>">
								</a>
							</div>
							<?php else : ?>
							<div class="col-10 p-0">	
								<img class="img-fluid" src="<?= get_tema_image_url('/coaching/formulario-detalhado-inativo.png') ?>">
							</div>	
							<?php endif ?>
							<div class="col-2 p-0 mt-1">
								<img class="img-fluid" src="<?= get_tema_image_url('/coaching/seta-coaching.png') ?>">
							</div>	
							</div>
						</div>
						<div class="col-12 col-md-6 col-lg-4 p-0">				
						<div class="ml-auto mr-auto row p-0" title="Fase de pré-inscrição - teste psicológico. Após terminar o teste, clique no botão Informar perfil e informe o resultado do seu teste.">
							<?php if($fase_aluno == 3) : ?>
								<div class="col-10 p-0">
								<a href="<?= get_perfil_psicopedagogico_url() ?>">
									<img class="img-fluid" src="<?= get_tema_image_url('/coaching/perfil.png') ?>">
								</a>
							</div>
							<?php else : ?>
							<div class="col-10 p-0">	
								<img class="img-fluid" src="<?= get_tema_image_url('/coaching/perfil-inativo.png') ?>">
							<?php endif ?>
							</div>
							</div>
						</div>
					
					
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
		