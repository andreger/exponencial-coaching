<?php redirecionar_se_nao_estiver_logado() ?>
<?php get_header() ?>

<?php echo get_cabecalho_secao('bandeira-coaching.png', 'Formulário Detalhado para o Coaching Exponencial Concursos')?>

<div class="container pt-1 pt-md-5 pb-5">
	<div class="row ml-auto mr-auto mt-5 mt-md-1">
		<div class="col-2 col-md-1">
			<img class="img-fluid" src="<?php echo get_tema_image_url('icone-formulario.png')?>">
		</div>
		
		<div class="col-10 col-md-11">
			Preencha estes dados com muita atenção. As informações contidas aqui servirão de base para prepararmos um plano de estudo personalizado para o seu desenvolvimento.
			<br>Se tiver dúvidas nos itens, passe o mouse sobre o campo que deve preencher e verá sua descrição.
			<br>Se ainda assim persistir sua dúvida, entre em contato conosco para que possamos ajudá-lo.
		</div>
	</div>

	<form class="mt-3" id="formulario-detalhado" method="post" action="<?php echo get_formulario_detalhado_url($item_id) ?>">
	
		<div class="row">
			<div class="col-12 col-md-3 text-center text-md-right">Selecione as matérias em que tem dificuldade:</div>
			<div class="col-12 col-md-8" title="Selecione somente as matérias que caem no concurso em que objetiva ser aprovado(a).||Para matérias como línguas, português e exatas, considere também seu histórico no Ensino Médio e na Faculdade.">
				<?php foreach ($materias_combo as $key => $value) {
					echo form_checkbox('materias[]', $key, $this->input->post('materias') ? in_array($key, $this->input->post('materias')) : false) . " $value<br>";
				}
				?>
				<div>
					<label for="materias[]" class="error" style="display: none;">Campo obrigatório</label>
				</div>
			</div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Informe livremente caso haja especificidades sobre as matérias escolhidas acima. E nos informe de outras matérias, se for o caso.</div>
			<div class="col-12 col-md-8"><textarea rows="3" class="form-control" type="text" name="especificidades"><?php echo $this->input->post('especificidades') ?: ''?></textarea></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Informe a faculdade onde estudou, se aplicável.</div>
			<div class="col-12 col-md-8"><input class="form-control" type="text" name="faculdade" value="<?php echo $this->input->post('faculdade') ?: ''?>"></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Informe seu curso de graduação, se aplicável.</div>
			<div class="col-12 col-md-8"><input class="form-control" type="text" name="graduacao" value="<?php echo $this->input->post('graduacao') ?: ''?>"></div>
		</div>

		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Informe a data de sua graduação, se aplicável.</div>
			<div class="col-12 col-md-8"><input class="form-control" type="text" id="data_graduacao" name="data_graduacao" value="<?php echo $this->input->post('data_graduacao') ?: ''?>"></div>
		</div>

		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Informe resumidamente sua experiência profissional <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><textarea rows="3" class="form-control" type="text" name="experiencia"><?php echo $this->input->post('experiencia') ?: ''?></textarea></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Está trabalhando atualmente? <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('trabalhando', $trabalhando_combo, $trabalhando_selecionado, 'class="form-control" id="trabalhando">') ?></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Quando você se desloca do trabalho para casa (e vice-versa), você consegue: (Ignore esta pergunta se não está trabalhando atualmente)</div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('deslocamento', $deslocamento_combo, $deslocamento_selecionado, 'class="form-control" id="deslocamento">') ?></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Quando você sai do trabalho, sente-se desgastado física ou mentalmente?</div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('desgaste', $desgaste_combo, $desgaste_selecionado, 'class="form-control" id="desgaste">') ?></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Quanto tempo diariamente você gasta no deslocamento casa-trabalho? (Ignore se não está trabalhando atualmente)</span></div>
			
				<div class="ml-5 ml-md-3 col-2 col-md-1 p-0"><input class="form-control" type="text" name="horas_deslocamento" value="<?php echo $this->input->post('horas_deslocamento') ?: ''?>"></div>
				<div class="col-3 col-md-1">Horas</div>
				<div class="col-2 col-md-1 p-0"><input class="form-control" type="text" name="minutos_deslocamento" value="<?php echo $this->input->post('minutos_deslocamento') ?: ''?>"></div>
				<div class="col-3 col-md-1">Minutos</div>
				
			
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Onde você costuma estudar?<span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8" title="Escolha todos os lugares onde gasta parte de seu tempo estudando">
				<?php foreach ($lugar_combo as $key => $value) {
					echo form_checkbox('lugar[]', $key, $this->input->post('lugar') ? in_array($key, $this->input->post('lugar')) : false) . " $value<br>";
				}
				?>
				<div>
					<label for="lugar[]" class="error" style="display: none;">Campo obrigatório</label>
				</div>
			</div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Quanto tempo você gasta para deslocar-se até a biblioteca mais próxima do trabalho ou casa? <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('tempo', $tempo_combo, $tempo_selecionado, 'class="form-control" id="tempo">') ?></div>
		</div>
				
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">O ambiente barulhento o/a distrai? <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('ambiente', $ambiente_combo, $ambiente_selecionado, 'class="form-control" id="ambiente">') ?></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Já teve dificuldades em fazer uma prova por conta do tempo? <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('dificuldade_tempo', $dificuldade_tempo_combo, $dificuldade_tempo_selecionado, 'class="form-control" id="dificuldade_tempo">') ?></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Se respondeu Sim acima, indique as matérias que mais tem dificuldade por conta de tempo:</div>
			<div class="col-12 col-md-8">
				<?php foreach ($dificuldade_materia_combo as $key => $value) {
					echo form_checkbox('dificuldade_materia[]', $key, $this->input->post('dificuldade_materia') ? in_array($key, $this->input->post('dificuldade_materia')) : false) . " $value<br>";
				}
				?>
				<div>
					<label for="dificuldade_materia[]" class="error" style="display: none;">Campo obrigatório</label>
				</div>
			</div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Informe outras provas que tenha realizado e sua classificação: <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><textarea rows="3" class="form-control" type="text" name="classificacao"><?php echo $this->input->post('classificacao') ?: ''?></textarea></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Selecione os tipos de materiais com que prefere estudar <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8" title="Escolha quantos considerar bons para você. É importante que considere sua facilidade para assimilar conteúdo e sua disposição para estudar por este método">
				<?php foreach ($tipos_materiais_combo as $key => $value) {
					echo form_checkbox('tipos_materiais[]', $key, $this->input->post('tipos_materiais') ? in_array($key, $this->input->post('tipos_materiais')) : false) . " $value<br>";
				}
				?>
				<div>
					<label for="tipos_materiais[]" class="error" style="display: none;">Campo obrigatório</label>
				</div>
			</div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Indique as técnicas de estudos que já aplica ou aplicou: <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8">
				<?php foreach ($tecnicas_combo as $key => $value) {
					echo form_checkbox('tecnicas[]', $key, $this->input->post('tecnicas') ? in_array($key, $this->input->post('tecnicas')) : false) . " $value<br>";
				}
				?>
				<div>
					<label for="tecnicas[]" class="error" style="display: none;">Campo obrigatório</label>
				</div>
			</div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Você faz resumos das matérias que estuda? <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('resumos', $resumos_combo, $resumos_selecionado, 'class="form-control" id="resumos">') ?></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Tem dificuldades em ler leis diretamente? <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('dificuldade_lei', $dificuldade_lei_combo, $dificuldade_lei_selecionado, 'class="form-control" id="dificuldade_lei">') ?></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Quando você lê leis diretamente, o que você faz? <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('lei_acao', $lei_acao_combo, $lei_acao_selecionado, 'class="form-control" id="lei_acao">') ?></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Você fez a última prova deste concurso? (Considere o concurso que tem como foco principal) <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('ultima_prova', $ultima_prova_combo, $ultima_prova_selecionado, 'class="form-control" id="ultima_prova">') ?></div>
		</div>

		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Se fez a última prova, em quais matérias teve maior dificuldade?</div>
			<div class="col-12 col-md-8">
				<?php foreach ($dificuldade_prova_combo as $key => $value) {
					echo form_checkbox('dificuldade_prova[]', $key, $this->input->post('dificuldade_prova') ? in_array($key, $this->input->post('dificuldade_prova')) : false) . " $value<br>";
				}
				?>
				<div>
					<label for="dificuldade_prova[]" class="error" style="display: none;">Campo obrigatório</label>
				</div>
			</div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Especifique as matérias e explique os motivos por que teve dificuldades nas matérias acima, se aplicável.</div>
			<div class="col-12 col-md-8"><textarea rows="3" class="form-control" type="text" name="motivos"><?php echo $this->input->post('motivos') ?: ''?></textarea></div>
		</div>
		
		<div class="col-12 text-center mt-3">
        	<input class="btn u-btn-blue" type="submit" name="submit" value="Prosseguir">
		</div>
	</form>
</div>
<?php include_js_libs() ?>
<script>
$().ready(function() {
	$( document ).tooltip({
		content: function(callback) { 
		     callback($(this).prop('title').replace(/\|/g, '<br />')); 
		}
	});
	// field masks
	$("#data_graduacao").mask("99/99/9999");

	 
	$("#formulario-detalhado").validate({
		rules: {
			experiencia: {
				required: true,
			},
			trabalhando: {
				required: true,
			},
			tempo: {
				required: true,
			},
			ambiente: {
				required: true,
			},
			dificuldade_tempo: {
				required: true,
			},
			classificacao: {
				required: true,
			},
			tipos_materiais: {
				required: true,
			},
			tecnicas: {
				required: true,
			},
			resumos: {
				required: true,
			},
			dificuldade_lei: {
				required: true,
			},
			lei_acao: {
				required: true,
			},
			ultima_prova: {
				required: true,
			},
			'lugar[]': {
				required: true,
			},
			'tecnicas[]': {
				required: true,
			},
			'tipos_materiais[]': {
				required: true,
			}				
		},
		messages: {
			experiencia: {
				required: "Campo obrigatório",
			},
			trabalhando: {
				required: "Campo obrigatório",
			},
			tempo: {
				required: "Campo obrigatório",
			},
			ambiente: {
				required: "Campo obrigatório",
			},
			dificuldade_tempo: {
				required: "Campo obrigatório",
			},
			classificacao: {
				required: "Campo obrigatório",
			},
			tipos_materiais: {
				required: "Campo obrigatório",
			},
			tecnicas: {
				required: "Campo obrigatório",
			},
			resumos: {
				required: "Campo obrigatório",
			},
			dificuldade_lei: {
				required: "Campo obrigatório",
			},
			lei_acao: {
				required: "Campo obrigatório",
			},
			ultima_prova: {
				required: "Campo obrigatório",
			},
			'lugar[]': {
				required: "Campo obrigatório",
			},
			'tecnicas[]': {
				required: "Campo obrigatório",
			},
			'tipos_materiais[]': {
				required: "Campo obrigatório",
			}		
		}
	});
	
});
</script>
<?php get_footer() ?>