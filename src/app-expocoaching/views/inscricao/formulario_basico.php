<?php redirecionar_se_nao_estiver_logado() ?>
<?php get_header() ?>

<?php echo get_cabecalho_secao('bandeira-coaching.png', 'Formulário Inicial para o Coaching Exponencial Concursos')?>

<div class="container pt-1 pt-md-5 pb-5">
	<div class="row ml-auto mr-auto mt-5 mt-md-1">
		<div class="col-2 col-md-1">
			<img class="img-fluid" src="<?php echo get_tema_image_url('icone-formulario.png')?>">
		</div>
		
		<div class="col-10 col-md-11">
			Preencha estes dados com muita atenção. As informações contidas aqui servirão de base para prepararmos um plano de estudo personalizado para o seu desenvolvimento.
			<br>Se tiver dúvidas nos itens, passe o mouse sobre o campo que deve preencher e verá sua descrição.
			<br>Se ainda assim persistir sua dúvida, entre em contato conosco para que possamos ajudá-lo.
		</div>
	</div>

	<form class="mt-3" id="formulario-basico" method="post" action="<?php echo get_formulario_basico_url($item_id) ?>">
		<!-- <div class="row">
			<div class="fourcol column text-align-right">Nome: <span class="campo-obrigatorio">*</span></div>
			<div class="sevencol column"><input class="form-control" type="text" name="nome" value="<?php echo $this->input->post('nome') ?: ''?>"></div>
		</div> -->
		
		<div class="row">
			<div class="col-12 col-md-3 text-center text-md-right">Qual concurso é seu foco principal: <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('concurso', $concursos_combo, $concurso_selecionado, 'class="form-control" id="concurso">') ?></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Skype ID:</div>
			<div class="col-12 col-md-8"><input class="form-control" type="text" name="skype_id" value="<?php echo $this->input->post('skype_id') ?: ''?>"></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">CPF: <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><input class="form-control" type="text" id="cpf" name="cpf" value="<?php echo $this->input->post('cpf') ?: ''?>"></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Sexo: <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('sexo', $sexos_combo, $sexo_selecionado, 'class="form-control" id="sexo">') ?></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Onde você mora? <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('estado', $estados_combo, $estado_selecionado, 'class="form-control" id="estado">') ?></div>
		</div>
	
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Nascimento: <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><input class="form-control" type="text" id="nascimento" name="nascimento" value="<?php echo $this->input->post('nascimento') ?: ''?>"></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Mora com: <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8 ">
				<?php foreach ($mora_com_combo as $key => $value) {
					echo form_checkbox('mora_com[]', $key, $this->input->post('mora_com') ? in_array($key, $this->input->post('mora_com')) : false) . " $value<br>";
				}
				?>
				<div style="margin-top:10px">
					<label for="mora_com[]" class="error" style="display: none;">Campo obrigatório</label>
				</div>
			</div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Possui filhos: <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('filhos', $filhos_combo, $filho_selecionado, 'class="form-control" id="filhos">') ?></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Possui irmãos:</div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('irmaos', $irmaos_combo, $irmao_selecionado, 'class="form-control" id="irmaos">') ?></div>
		</div>	
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Pratica alguma atividade física?</div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('atividade', $atividades_combo, $atividade_selecionada, 'class="form-control" id="atividade">') ?></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Qual a qualidade do seu sono?</div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('qualidade_sono', $qualidades_combo, $qualidade_selecionada, 'class="form-control" id="qualidade_sono">') ?></div>
		</div>	
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Qual sua capacidade de atenção quando estudando?</div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('capacidade_atencao', $capacidades_combo, $capacidade_selecionada, 'class="form-control" id="capacidade_atencao">') ?></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Horas de estudo disponível por semana? <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8" title="Informe as horas brutas de estudo!"><?php echo form_dropdown('horas_estudo', $horas_combo, $hora_selecionada, 'class="form-control" id="horas_estudo">') ?></div>
		</div>
	
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Qual horário estuda melhor?</div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('horarios', $horarios_combo, $horario_selecionado, 'class="form-control" id="horarios">') ?></div>
		</div>
	
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Sente cansaço físico e/ou mental ao estudar? <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('cansaco', $cansacos_combo, $cansaco_selecionado, 'class="form-control" id="cansaco">') ?></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Tempo médio de sono por dia: <span class="campo-obrigatorio">*</span></div>
		
				<div class="ml-5 ml-md-3 col-2 col-md-1 p-0"><input class="form-control" type="text" name="horas_sono" value="<?php echo $this->input->post('horas_sono') ?: ''?>"></div>
				<div class="col-3 col-md-1">Horas</div>
				<div class="col-2 col-md-1 p-0"><input class="form-control" type="text" name="minutos_sono" value="<?php echo $this->input->post('minutos_sono') ?: ''?>"></div>
				<div class="col-3 col-md-1">Minutos</div>				
				<div class="mt-4">
					<label for="horas_sono" class="error" style="display: none;">Campo obrigatório</label>
				</div>
			
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Qual era sua média no colégio? <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8" title="Indique um valor aproximado, considerando sua média geral">
				<?php foreach ($medias_combo as $key => $value) {
					echo form_radio('media_colegio', $key, $this->input->post('media_colegio') ? $key == $this->input->post('media_colegio') : false) . " $value<br>";
				}
				?>
				<div style="margin-top:10px">
					<label for="media_colegio" class="error" style="display: none;">Campo obrigatório</label>
				</div>
			</div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Há quanto tempo estuda para concursos? <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('tempo_estudo', $tempo_estudo_combo, $tempo_selecionado, 'class="form-control" id="tempo_estudo">') ?></div>
		</div>	
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Já fez alguma consultoria/coaching antes? Qual(is)? <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8" title="Caso tenha feito mais de uma, informe aquela em que ficou mais tempo">
				<?php foreach ($consultorias_combo as $key => $value) {
					echo form_checkbox('consultorias[]', $key, $this->input->post('consultorias') ? in_array($key, $this->input->post('consultorias')) : false) . " $value<br>";
				}
				?>
				<div style="margin-top:10px">
					<label for="consultorias[]" class="error" style="display: none;">Campo obrigatório</label>
				</div>
			</div>
		</div>	
		
		<div class="row">
			<div class="col-12 col-md-3 text-center text-md-right">Por que decidiu fazer este concurso? <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><textarea rows="3" class="form-control" type="text" name="motivo_concurso"><?php echo $this->input->post('motivo_concurso') ?: ''?></textarea></div>
		</div>
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Por que está buscando coaching? <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><textarea rows="3" class="form-control" type="text" name="motivo_coaching"><?php echo $this->input->post('motivo_coaching') ?: ''?></textarea></div>
		</div>	
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Como conheceu o Exponencial Concursos? <span class="campo-obrigatorio">*</span></div>
			<div class="col-12 col-md-8"><?php echo form_dropdown('como_conheceu', $como_conheceu_combo, $como_conheceu_selecionado, 'class="form-control" id="como_conheceu">') ?></div>
		</div>	
		
		<div class="row mt-3">
			<div class="col-12 col-md-3 text-center text-md-right">Caso tenha conhecido o Exponencial por indicação de um aluno nosso, indique seu nome:</div>
			<div class="col-12 col-md-8"><input class="form-control" type="text" name="indicacao" value="<?php echo $this->input->post('indicacao') ?: ''?>"></div>
		</div>
		
		<div class="col-12 text-center mt-2">
        	<input class="btn u-btn-blue" type="submit" name="submit" value="Prosseguir">
		</div>
	</form>
</div>
<?php include_js_libs() ?>
<script>
$().ready(function() {
	$( document ).tooltip({
		content: function(callback) { 
		     callback($(this).prop('title').replace(/\|/g, '<br />')); 
		}
	});
	// field masks
	$("#cpf").mask("999.999.999-99");
	$("#nascimento").mask("99/99/9999");

	 
	$("#formulario-basico").validate({
		rules: {
// 			nome: {
// 				required: true,
// 				onlychar: true
// 			},
			concurso: {
				required: true,
			},
			cpf: {
				required: true,
				cpf: true
			},
			sexo: {
				required: true,
			},
			'mora_com[]': {
				required: true,
			},
			estado: {
				required: true,
			},
			date: {
				required: true,
				birthdate: true
			},
			filhos: {
				required: true,
			},
			horas_estudo: {
				required: true,
			},
			cansaco: {
				required: true,
			},
			tempo_estudo: {
				required: true,
			},
			horas_sono: {
				required: true,
			},
			media_colegio: {
				required: true,
			},
			'consultorias[]': {
				required: true,
			},
			motivo_concurso: {
				required: true,
			},
			motivo_coaching: {
				required: true,
			},
			como_conheceu: {
				required: true,
			},
			confirmacao: {
				required: true,
			},
		},
		messages: {
// 			nome: {
// 				required: "Nome é obrigatório",
// 				onlychar: "O nome deve conter apenas letras e espaços"
// 			},
			concurso: {
				required: "Campo obrigatório",
			},
			cpf: {
				required: "CPF é obrigatório",
				cpf: "CPF inválido"
			},
			sexo: {
				required: "Sexo é obrigatório",
			},
			'mora_com[]': {
				required: "Campo obrigatório",
			},
			estado: {
				required: "Campo obrigatório",
			},
			nascimento: {
				required: "Data de nascimento é obrigatória",
				birthdate: "Data de nascimento inválida"
			},
			filhos: {
				required: "Campo obrigatório",
			},
			horas_estudo: {
				required: "Campo obrigatório",
			},
			cansaco: {
				required: "Campo obrigatório",
			},
			tempo_estudo: {
				required: "Campo obrigatório",
			},
			horas_sono: {
				required: "Campo obrigatório",
			},
			media_colegio: {
				required: "Campo obrigatório",
			},
			'consultorias[]' : {
				required: "Campo obrigatório",
			},
			motivo_concurso: {
				required: "Campo obrigatório",
			},
			motivo_coaching: {
				required: "Campo obrigatório",
			},
			como_conheceu: {
				required: "Campo obrigatório",
			},
			confirmacao: {
				required: "Campo obrigatório",
			},
		}
	});
	
});
</script>
<?php get_footer() ?>