<?php get_header(); ?>

<div class="ativo-fundo p-5">
	<div class="col-12 font-pop text-white">Inscrição Ativa</div>
	<div class="container pb-5 pt-1 pt-md-5" id="coaching">	
		<div class="row">			
			    	<div class="col-md-5 col-lg-3">
			    		<div class="mt-2">
			    			<a class="btn col-12 col-xl-11 font-12 u-btn-darkblue" href="/fale-conosco">Falar com meu Coach</a>
			    		</div>
			    		<div class="mt-2">
			    			<a class="btn col-12 col-xl-11 font-12 u-btn-blue" href="/descontos_promocoes">Descontos EXCLUSIVOS</a>
			    		</div>
			    		<div class="mt-2">
			    			<a class="btn col-12 col-xl-11 font-12 u-btn-primary" href="/painel-coaching/inscricao/planilhas_detalhadas">Enviar Planilhas</a>
			    		</div>
			    	</div>				
		</div>
	</div>
</div>

<?php get_footer(); ?>			