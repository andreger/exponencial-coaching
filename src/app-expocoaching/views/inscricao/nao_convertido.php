<?php get_header(); ?>

<div class="ex-aluno-fundo pt-1 pt-md-5 pb-5">
	<div id="coaching pt-5 pb-5">		
		<div class="pt-5 pb-5">				
			<div class="pt-5 pb-5 ml-auto mr-auto col-12 col-lg-10"><h1 class="text-white">
			    	Olá, identificamos que você iniciou anteriormente o processo de inscrição sem conclusão. Caso queira dar continuidade a sua inscrição, <a class="t-d-none font-weight-bold text-blue-2 font-arial" href="/fale-conosco">clique aqui</a> e nos envie uma mensagem.</h1>	    	
			</div>
		</div>		
	</div>
</div>

<?php get_footer(); ?>			