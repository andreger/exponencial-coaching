<?php get_header(); ?>

<div class="coaching-fundo pt-5 pb-5">
	<div class="container-fluid  pt-3 pb-3" id="coaching">	
	<div class="pt-3 mb-5 col-12 text-center text-md-left text-blue">
		<h1>Inscrições - Coaching</h1>
	</div>
		
	
		<div class="mt-5 row ml-auto mr-auto">				
					<div class="col-12 col-md-5">
			        	<img class="img-fluid" src="<?php echo get_tema_image_url('painel-coaching-saco.png') ?>">
			    	</div>
			    	<div class="col-12 col-md-7 text-center font-arial">
			    		<p class="font-18 font-weight-bold">Você ainda não é nosso aluno de coaching!</p>
			    		<p>Caso queira conhecer nosso trabalho, <a class="t-d-none text-blue-2" target="_blank" href="/coaching">clique aqui</a>.</p>
						<p>Caso queira fazer sua inscrição, clique no botão 'Iniciar inscrição' abaixo.</p>
						<p>O processo de inscrição é composto por 5 fases. Caso tenha dificuldade em qualquer uma delas, entre em contato conosco pelo <a class="t-d-none text-blue-2" target="_blank" href="/fale-conosco">Fale Conosco</a>.</p>
						<p>
							<a class="btn u-btn-blue" href="<?php echo get_iniciar_inscricao_url() ?>">Iniciar Inscrição</a>
						</p>
			    	</div>
				
		</div>
	</div>
</div>

<?php get_footer(); ?>			