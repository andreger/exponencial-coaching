    	</div>
    </div>
    
    <?php if($js) : ?>
 		<?php foreach ($js as $src) : ?>
 			<script src="<?= $src ?>"></script>	
	 	<?php endforeach ?>
	<?php endif ?>   

    <script>
        $(document).ready(function() {
            
        	<?php if(isset($include_icheck)) : ?>
        	$('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            <?php endif ?>
            
			<?php if(isset($include_data_tables)) : ?>
        	$('.dataTables').dataTable({
				<?php if(isset($data_table_order)) : ?>
				"order": <?= $data_table_order ?>,
				<?php endif; ?>
				 
				<?php if(isset($data_table_length)) : ?>
				"pageLength": <?= $data_table_length ?>,
				<?php endif; ?>            	

        		language: {
        	        url: "<?php echo base_url('assets-admin/js/ext/dataTable-pt_BR.json') ?>"
        	    },
                responsive: true,
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "<?php echo base_url('assets-admin/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf') ?>",
                    "aButtons": [
                       	{
                       		"sExtends": "copy",
                       		"sButtonText": "Copiar"
                       	},
                        {
                        	"sExtends": "csv",
                        },
                        {
                        	"sExtends": "xls",
                        },
                        {
                        	"sExtends": "pdf",
                        },
                        {
                        	"sExtends": "print",
                        	"sButtonText": "Imprimir"
                        }
                	]
                }
            });
            <?php endif; ?>     

            <?php if(isset($include_tinymcepro)) : ?>
            tinymce.init({	  
            	  selector: '.tinymcepro',
            	  language: 'pt_BR',
            	  language_url: '/wp-content/themes/academy/js/tinymcepro/lang/pt_BR.js',
            	  plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount tinymcespellchecker a11ychecker imagetools textpattern help formatpainter permanentpen pageembed tinycomments mentions linkchecker',
            	  toolbar: 'formatselect | bold italic strikethrough forecolor backcolor permanentpen formatpainter | link image media pageembed | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat | addcomment',
            	  image_advtab: true,       
            	  importcss_append: true,
            	  height: 400,   	  
            	  template_cdate_format: '[CDATE: %m/%d/%Y : %H:%M:%S]',
            	  template_mdate_format: '[MDATE: %m/%d/%Y : %H:%M:%S]',
            	  image_caption: true,
            	  spellchecker_dialog: true,
            	  spellchecker_whitelist: ['Ephox', 'Moxiecode'],
            	  tinycomments_mode: 'embedded',
            	  content_style: '.mce-annotation { background: #fff0b7; } .tc-active-annotation {background: #ffe168; color: black; }'
            	 });
            <?php endif; ?> 
        });
    </script>
    
    <?php if(isset($include_data_tables)) : ?>
    <style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
	</style>
	<?php endif; ?>
</body>