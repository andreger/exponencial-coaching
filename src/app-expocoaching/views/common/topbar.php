<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
	<div class="navbar-header">
		<div class="navbar-bemvindo">
			<a title="Retrair/Voltar o menu lateral" class="navbar-minimalize minimalize-styl-1 btn btn-primary" href="#"><i class="fa fa-bars"></i></a>
			<?php 
			if(is_visitante()) {
				echo "Bem-vindo(a). Bons estudos!";
			}
			else {
				echo "Bem-vindo(a), " . get_usuario_nome_exibicao(get_current_user_id()) . "!";
			}
			?>
		</div>
	</div>
	
	<ul class="nav navbar-top-links navbar-right">
		<li>
			<a href="/">
				<?= get_tema_image_tag('sq-ir.png', null, null, null, 'vertical-bottom') ?> Ir para site
			</a>
		</li>
			
		<li>
			<a href="/questoes/login/logout">
				<?= get_tema_image_tag('sq-sair.png') ?> Sair
			</a>
		</li>
	</ul>
</nav>

<script>
$(function () {
	$('.navbar-minimalize').click(function () {
		if(sessionStorage.miniNavbar && sessionStorage.miniNavbar == 1) {
			sessionStorage.miniNavbar = 0;
		}
		else {
			sessionStorage.miniNavbar = 1;
		}
	});
});

if(sessionStorage.miniNavbar && sessionStorage.miniNavbar == 1) {
	$('body').addClass('mini-navbar');
}
else {
	$('body').removeClass('mini-navbar');	
}
</script>