<nav class="navbar-default navbar-static-side menu-admin" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="<?php menu_ativo_inativo(array('listar_metas_atomicas','listar_metas_agregadas', '', 'editar_meta_atomica')) ?>">
                <a href="#"><i class="fa fa-folder-o"></i> <span class="nav-label">Metas</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                	<li class="<?php menu_ativo_inativo('listar_metas_atomicas') ?> <?php menu_ativo_inativo('') ?>"><a href="<?= get_listar_metas_atomicas_url() ?>">Metas Atômicas</a></li>
                    <li class="<?php menu_ativo_inativo('listar_metas_agregadas') ?>"><a href="<?= get_listar_metas_agregadas_url() ?> ">Metas Agregadas</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>