<?php admin_cabecalho_pagina("Metas Atômicas") ?>

<?php echo get_mensagem_flash_ci(); ?>

<div class="container-fluid">
	<div class="row">
   		<div class="col-12 mt-5">
	    	
	    	<a class="btn btn-primary btn-default m-b m-t" href="<?= editar_meta_atomica_url() ?>"> Nova meta </a>
	    	
	    	<div class="ibox-content">
	    		<div>
	    			<a class="btn btn-outline btn-default m-b" data-toggle="modal" data-target="#filtrar-meta-modal" data-toggle="modal" href="#"><i class="fa fa-filter"></i> Filtrar </a>
	    		</div>

           		<div class="table-responsive">

           			<?php if($metas) : ?>

	            	<table id="tabela-acervos" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
			        	<thead>
			            	<tr>
			                	<th>Nome</th>
			                	<th>Criado por</th>
			                	<th>Criado em</th>
			                	<th>Editado por</th>
			                	<th>Editado em</th>
			                	<th>Disciplinas</th>
								<th>HBC</th>
								<th>Áreas</th>
			                	<th>Ações</th>
			           	 	</tr>
			        	</thead>

			        	<tbody>
			        	<?php foreach ($metas as $meta) : ?>
			        		<tr>
			        			<td><?= $meta['met_nome'] ?></td>
			        			<td><?= $meta['criador_nome'] ?></td>
			        			<td><?= converter_para_ddmmyyyy_HHiiss($meta['met_data_criacao']) ?></td>
			        			<td><?= $meta['editor_nome'] ?></td>
			        			<td><?= converter_para_ddmmyyyy_HHiiss($meta['met_data_edicao']) ?></td>
			        			<td><?= $meta['disciplinas'] ?></td>
								<td><?= $meta['met_hbc'] ?></td>
								<td><?= $meta['areas'] ?></td>
			        			<td nowrap>
									<?php if(tem_acesso([ADMINISTRADOR, COORDENADOR_COACHING]) || $meta['usu_id'] == $usuario_logado_id) : ?>
										<a href="<?= $meta['editar_url'] ?>" class="btn btn-white" title="Editar" style="margin-bottom: 5px"><i class="fa fa-pencil"></i></a> 
										<a href="<?= get_excluir_meta_atomica_url($meta['met_id']) ?>" style="margin-bottom: 5px" class="btn btn-white" onclick="return confirm('Tem certeza que deseja excluir a meta <?= $meta['met_nome'] ?> ?');" title="Excluir"><i class="fa fa-times"></i></a> 
									<?php endif; ?>
			        				<a href="<?= get_imprimir_meta_atomica_url($meta['met_id']) ?>" style="margin-bottom: 5px" target="_blank" class="btn btn-white" title="Imprimir"><i class="fa fa-print"></i></a> 
			        				<a href="<?= get_duplicar_meta_atomica_url($meta['met_id']) ?>" style="margin-bottom: 5px" class="btn btn-white" title="Duplicar"><i class="fa fa-copy"></i></a> 
								</td>
			        		</tr>
			        	<?php endforeach ?>
			        	</tbody>

			        	<tfoot>
			            	<tr>
			                	<th>Nome</th>
			                	<th>Criado por</th>
			                	<th>Criado em</th>
			                	<th>Editado por</th>
			                	<th>Editado em</th>
			                	<th>Disciplinas</th>
								<th>HBC</th>
								<th>Áreas</th>
			                	<th>Ações</th>
			           	 	</tr>
			        	</tfoot>
			    	</table>
			    	
			    	<div><?= $links ?></div>

			    	<?php else : ?>
			    	
			    	<div>Nenhuma meta foi cadastrada.</div>

			    	<?php endif; ?>
            	</div>
        	</div>
    	</div>
	</div>
</div>

<?php $this->view('modals/admin_meta_filtro', ['form_id' => 'form-listar-questao']);