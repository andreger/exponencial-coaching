<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="ibox float-e-margins">
                <div class="ibox-content" style="border: 0">
                    <form method="post" class="form-horizontal" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Nome:</label>
                            <div class="col-md-10" style="margin-top: 7px">
                               	<?= $met_nome ?>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-md-2 control-label">Disciplina:</label>
                            <div class="col-md-10" style="margin-top: 7px">
                                <?= $met_disciplinas ?>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-md-2 control-label">Assuntos:</label>
                            <div class="col-md-10" style="margin-top: 7px">
                                <?= $met_assuntos ?>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-md-2 control-label">Orientações:</label>
                            <div class="col-md-10" style="margin-top: 7px">
                                <?= $met_orientacao ?>
                            </div>
                        </div> 

                        <div class="form-group"><label class="col-md-2 control-label">Cadernos:</label>
                            <div class="col-md-10" style="margin-top: 7px">
                                <?= $met_cadernos ?>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="col-md-2 control-label">HBC:</label>
                            <div class="col-md-10" style="margin-top: 7px">
                                <?= $met_hbc ?>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-md-2 control-label">Área:</label>
                            <div class="col-md-10" style="margin-top: 7px">
                                <?= $met_areas  ?>
                            </div>
                        </div> 

                        <div class="form-group"><label class="col-md-2 control-label">Anotações:</label>
                            <div class="col-md-10" style="margin-top: 7px">
                                <?= $met_anotacao ?>
                            </div>
                        </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>