
<?php admin_cabecalho_pagina( (isset($meta['mea_id'])?"Editar":"Nova") . " Meta Agregada") ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form id="editar_meta_agregada_form" method="post" class="form-horizontal" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Nome</label>
                            <div class="col-md-10">
                            	<input type="text" class="form-control" name="mea_nome" value="<?= $_POST['mea_nome']?:$meta['mea_nome'] ?>">
    							<span class="help-block m-b-none error"><?= form_error('mea_nome'); ?></span>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-md-2 control-label">Área</label>
                            <div class="col-md-10">
                                <?= form_multiselect('are_ids[]', $areas, $_POST['are_ids']?:$meta['mea_areas'], "class='form-control chosen-select' data-placeholder='Escolha áreas...' id='are_ids' autocomplete='off' ") ?>
                                <span class="help-block m-b-none error"><?= form_error('are_ids'); ?></span>
                            </div>
                        </div> 

                        <div class="form-group"><label class="col-md-2 control-label">Anotações</label>
                            <div class="col-md-10">
                                <textarea type="text" class="form-control" name="mea_anotacoes" ><?= $_POST['mea_anotacoes']?:$meta['mea_anotacoes'] ?></textarea>
                                <span class="help-block m-b-none error"><?= form_error('mea_anotacao'); ?></span>
                            </div>
                        </div> 

						<div class="form-group"><label class="col-md-2 control-label">Metas atômicas</label>
                            <div class="col-md-10">
                            	<a class="btn btn-outline btn-default m-b" data-toggle="modal" data-target="#filtrar-meta-modal" data-toggle="modal" href="#"><i class="fa fa-plus"></i> Selecionar </a>
                            </div>
                        </div>

                        <div class="form-group">
                        	<div class="col-md-2"></div>
                            <div class="col-md-5">
                                <div class="dd" id="nestable">
                                    <ol class="dd-list">
                                        <?php foreach($meta['mea_metas_atomicas'] as $meta_atomica): ?>
                                        <li class="dd-item" data-met_id="<?= $meta_atomica['met_id'] ?>">
                                            <div class="dd-handle"><?= $meta_atomica['met_nome'] ?><a href="#" class="close remover_meta dd-nodrag">x</a></div>
                                        </li>
                                        <?php endforeach; ?>
                                    </ol>
                                </div>
                            </div>
                            <input id="met_ids" name="met_ids" type="hidden" value='<?= json_encode($meta['mea_met_ids']) ?>' />
                        </div>
                       
                        <div class="form-group mr-3 mt-4">
                            <div class="col-md-10 offset-md-2 ml-auto mr-5 text-center">
                                <a class="btn btn-white" href="/painel-coaching/admin/listar_metas_agregadas">Cancelar</a>
                                <button class="btn btn-primary" name="salvar" value="1"  type="submit">Salvar</button>
                            </div>
                        </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->view("modals/editar_meta_arvore_assuntos") ?>

<?php $this->view('modals/admin_meta_filtro', ['form_id' => 'editar_meta_agregada_form']); ?>

<?php $this->view('modals/admin_metas_atomicas'); ?>

<script>
<?php if($is_exibir_busca): ?>
	$(document).ready(function(){
		$("#listar-meta-modal").modal("show");
	});
<?php endif; ?>
</script>