<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Painel do Coaching</title>

    <link href="/painel-coaching/assets-admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="/painel-coaching/assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="/painel-coaching/assets-admin/css/animate.css" rel="stylesheet">
    <link href="/painel-coaching/assets-admin/css/style.css" rel="stylesheet">
    <link href="/questoes/assets-admin/css/custom.css" rel="stylesheet">

</head>
<body>
<div class="row wrapper border-bottom white-bg page-heading" style="margin-top: 30px">
    <div class="col-10 pt-4 pl-4 text-center">
    	<div><img src='/wp-content/themes/academy/images/logo2018.png' width="300px"></div>
        <h1>Meta Agregada</h1>
    </div>
</div>

<?php foreach ($metas_atomicas as $meta): ?>
    <div class="row wrapper border-bottom white-bg page-heading" style="margin-top: 30px">
    	<?php $this->view("admin/imprimir_meta_corpo", $meta) ?>
    </div>
<?php endforeach; ?>

<script>
window.print();
</script>
</body>
