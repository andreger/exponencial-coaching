<?php admin_cabecalho_pagina( (isset($met_id)?"Editar":"Nova") . " Meta Atômica") ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Nome</label>
                            <div class="col-md-10">
                            	<input type="text" class="form-control" name="met_nome" value="<?= $met_nome ?>">
    							<span class="help-block m-b-none error"><?= form_error('met_nome'); ?></span>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-md-2 control-label">Disciplina</label>
                            <div class="col-md-10">
                                <?= form_multiselect('dis_ids[]', $disciplinas, $met_disciplinas, "class='form-control chosen-select' data-placeholder='Escolha disciplinas...' id='dis_ids' autocomplete='off' ") ?>
                                <span class="help-block m-b-none error"><?= form_error('dis_ids'); ?></span>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-md-2 control-label">Assuntos</label>
                            <div class="col-md-10">
                                <?= form_multiselect('ass_ids[]', $assuntos, $met_assuntos, "class='form-control chosen-select' data-placeholder='Escolha assuntos...' id='ass_ids' autocomplete='off' ") ?>
                                <div>
        							<a id="arvore-btn" data-toggle="modal" href="#modal-arvore-assuntos">Árvore de assuntos</a>
        						</div>
                                <span class="help-block m-b-none error"><?= form_error('ass_ids'); ?></span>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-md-2 control-label">Orientações</label>
                            <div class="col-md-10">
                                <textarea class="summernote" name="met_orientacao"><?= $met_orientacao ?></textarea>
                                <span class="help-block m-b-none error"><?= form_error('met_orientacao'); ?></span>
                            </div>
                        </div> 

                        <div class="form-group"><label class="col-md-2 control-label">Cadernos</label>
                            <div class="col-md-10">
                                <?= form_multiselect('cad_ids[]', $cadernos, $met_cadernos, "class='form-control chosen-select' data-placeholder='Escolha cadernos...' id='cad_ids' autocomplete='off' ") ?>
                                <span class="help-block m-b-none error"><?= form_error('cad_ids'); ?></span>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="col-md-2 control-label">HBC</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="met_hbc" id="met_hbc" value="<?= $met_hbc ?>" placeholder="hh:mm">
                                <span class="help-block m-b-none error"><?= form_error('met_hbc'); ?></span>
                            </div>
                        </div> 

                        <div class="form-group"><label class="col-md-2 control-label">Área</label>
                            <div class="col-md-10">
                                <?= form_multiselect('are_ids[]', $areas, $met_areas, "class='form-control chosen-select' data-placeholder='Escolha áreas...' id='cad_ids' autocomplete='off' ") ?>
                                <span class="help-block m-b-none error"><?= form_error('are_ids'); ?></span>
                            </div>
                        </div> 

                        <div class="form-group"><label class="col-md-2 control-label">Anotações</label>
                            <div class="col-md-10">
                                <textarea type="text" class="form-control" name="met_anotacao" ><?= $met_anotacao ?></textarea>
                                <span class="help-block m-b-none error"><?= form_error('met_anotacao'); ?></span>
                            </div>
                        </div> 
                       
                        <div class="form-group mr-3 mt-4">
                            <div class="col-md-10 offset-md-2 ml-auto mr-5 text-center">
                                <a class="btn btn-white" href="<?= get_listar_metas_atomicas_url() ?>">Cancelar</a>
                                <button class="btn btn-primary" name="salvar" value="1"  type="submit">Salvar</button>
                            </div>
                        </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->view("modals/editar_meta_arvore_assuntos") ?>