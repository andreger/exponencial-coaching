<div class="modal fade" id="filtrar-meta-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content modal-form-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Filtrar Metas</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="form-listar-questao" class="form-horizontal">
			
					<div class="form-group">
						<label class="col-sm-2">Nome</label>

						<div class="col-sm-10">
							<?= form_input('nome', $filtro['nome_selecionado'],'id="nome" placeholder="Nome" class="form-control m-b" form="'. $form_id . '"'); ?>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2">Autores</label>

						<div class="col-sm-10">
						<?= form_multiselect('consultores_ids[]', $consultores, $filtro['consultores_selecionados'], 'id="consultores_ids" class="multiselect form-control m-b" form="'. $form_id . '" data-placeholder="Escolha autores..."'); ?>
						</div>
						<div style="clear:both"></div>
					</div>

					<div class="form-group">
						<label class="col-sm-2">Disciplinas</label>

						<div class="col-sm-10">
						<?= form_multiselect('disciplinas_ids[]', $disciplinas, $filtro['disciplinas_selecionadas'], "class='form-control multiselect m-b' form='".$form_id."' data-placeholder='Escolha disciplinas...' id='disciplinas_ids' autocomplete='off' ") ?>
						</div>
						<div style="clear:both"></div>
					</div>

					<div class="form-group">
						<label class="col-sm-2">Assuntos</label>

						<div class="col-sm-10">
						<?= form_multiselect('assuntos_ids[]', $assuntos, $filtro['assuntos_selecionados'], 'id="assuntos_ids" class="multiselect form-control m-b" form="'. $form_id . '" data-placeholder="Escolha assuntos..."'); ?>
						</div>
						<div style="clear:both"></div>
					</div>

					<div class="form-group">
						<label class="col-sm-2">HBC</label>			

						<div class="col-sm-5">
							<?= form_input('hbc_de', $filtro['hbc_de'],'id="hbc-de" placeholder="De: (hh:mm)" class="form-control m-b hbc" form="'. $form_id . '"'); ?>
						</div>
						
						<div class="col-sm-5">
							<?= form_input('hbc_ate', $filtro['hbc_ate'],'id="hbc-ate" placeholder="Até: (hh:mm)" class="form-control m-b hbc" form="'. $form_id . '"'); ?>
						</div>
						
						<div style="clear:both"></div>
					</div>

					<div class="form-group">
						<label class="col-sm-2">Data de Criação</label>

						<div class="col-sm-5">
							<?= form_input('data_criacao_de', $filtro['data_criacao_de'],'id="data_criacao_de" class="form-control m-b data" form="'. $form_id . '"  placeholder="De:" '); ?>
						</div>

						<div class="col-sm-5">
							<?= form_input('data_criacao_ate', $filtro['data_criacao_ate'],'id="data_criacao_ate" class="form-control m-b data" form="'. $form_id . '" placeholder="Até:"'); ?>
						</div>

						<div style="clear:both"></div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2">Data de Edição</label>

						<div class="col-sm-5">
							<?= form_input('data_edicao_de', $filtro['data_edicao_de'], 'id="data_edicao_de" class="form-control m-b data" form="'. $form_id . '" placeholder="De:" '); ?>
						</div>

						<div class="col-sm-5">
							<?= form_input('data_edicao_ate', $filtro['data_edicao_ate'], 'id="data_edicao_ate" class="form-control m-b data" form="'. $form_id . '" placeholder="Até:"'); ?>
						</div>

						<div style="clear:both"></div>
					</div>

					<div class="form-group">
						<?= form_submit('buscar', 'Buscar!', 'class="btn btn-primary" form="'. $form_id . '" id="filtro-submit"'); ?>
						<a class="btn btn-outline btn-default" id="limpar-botao-modal">Limpar Filtros</a>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>