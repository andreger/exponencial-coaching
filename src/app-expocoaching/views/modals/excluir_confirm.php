<div class="modal inmodal fade" id="excluir_confirm" tabindex="-1" role="dialog" aria-hidden="true">
	<form method="post" id="excluir-form">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title">Confirmar exclusão...</h4>
				</div>
				<div class="modal-body">
					<p>
						Você <strong>confirma a EXCLUSÃO</strong> do item selecionado?
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
					<input type="submit" value="Excluir" class="btn btn-primary">
				</div>
			</div>
		</div>
	</form>
</div>
