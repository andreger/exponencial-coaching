<div class="modal fade" id="listar-meta-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content modal-form-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Selecionar Metas Atômicas</h4>
			</div>
			<div class="modal-body">

				<div class="form-group">
					<a class="btn btn-primary btn-default" id="adicionar-metas-modal" href="#">Adicionar selecionadas</a>
					<a class="btn btn-primary btn-default" data-dismiss="modal" href="#">Voltar para Meta</a>
					<a class="btn btn-primary btn-default" onclick="$('#listar-meta-modal').modal('hide');" data-toggle="modal" data-target="#filtrar-meta-modal" data-toggle="modal" href="#">Exibir filtro</a>
				</div>
				
				<div class="table-responsive">

           			<?php if($metas_busca) : ?>

    	            	<table id="tabela-metas-atomias-modal" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
    			        	<thead>
    			            	<tr>
    			            		<th></th>
    			                	<th>Nome</th>
    								<th>Disciplina</th>
    			                	<th>Área</th>
    								<th>Criado por</th>
    			                	<th>Criado em</th>
    			           	 	</tr>
    			        	</thead>
    
    			        	<tbody>
    			        	<?php foreach ($metas_busca as $meta) : ?>
    			        		<tr>
    			        			<td><?= form_checkbox("busca_met_ids[]", $meta['met_id'], FALSE, "data-met_nome='{$meta['met_nome']}'") ?></td>
    			        			<td><?= $meta['met_nome'] ?></td>
    			        			<td><?= $meta['disciplinas'] ?></td>
    								<td><?= $meta['areas'] ?></td>
    			        			<td><?= $meta['criador_nome'] ?></td>
    			        			<td><?= converter_para_ddmmyyyy_HHiiss($meta['met_data_criacao']) ?></td>
    			        		</tr>
    			        	<?php endforeach ?>
    			        	</tbody>
    
    			        	<tfoot>
    			            	<tr>
    			                	<th></th>
    			                	<th>Nome</th>
    								<th>Disciplina</th>
    			                	<th>Área</th>
    								<th>Criado por</th>
    			                	<th>Criado em</th>
    			           	 	</tr>
    			        	</tfoot>
    			    	</table>
    			    	
			    	<?php else : ?>
			    	
			    		<div>Nenhuma meta foi encontrada.</div>

			    	<?php endif; ?>
            	</div>
				
			</div>
		</div>
	</div>

</div>