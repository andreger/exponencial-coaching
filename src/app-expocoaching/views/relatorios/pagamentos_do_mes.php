<?php get_header() ?>
<div class="container pt-1 pt-md-5 pb-5">
	<div class="mb-5 col-12 text-center text-md-left text-blue">
		<h1>Relatório de pagamentos do mês</h1>
	</div>


<?php if($pagamentos) : ?>	
	<div class="mt-5">
		<?php echo get_pagamentos_do_mes_table($pagamentos) ?>
	</div>
<?php else : ?>
	<div class="mt-3 text-center">
		Não há pagamentos para o mês atual
	</div>
<?php endif; ?>	
</div>
<?php get_footer() ?>
