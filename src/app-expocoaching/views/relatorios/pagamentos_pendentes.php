<?php get_header() ?>
<div class="container pt-1 pt-md-4 pb-5">
	<div class="mb-5 col-12 text-center text-md-left text-blue">
		<h1>Relatório de pagamentos pendentes</h1>
	</div>

<?php if($data_relatorio) : ?>
	<div class="mt-4 mb-3">	
		<a class="mb-3 btn u-btn-blue" href="<?php echo get_relatorio_exportar_pagamentos_pendentes_url() ?>">Exportar Excel</a>	
	    <p>Última atualização em <?php echo $data_relatorio ?></p>    
			
	</div>
	
	<div class="mt-3">
		<?php echo get_pagamentos_pendentes_table($pagamentos_pendentes) ?>
	</div>
<?php else : ?>
	<div>
		Não há pagamentos pendentes no mês atual
	</div>
</div>	
<?php endif; ?>	
<?php get_footer() ?>
