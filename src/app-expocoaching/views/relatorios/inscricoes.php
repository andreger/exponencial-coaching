<?php get_header() ?>

<div class="container-fluid pt-1 pt-md-4">
	<?= get_mensagem_flash_wp() ?>
	<div class="mb-5 col-12 text-center text-md-left text-blue">
		<h1>Coaching - Inscrições</h1>
	</div>
	

	<table border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-striped" style="overflow-x: scroll;display:block">
		<thead>
			<tr class="font-10 bg-blue text-white">
				<th></th>
				<th>Nome</th>
				<th>E-mail</th>
				<th>Etapa</th>
				<th>Formulário Básico</th>
				<th>Formulário Detalhado</th>
				<th>Perfil Psicopedagógico</th>
				<th>Planilhas Detalhadas</th>
				<th>Área</th>
			</tr>
		</thead>
		
		<tbody>
		<?php $i = 1; foreach ($alunos as $aluno) : ?>
			<tr>
				<td><?= $i++ ?></td>
				<td><?= $aluno['nome'] ?></td>
				<td><?= $aluno['email'] ?></td>
				<td><?= $aluno['etapa'] ?></td>
				<td><?php
						if(isset($aluno['data_formulario_basico'])) {
							echo $aluno['data_formulario_basico']->format('d/m/Y');
						}
					?>
				</td>
				<td><?php
						if(isset($aluno['data_formulario_detalhado'])) {
							echo $aluno['data_formulario_detalhado']->format('d/m/Y');
						}
					?>
				</td>
				<td><?php
						if(isset($aluno['data_perfil_psicopedagogico'])) {
							echo $aluno['data_perfil_psicopedagogico']->format('d/m/Y');
						}
					?>
				</td>
				<td><?php
						if(isset($aluno['data_planilhas_detalhadas'])) {
							echo $aluno['data_planilhas_detalhadas']->format('d/m/Y');
						}
					?>
				</td>
				<td><?= $aluno['area'];	?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>


<?php get_footer() ?>
