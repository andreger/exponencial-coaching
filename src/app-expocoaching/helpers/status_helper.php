<?php
function get_etapa_id($aluno) 
{
	switch($aluno['etapa']) {
		case 'Ativo / Formulário básico': return 1;
		case 'Ativo / Formulário detalhado': return 2;
		case 'Ativo / Perfil psicopedagógico': return 3;
		case 'Ativo / Planilhas detalhadas': return 4;
		case 'Inscrição pendente':
		case 'Pagamento liberado': return 5;
		case 'Ativo': return 6;
		case 'Ex-aluno': return 7;
		case 'Não convertido': return 8;
	}
	return null;
}

function is_etapa_status_ativo($etapa)
{
	return in_array($etapa, [1,2,3,4,6]) ? true : false;
}