<?php
function remover_tracos($str)
{
	return str_replace('-', '', $str);	
}

function formatar_data_para_nota_fiscal($str)
{
	return str_replace(' ', 'T', $str);
}

function formatar_data_para_interface($str)
{
	return date('d/m/Y H:i:s', strtotime($str));
}

function strip_accents($input)
{
	$array1 = array( "á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç"
			, "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç" );
	$array2 = array( "a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c"
			, "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C" );
	return str_replace( $array1, $array2, $input);
}

function slugify($input)
{
	$pre_slug = strip_accents($input);
	return url_title($pre_slug,'dash',true);
}