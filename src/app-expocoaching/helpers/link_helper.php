<?php
function get_servico_indisponivel_url()
{
	return base_url('/status/servico_indisponivel');
}

function get_iniciar_inscricao_url()
{
	return base_url('/inscricao/iniciar?action=start');
}

function get_inscricao_home_url()
{
	return base_url('/inscricao?action=home');
}

function get_novo_aluno_url() 
{
	return base_url('/inscricao/novo_aluno');
}

function get_formulario_basico_url()
{
	return base_url('/inscricao/formulario_basico/');
}

function get_formulario_detalhado_url()
{
	return base_url('/inscricao/formulario_detalhado/');
}

function get_perfil_psicopedagogico_url()
{
	return base_url('/inscricao/perfil_psicopedagogico/');
}

function get_planilhas_detalhadas_url()
{
	return base_url('/inscricao/planilhas_detalhadas/');
}

function get_solicitar_planilhas_detalhadas_url($item_id)
{
	return base_url('/inscricao/solicitar_planilhas_detalhadas/' . $item_id);
}

function get_ativar_inscricao_url($item_id)
{
	return base_url('/inscricao/ativar/' . $item_id);
}

function get_liberar_pagamento_url($item_id)
{
	return base_url('/inscricao/liberar_pagamento/' . $item_id);
}

function get_continuar_inscricao_url($item_id)
{
	return base_url('/inscricao/continuar_inscricao/' . $item_id);
}

function get_relatorio_inscricoes()
{
	return base_url('/relatorios/inscricoes');
}

function get_relatorio_exportar_pagamentos_pendentes_url()
{
	return base_url('/relatorios/exportar_pagamentos_pendentes');
}