<?php
function botao_liberar_pagamento($aluno)
{
	if($aluno['etapa'] == 'Inscrição pendente') {
		$url = get_liberar_pagamento_url($aluno['item_id']);	
		echo "<a href='{$url}'>Liberar pagamento</a>";
	}
}

function botao_ativar_inscricao($aluno)
{
	if($aluno['etapa'] == 'Pagamento liberado') {
		$url = get_ativar_inscricao_url($aluno['item_id']);
		echo "<a href='{$url}'>Ativar inscrição</a>";
	}
}

function botao_solicitar_planilhas($aluno)
{
	if($aluno['etapa'] == 'Planilhas detalhadas') {
		$url = get_solicitar_planilhas_detalhadas_url($aluno['item_id']);
		echo "<a href='{$url}'>Solicitar Planilhas</a>";
	}	
}

function get_fase_class_id($fase, $fase_aluno, $url = false) 
{
	$href = $url ? "style='cursor:pointer' onclick=\"window.location='$url'\"" : "";
	echo $fase == $fase_aluno ? "id='fase-$fase-atual' class='coaching-fase fase-$fase-atual' $href" : "id='fase-$fase' class='coaching-fase'";
}

function include_js_libs()
{
	echo "<link rel='stylesheet' href='//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css' />";
	echo "<script src='//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>";
	echo "<script src='//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js'></script>";
	echo "<script src='/wp-content/themes/academy/js/jquery.validate.min.js'></script>";
	echo "<script src='/wp-content/themes/academy/js/jquery.maskedinput.min.js'></script>";
	echo "<script>jQuery.validator.addMethod('birthdate', function(value, element) {
				return this.optional(element) || /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/.test(value);
			}, 'Data de nascimento inválida');
			jQuery.validator.addMethod('brphone', function(value, element) {
				value = value.replace('_','');
				return this.optional(element) || /^([\(]{1}[0-9]{2,3}[\)]{1}[ ]{1}[0-9]{3,5}[\-]{1}[0-9]{3,4})$/.test(value);
			}, 'Telefone inválido');
			jQuery.validator.addMethod('onlychar', function(value, element) {
				return this.optional(element) || /^([a-zA-ZãõáéíóúâêôçÃÕÁÉÍÓÚÂÊÔÇ]+\s)*[a-zA-ZãõáéíóúâêôçÃÕÁÉÍÓÚÂÊÔÇ]+$/.test(value);
			}, 'Nome inválido');
			jQuery.validator.addMethod('cpf', function (cpf, element) {
				cpf = cpf.replace('.','');
				cpf = cpf.replace('.','');
				cpf = cpf.replace('-','');
			
				while (cpf.length < 11) cpf = '0' + cpf;
				var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
				var a = [];
				var b = new Number;
				var c = 11;
				for (i = 0; i < 11; i++) {
					a[i] = cpf.charAt(i);
					if (i < 9) b += (a[i] * --c);
				}
				if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11 - x }
				b = 0;
				c = 11;
				for (y = 0; y < 10; y++) b += (a[y] * c--);
				if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11 - x; }
				if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) return this.optional(element) || false;
				return this.optional(element) || true;
			}, 'Informe um CPF válido.');
	</script>";
}

function get_pagamentos_pendentes_table($pagamentos_pendentes)
{
	$table = "<table border='0' cellspacing='0' cellpadding='0' class='table table-striped table-bordered'>
				<thead>
				<tr class='font-10 bg-blue text-white'>
					<th></th>
					<th>Nome</th>
					<th>E-mail</th>
					<th>Coach</th>
					<th>Área</th>
					<th>Dia de Pagamento</th>
					<th>Último Pagamento</th>
				</tr>
				</thead>
	
				<tbody>";

	$i = 0;
	foreach ($pagamentos_pendentes as $pagamentos) {
		$i++;
		$ultimo_pagamento = converter_para_ddmmyyyy($pagamentos['ppe_ultimo_pagamento']);
		$table .= "<tr>
					<td>{$i}</td>
					<td>{$pagamentos['ppe_nome']}</td>
					<td>{$pagamentos['ppe_email']}</td>
					<td>{$pagamentos['ppe_coach']}</td>
					<td>{$pagamentos['ppe_turma']}</td>
					<td>{$pagamentos['ppe_data_pgto']}</td>
					<td>{$ultimo_pagamento}</td>
				</tr>";
	}
	
	$table .= "</tbody>
		</table>";
	
	return $table;
}

function get_pagamentos_do_mes_table($pagamentos)
{	
	$table = "<table border='0' cellspacing='0' cellpadding='0' class='table table-striped table-bordered'>
				<thead>
				<tr class='font-10 bg-blue text-white text-center'>
					<th></th>
					<th>Nome</th>
					<th>E-mail</th>
					<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Coach&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
					<th>Área</th>
					<th>Data do Pagamento</th>
					<th>Último Pagamento</th>
					<th clas='text-center'>Data E-mail Cobrança</th>
				</tr>
				</thead>

				<tbody>";

	$i = 0;
	foreach ($pagamentos as $pagamento) {
		// ignora usuários que não tenham dia de pagamento definido.
		if(!isset($pagamento['referencia_pgto'])) continue;
		
		$data_pagamento = date('Y-m-') . $pagamento['referencia_pgto']->format('d');
		$data_pagamento_ddmmyyyy = converter_para_ddmmyyyy($data_pagamento);
		$data_cobranca = date('Y-m-d', strtotime($data_pagamento . ' -' . EMAIL_COBRANCA_COACHING_DIAS_ANTES . 'days'));
		$data_cobranca_ddmmyyyy = converter_para_ddmmyyyy($data_cobranca);
		$ultimo_pagamento = converter_para_ddmmyyyy($pagamento['ultimo_pagamento']);
		
		//Se último pagamento for maior do que a data de pagamento pular linha
		if($pagamento['ultimo_pagamento'] >= $data_pagamento) continue;
		
		$i++;
		$table .= "<tr class='text-center'>
		<td>{$i}</td>
		<td>{$pagamento['nome']}</td>
		<td>{$pagamento['email']}</td>
		<td>{$pagamento['coach']}</td>
		<td>{$pagamento['area']}</td>
		<td>{$data_pagamento_ddmmyyyy}</td>
		<td>{$ultimo_pagamento}</td>
		<td>{$data_cobranca_ddmmyyyy}</td>
		</tr>";
	}
	
	$table .= "</tbody>
		</table>";

	if($i == 0) {
		$table = "<div style='text-align:center; margin: 40px'>Não há alunos com pagamentos abertos no mês</div>";
	}
	
	return $table;
}


function get_combo_options($dados)
{
	$options = array();
	foreach ($dados as $dado) {
		$options[$dado['cco_valor']] = $dado['cco_texto'];
	}
	
	return $options;
}

function get_caixa_mensagem($mensagem, $tipo)
{
    if(empty($mensagem)) return;
    
    $classe = '';
    if($tipo == 'sucesso')
        $classe = 'alert-success';
        elseif($tipo == 'erro')
        $classe = 'alert-danger';
        
        return
        "<div class='alert {$classe} alert-dismissable' style='margin-left: 1.5rem; margin-right: 1.5rem;'>
			<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
			{$mensagem}
		</div>";
}

function pagination_config($base_url, $total_rows, $per_page, $url_segment = 3)
{
    $config = array();
    $config["base_url"] = $base_url;
    $config["total_rows"] = $total_rows;
    $config["per_page"] = $per_page;
    $config['uri_segment'] = $url_segment;
    $config['num_links'] = 3;
    $config['full_tag_open'] = '<div class="btn-group btn-pagination">';
    $config['full_tag_close'] = '</div>';
    $config['first_link'] = 'Primeiro';
    $config['first_tag_open'] = '<div class="btn btn-white">';
    $config['first_tag_close'] = '</div>';
    $config['last_link'] = 'Último';
    $config['last_tag_open'] = '<div class="btn btn-white">';
    $config['last_tag_close'] = '</div>';
    $config['next_link'] = '<i class="fa fa-chevron-right"></i>';
    $config['next_tag_open'] = '<div class="btn btn-white">';
    $config['next_tag_close'] = '</div>';
    $config['prev_link'] = '<i class="fa fa-chevron-left"></i>';
    $config['prev_tag_open'] = '<div class="btn btn-white">';
    $config['prev_tag_close'] = '</div>';
    $config['num_tag_open'] = '<div class="btn btn-white">';
    $config['num_tag_close'] = '</div>';
    $config['cur_tag_open'] = '<div class="btn btn-white  active">';
    $config['cur_tag_close'] = '</div>';
    
    return $config;
}

/**
 * Grupo de componentes para Cabeçalhos de páginas
 */
function get_admin_cabecalho_pagina($titulo)
{
    return
    "<div class='row wrapper white-bg page-heading'>
			<div class='col-lg-12'>
				<h1>{$titulo}</h1>
			</div>
		</div>";
}

function admin_cabecalho_pagina($titulo)
{
    echo get_admin_cabecalho_pagina($titulo);
}