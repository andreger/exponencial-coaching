<?php 
function set_mensagem_flash($tipo, $mensagem)
{
	$_SESSION['mflash'] = array(
			'tipo' => $tipo,
			'mensagem' => $mensagem
	);
}

function get_mensagem_flash_wp()
{
	if(isset($_SESSION['mflash'])) {

		if($flash = $_SESSION['mflash']) {
			$_SESSION['mflash'] = NULL;

			return "<div class='register-success coaching-msg text-center'>{$flash['mensagem']}</div>
					<script>setTimeout(function() { jQuery('.coaching-msg').hide('slow') }, 5000);</script>";	
		}
	}	
}

function get_mensagem_flash_ci()
{
    $flash = $_SESSION['mflash'];
    
    $caixa = get_caixa_mensagem($flash['mensagem'], $flash['tipo']);
    $_SESSION['mflash'] = null;
    
    return $caixa;
}

function get_action()
{
	$CI =&get_instance();
	return $CI->uri->segment(2);
}
/**
 * Inicia o controle do profiler do CodeIniter.
 * Se houver um cookie KPROFILER_ENABLE exibe o profiler do codeigniter
 */
function init_profiler()
{
    if($_COOKIE['KDEBUG_ENABLE']) {
        $CI =& get_instance();
        $CI->output->enable_profiler(true);
    }
}