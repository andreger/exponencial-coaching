<?php
use MatthiasMullie\Minify;

function get_css_minified()
{
    
    $arquivos = [
        'assets-admin/font-awesome/css/font-awesome.css',
        'assets-admin/css/plugins/toastr/toastr.min.css',
        'assets-admin/js/plugins/gritter/jquery.gritter.css',
        'assets-admin/css/plugins/chosen/chosen.css',
        'assets-admin/js/plugins/select2/select2.css',
        'assets-admin/js/plugins/select2/select2-bootstrap.css',
        'assets-admin/css/plugins/dataTables/dataTables.bootstrap.css',
        'assets-admin/css/plugins/dataTables/dataTables.responsive.css',
        'assets-admin/css/plugins/dataTables/dataTables.tableTools.min.css',
        'assets-admin/css/plugins/iCheck/custom.css',
        'assets-admin/css/plugins/summernote/summernote.css',
        'assets-admin/css/plugins/summernote/summernote-bs3.css',
        'assets-admin/css/plugins/datapicker/datepicker3.css',
        'assets-admin/css/plugins/jquery-stars/stars.css',
        'assets-admin/css/plugins/daterangepicker2/daterangepicker.css',
        'assets-admin/css/plugins/steps/jquery.steps.css',
        'assets-admin/css/plugins/jsTree/style.min.css',
        'assets-admin/css/plugins/jsTree/jtree-ext.css',
        'assets-admin/css/plugins/cropper/cropper.min.css',
        'assets-admin/css/animate.css',
        'assets-admin/css/style.css',
        'assets-admin/css/custom.css',
        'assets-admin/css/mobile.css',
    ];
    
    
    $sourcePath = "assets-admin/min/all-" . VERSAO . ".css";
    
    $minifier = new Minify\CSS($arquivos[0]);
    
    for($i = 1; $i < count($arquivos); $i++) {
        $minifier->add($arquivos[$i]);
    }
    
    $minifier->minify($sourcePath);
    return base_url($sourcePath);
}

function get_js_minified()
{
    
    $arquivos = [
        'assets-admin/js/plugins/jquery-ui/jquery-ui.min.js',
        'assets-admin/js/plugins/metisMenu/jquery.metisMenu.js',
        'assets-admin/js/plugins/slimscroll/jquery.slimscroll.min.js',
        'assets-admin/js/inspinia.js',
        'assets-admin/js/plugins/pace/pace.min.js',
        'assets-admin/js/jquery.form.js',
        'assets-admin/js/plugins/toastr/toastr.min.js',
        'assets-admin/js/plugins/dataTables/jquery.dataTables.js',
        'assets-admin/js/plugins/dataTables/dataTables.bootstrap.js',
        'assets-admin/js/plugins/dataTables/dataTables.responsive.js',
        'assets-admin/js/plugins/dataTables/dataTables.tableTools.min.js',
        'assets-admin/js/plugins/staps/jquery.steps.min.js',
        'assets-admin/js/plugins/validate/jquery.validate.min.js',
        'assets-admin/js/plugins/validate/additional-methods.min.js',
        'assets-admin/js/validate.translate.js',
        'assets-admin/js/plugins/datapicker/bootstrap-datepicker.js',
        'assets-admin/js/jquery.chained.remote.min.js',
        'assets-admin/js/plugins/summernote/summernote.js',
        'assets-admin/js/plugins/summernote/lang/summernote-pt-BR.js',
        'assets-admin/js/ext/summernote-ext.js',
        'assets-admin/js/ext/summernote-ext-specialchars.js',
        'assets-admin/js/ext/summernote-ext-myenter.js',
        'assets-admin/js/plugins/chosen/chosen.jquery.js',
        'assets-admin/js/plugins/select2/select2.js',
        'assets-admin/js/plugins/jsTree/jstree.min.js',
        'assets-admin/js/plugins/iCheck/icheck.min.js',
        'assets-admin/js/plugins/cropper/cropper.min.js',
        'assets-admin/js/plugins/jquery-stars/stars.min.js',
        'assets-admin/js/plugins/daterangepicker2/moment.min.js',
        'assets-admin/js/plugins/daterangepicker2/daterangepicker.js',
        'assets-admin/js/plugins/nestable/jquery.nestable.js',
    ];
    
    $sourcePath = "assets-admin/min/all-" . VERSAO . ".js";
    
    $minifier = new Minify\JS($arquivos[0]);
    
    for($i = 1; $i < count($arquivos); $i++) {
        $minifier->add($arquivos[$i]);
    }
    
    $minifier->minify($sourcePath);
    return base_url($sourcePath);
}