<?php

function get_menu_ativo_inativo($atual)
{
	$action = get_action();

	if(is_array($atual)) {
		foreach ($atual as $item) {
			if($item == $action) return 'active';
		}
	}
	return $atual == $action ? 'active' : '';
}

function menu_ativo_inativo($atual)
{
	echo get_menu_ativo_inativo($atual);
}

