<?php
function ordenar_pagamentos_por_nome_aluno($pagamentos)
{
	usort($pagamentos, "cmp_pagamento_nome_aluno");
	return $pagamentos;
}

function cmp_pagamento_nome_aluno($a, $b)
{
// 	return strcmp(strtolower($a['nome']), strtolower($b['nome']));
	
	if(strtolower($a['turma']) == strtolower($b['turma'])) {
		return strcmp(strtolower($a['nome'], strtolower($b['nome'])));
	}
	
	return strcmp(strtolower($a['turma'], strtolower($b['turma'])));
}

function ordenar_pagamentos_por_turma($pagamentos)
{
	usort($pagamentos, "cmp_pagamento_turma");
	return $pagamentos;
}

function cmp_pagamento_turma($a, $b)
{
	return strcmp(strtolower($a['turma']), strtolower($b['turma']));
}