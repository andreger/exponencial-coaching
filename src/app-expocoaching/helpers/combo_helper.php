<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Helper para organizar rotinas de campos combobox (input select)
 * 
 * @package	Coaching/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

/**
 * Retorna um array com itens no formato chave => valor
 * 
 * @since	K4
 * 
 * @param array $lista 			Lista de itens que gerarão o combobox
 * @param string $chave 		Campo do item a ser extraído value da opção
 * @param string $valor 		Campo do item  a ser extraído texto da opção
 * @param string|NULL $label 	Texto a ser exibido no primeiro elemento do combobox
 * 
 * @return array Lista de itens no formato chave => valor
 */ 

function get_combo($lista, $chave, $valor, $label = NULL)
{
	$combo = [];

	if($label) {
		$combo[0] = $label;
	}

	foreach ($lista as $item) {
		$combo[$item[$chave]] = $item[$valor];
	}

	return $combo;
}

function get_operacoes_logicas(){
	return [
	OP_IGUAL => '=',
	OP_MENOR => '<',
	OP_MENOR_OU_IGUAL => '<=',
	OP_MAIOR => '>',
	OP_MAIOR_OU_IGUAL => '>=',
	OP_ENTRE => 'Entre',
];
}

function get_operacoes_logicas_combo(){
	$array = array ("" => "Selecione...");
	return array_replace($array, get_operacoes_logicas());
}