<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function editar_meta_atomica_url($meta_id = NULL)
{
	$sufixo = $meta_id ? "/{$meta_id}" : "";

	return base_url('/admin/editar_meta_atomica' . $sufixo);
}

function get_excluir_meta_atomica_url($meta_id){
	return base_url('/admin/excluir_meta_atomica/'.$meta_id);
}

function get_imprimir_meta_atomica_url($meta_id){
	return base_url('/admin/imprimir_meta_atomica/'.$meta_id);
}

function get_duplicar_meta_atomica_url($meta_id){
    return base_url('/admin/duplicar_meta_atomica/'.$meta_id);
}

function get_listar_metas_atomicas_url(){
	return base_url('/admin/listar_metas_atomicas');
}

function editar_meta_agregada_url($meta_id = NULL)
{
	$sufixo = $meta_id ? "/{$meta_id}" : "";

	return base_url('/admin/editar_meta_agregada' . $sufixo);
}

function get_excluir_meta_agregada_url($meta_id){
	return base_url('/admin/excluir_meta_agregada/'.$meta_id);
}

function get_imprimir_meta_agregada_url($meta_id){
	return base_url('/admin/imprimir_meta_agregada/'.$meta_id);
}

function get_duplicar_meta_agregada_url($meta_id){
    return base_url('/admin/duplicar_meta_agregada/'.$meta_id);
}

function get_listar_metas_agregadas_url(){
	return base_url('/admin/listar_metas_agregadas');
}

function get_array_str($lista, $campo)
{
	if($lista) {

		$output_a = [];
		foreach ($lista as $item) {
			$output_a[] = $item[$campo];
		}

		return implode(", ", $output_a);
	}

	return "";
}

/**
 * Retorna o valor HBC em minutos
 *  
* @since K5
* 
* @param int $hbc_horas Quantidade de horas inteiras do HBC
* @param int $hbc_minutos Quantidade de minutos inteiros do HBC
* 
* @return int HBC em minutos.
 */
function get_meta_hbc($hbc_horas, $hbc_minutos){
	$hbc = 0;
	if($hbc_horas){
		$hbc += $hbc_horas * 60;
	}
	if($hbc_minutos){
		$hbc += $hbc_minutos;
	}
	return $hbc;
}

/**
 * Adiciona o HBC em horas e minutos a uma $meta
 * @since K5
 * 
 * @param object|Array Meta na qual sera extraída o HBC total em minutos e setada o HBC em horas e minutos inteiros
 * 
  */
function set_meta_hbc_horas_minutos(&$meta){
	$hbc = $meta['met_hbc'];
	if($hbc){
		$meta['met_hbc_horas'] = intdiv($hbc, 60);
		$meta['met_hbc_minutos'] = $hbc % 60;
	}
}