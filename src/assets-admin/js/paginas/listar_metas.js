$(function() {
    
	$('.op').change(function() {
		if($(this).val() == 6) {
			$(this).closest(".form-group").find(".op_entre").show();
		}
		else {
			$(this).closest(".form-group").find(".op_entre").hide();
		}
	});

    $("#consultores_ids").select2();
    $("#disciplinas_ids").select2();
    $("#assuntos_ids").select2();

    $('#disciplinas_ids').change("select2:open", function () { 
		var assIds = $('#assuntos_ids').val();
   		var str = "";
   		$('#assuntos_ids').html("<option value=''>Carregando...</option>");
   		
		var ids_a = [];
       	$('#disciplinas_ids option:selected').each(function(i, selected){ 
       		ids_a[i] = $(selected).val(); 
       	});

       	var ids = ids_a.join('-');
       	$.get("/questoes/main/ajax_get_assuntos/" + ids, function(data) {
       		$.each(data,function(index,value){
			  var sel = ($.inArray(data[index].id, assIds) != -1) ? ' selected="selected"' : '';
       		  str += '<option value="' + data[index].id+ '" ' + sel + ' >'+data[index].text+'</option>';
       		});
       		$('#assuntos_ids').html(str);
       	}, 'json');
    });

    $("#limpar-botao-modal").click(function(e) {
		e.preventDefault();
        
        $('#nome').val("");
        $('.multiselect').select2("val", "");
        $('.hbc').val("");
        $('.data').val("");
	});

});

$(document).ready(function(){
	$('.data').mask('99/99/9999');
	$(function() {
		$.fn.datepicker.dates['pt'] = {
				format: 'dd/mm/yyyy',
				days: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
				daysMin: ['D','S','T','Q','Q','S','S','D'],
				daysShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
				months: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
				monthsShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
				today: 'Hoje',
				weekStart: 0
			};
		
		$(document).on('click', '#data_criacao_de', function () { 
	        var me = $("#data_criacao_de");   
	        me.datepicker({
	            showOn: 'focus',
	            altFormat: "mm/dd/yy",
	            dateFormat: "mm/dd/yy",
	            language: 'pt'
	        }).focus();
	    }).on('focus', '#data_criacao_de', function () {
	        var me = $("#data_criacao_de");
	        me.mask('99/99/9999');
	    });

		$(document).on('click', '#data_criacao_ate', function () { 
	        var me = $("#data_criacao_ate");   
	        me.datepicker({
	            showOn: 'focus',
	            altFormat: "mm/dd/yy",
	            dateFormat: "mm/dd/yy",
	            language: 'pt'
	        }).focus();
	    }).on('focus', '#data_criacao_ate', function () {
	        var me = $("#data_criacao_ate");
	        me.mask('99/99/9999');
	    });
		
		$(document).on('click', '#data_edicao_de', function () { 
	        var me = $("#data_edicao_de");   
	        me.datepicker({
	            showOn: 'focus',
	            altFormat: "mm/dd/yy",
	            dateFormat: "mm/dd/yy",
	            language: 'pt'
	        }).focus();
	    }).on('focus', '#data_edicao_de', function () {
	        var me = $("#data_edicao_de");
	        me.mask('99/99/9999');
	    });

		$(document).on('click', '#data_edicao_ate', function () { 
	        var me = $("#data_edicao_ate");   
	        me.datepicker({
	            showOn: 'focus',
	            altFormat: "mm/dd/yy",
	            dateFormat: "mm/dd/yy",
	            language: 'pt'
	        }).focus();
	    }).on('focus', '#data_edicao_ate', function () {
	        var me = $("#data_edicao_ate");
	        me.mask('99/99/9999');
	    });
	});

	$('.hbc').mask('99:99');
});

