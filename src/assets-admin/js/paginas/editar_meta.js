$(document).ready(function(){
    $('.summernote').summernote({
        height: 400,
        
        onImageUpload: function(files) {
            sendFile(files[0], $(this));
        },
        
        onFocus: function(e) {
            $('.note-dialog').parents().removeClass('animated');
        }
    });

    var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Nenhum resultado encontrado.'},
        '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
    
   
    $('#dis_ids').chosen().change(function () { 
		var assIds = $('#ass_ids').val();
   		var str = "";
   		$('#ass_ids').html("<option value=''>Carregando...</option>");
   		$('#ass_ids').trigger("chosen:updated");
		
   		var ids_a = [];
       	$('#dis_ids option:selected').each(function(i, selected){ 
       		ids_a[i] = $(selected).val(); 
       	});

       	var ids = ids_a.join('-');
       	$.get("/questoes/main/ajax_get_assuntos/" + ids, function(data) {
       		$.each(data,function(index,value){
			  var sel = ($.inArray(data[index].id, assIds) != -1) ? ' selected="selected"' : '';
       		  str += '<option value="' + data[index].id+ '" ' + sel + ' >'+data[index].text+'</option>';
       		});
       		$('#ass_ids').html(str);
	       	$("#ass_ids").trigger("chosen:updated");
       	}, 'json');
    });
    
    $('#met_hbc').mask("99:99");
    
    $('#arvore-btn').click(function(e) {
		e.preventDefault();
		
		var ids_a = $("#dis_ids").val();
		 
		var qs = "q=1";

		if($.isArray(ids_a)) {
			$.each(ids_a, function(i, value) {
				qs += '&dis_ids[]=' + value; 
			});
		}
		else {
			qs += '&dis_ids[]=' + ids_a; 
		}

		if(ids_a == null) {
			$('#p2-corpo').html('É necessário selecionar pelo menos uma disciplina.');
		}
		else {
			
			$('#p2-corpo').jstree('destroy');
			
			$('#p2-corpo').jstree( {
			'core' : {
				  'data' : {
				    'url' : '/questoes/xhr/get_arvore?' + qs,
				    'data' : function (node) {
				      return { 'id' : node.id }
				    }
				  }
				},
			'plugins' : [ 'types', 'dnd', 'checkbox' ]
		
			}).bind('loaded.jstree', function(e, data){ 
				
				var ass_ids = $('#ass_ids').val();
				
				if(ass_ids){
					$.each(ass_ids,function(index,value){
						$('#p2-corpo').jstree('check_node', '#'+value);
					});
				}
			});		
		}
	});
    
    $('#adicionar').click(function(e) {
		e.preventDefault();	

		var ids = $("#p2-corpo").jstree("get_checked",null,true);

		$('#ass_ids').val(ids);
		$('#ass_ids').trigger("chosen:updated");
	});

	$('#expand-all').click(function(e){
		$('#p2-corpo').jstree('open_all');
	});
	
	$('#collapse-all').click(function(e){
		$('#p2-corpo').jstree('close_all');
	});
	
    var updateMetasAtomicasInput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');

        if (window.JSON) {
            var metas_atomicas = window.JSON.stringify(list.nestable('serialize'));
            $("#met_ids").val(metas_atomicas);
        }
    };

    $('#nestable').nestable({
        group: 1
    }).on('change', updateMetasAtomicasInput);

    $("#adicionar-metas-modal").click(function(e){
    	var met_ids = $("#met_ids").val();
    	met_ids = window.JSON.parse(met_ids);
    	$("#tabela-metas-atomias-modal input:checked").each(function() {
    		met_ids.push({"met_id": $(this).val()});
    		$("#nestable .dd-list").append("<li class='dd-item' data-met_id='" + $(this).val() + "'><div class='dd-handle'>" + $(this).data("met_nome") + "<a href='#' class='close remover_meta dd-nodrag'>x</a></div></li>");
    	});
    	met_ids = window.JSON.stringify(met_ids)
    	$("#met_ids").val(met_ids);
    	$("#listar-meta-modal").modal("hide");
    	$("#filtrar-meta-modal").modal("show");
    });
    
    $(document).on("click", '.remover_meta', function(event) {
    	var met_id = $(this).closest(".dd-item").data("met_id");
    	$(this).closest(".dd-item").remove();
    	var met_ids = $("#met_ids").val();
    	met_ids = window.JSON.parse(met_ids);
    	met_ids = met_ids.filter(function(item, index) { 
		   return item.met_id != met_id;  
		});
    	met_ids = window.JSON.stringify(met_ids)
    	$("#met_ids").val(met_ids);
    });
	
});